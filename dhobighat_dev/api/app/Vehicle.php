<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Vehicle extends Model
{
    protected $table='vehicle_details';
    private static $_instance = null;


    public static function getInstance()
    {
        if (!is_object(self::$_instance))  //or if( is_null(self::$_instance) ) or if( self::$_instance == null )
            self::$_instance = new Vehicle();
        return self::$_instance;
    }



    public function getVehicleById($vehicle_no){

        $result = DB::table($this->table)
            ->where('vehicle_registration_no',$vehicle_no)
            ->where('vehicle_status',1)
            ->where('vehicle_idle_status',1)
            ->first();

        if ($result) {
            return $result;
        } else {
            return 0;
        }

    }



    public function getVehicleData($vehicle_id){

        $result = DB::table($this->table)
            ->where('vehicle_id',$vehicle_id)
            ->first();

        if ($result) {
            return $result;
        } else {
            return 0;
        }

    }


    public function updateVehicleIdleStatus(){

        if (func_num_args() > 0) {
            $vehicle_registration_no = func_get_arg(0);
            $data = func_get_arg(1);



            try {
                $result = DB::table($this->table)
                    ->where('vehicle_registration_no', $vehicle_registration_no)
                    ->update($data);
            } catch (\Exception $e) {
                return $e->getMessage();
            }
            if ($result) {
                return $result;
            } else {
                return 0;
            }
        } else {
            throw new Exception('Argument Not Passed');
        }


    }



    public function updateVehicleById(){

        if (func_num_args() > 0) {
            $vehicle_id = func_get_arg(0);
            $data = func_get_arg(1);



            try {
                $result = DB::table($this->table)
                    ->where('vehicle_id', $vehicle_id)
                    ->update($data);
            } catch (\Exception $e) {
                return $e->getMessage();
            }
            if ($result) {
                return $result;
            } else {
                return 0;
            }
        } else {
            throw new Exception('Argument Not Passed');
        }


    }


}