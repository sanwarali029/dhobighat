<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    //

    protected $table='price_details';
    private static $_instance = null;

    public static function getInstance()
    {
        if (!is_object(self::$_instance))  //or if( is_null(self::$_instance) ) or if( self::$_instance == null )
            self::$_instance = new Price();
        return self::$_instance;
    }


    public function getPriceDetails(){

        $result = DB::table($this->table)
            ->get();

        if ($result) {
            return $result;
        } else {
            return 0;
        }

    }

}
