<?php

namespace App\Http\Controllers;

use App\Executive;
use App\Vehicle;
use App\ExecutiveVehicle;
use Illuminate\Http\Request;

use App\Http\Requests;
use stdClass;

class ExecutiveController extends Controller
{
    public function executiveLogIn(Request $request)
    {


        $response = new stdClass();

        if ($request->isMethod('POST')) {
            $executive_id = $request->input('executive_custom_id');
            $executive_password = $request->input('executive_password');
            $vehicle_no = $request->input('vehicle_no');

            $vehicle_registration_no['vehicle_registration_no'] = $request->input('vehicle_no');
            $vehicle_data['vehicle_idle_status']=0;
            $vehicle_data['vehicle_last_logged_in_executive']=$executive_id;

            $data['executive_token_no']=$request->input('executive_token_no');
            $data['executive_device_id'] = $request->input('executive_device_id');
            $data['executive_last_logged_in_vehicle'] = $request->input('vehicle_no');

            $objectModelExecutive = Executive::getInstance();
            $objectModelVehicle = Vehicle::getInstance();
            $objectModelExecutiveVehicle = ExecutiveVehicle::getInstance();

            $getVehicleById = $objectModelVehicle->getVehicleById($vehicle_no);
//
//            print_r($objectModelExecutive);
//            print_r($objectModelVehicle);
//            print_r($objectModelExecutiveVehicle);
            //die();

            if ($getVehicleById) {


                if ($executive_id || $executive_password) {

                    $getExecutiveLogin = $objectModelExecutive->getExecutiveLogin($executive_id, $executive_password);
                    // print_r($getExecutiveByCustomId);

                    if ($getExecutiveLogin) {

                             $getExecutiveVehicleById = $objectModelExecutiveVehicle->getExecutiveVehicleDetailsById($getExecutiveLogin->executive_id);

                          //   print_r($getExecutiveVehicleById);
                                 if($getExecutiveVehicleById){


                                   //  print_r('yes');

                                     $idle_status['vehicle_idle_status']=1;
                                     $updateVehicleIdleStatus= $objectModelVehicle->updateVehicleIdleStatus($getExecutiveVehicleById->vehicle_registration_no,$idle_status);

                                     $UpdateExecutiveVehicleById = $objectModelExecutiveVehicle->updateExecutiveWithVehicleById($getExecutiveLogin->executive_id,$vehicle_registration_no);

                                 }

                                 else{
                                     //print_r('no');
                                     $EVdata = array();
                                     $EVdata['vehicle_registration_no'] = $request->input('vehicle_no');
                                     $EVdata['executive_id'] = $getExecutiveLogin->executive_id;
                                     $EVdata['service_status'] = 1;


                                     $addExecutiveVehicleById = $objectModelExecutiveVehicle->addVehicleByExecutiveId($EVdata);

                                 }



                       // $updateDeviceId = $objectModelExecutive->updateDeviceId($executive_id, $Executive_device_id);

                        $updateExecutiveData = $objectModelExecutive->updateExecutiveById($getExecutiveLogin->executive_id, $data);
                        $updateVehicleIdleStatus= $objectModelVehicle->updateVehicleIdleStatus($vehicle_no,$vehicle_data);


                        // print_r($updateDeviceId);
                        //   print_r($addExecutiveVehicleById);

                        // here data is fetched
                      //  if ($updateDeviceId) {


                            $getExecutiveById = $objectModelExecutive->getExecutiveData($getExecutiveLogin->executive_id);
                            $executiveVehicleData = $objectModelExecutiveVehicle->getExecutiveVehicleDetailsById($getExecutiveLogin->executive_id);


                            $response->code = 200;
                            $response->message = "Login successful";
                            $response->data = $getExecutiveById;
                            $response->ExecutiveVehicleData = $executiveVehicleData;
                            echo json_encode($response);

                            //  print_r($executiveVehicleData);





                    } else {

                        $response->code = 400;
                        $response->message = "Invalid credentials";
                        $response->data = null;
                        echo json_encode($response);
                    }


                } else {

                    $response->code = 400;
                    $response->message = "Enter Executive ID and Password ";
                    $response->data = null;
                    echo json_encode($response);
                }
            }
            else {

                    $response->code = 400;
                    $response->message = "Vehicle id is not available or does not exist ";
                    $response->data = null;
                    echo json_encode($response);


                }

            }
        }

    public function executiveLogOut(Request $request){


        $response = new stdClass();

        if ($request->isMethod('POST')) {
            $executive_id = $request->input('executive_id');
            $vehicle_no = $request->input('vehicle_no');
            $vehicle_idle_status['vehicle_idle_status']=1;

            $objectModelVehicle = Vehicle::getInstance();
            $objectModelExecutiveVehicle = ExecutiveVehicle::getInstance();

            $deleteExecutiveVehicle= $objectModelExecutiveVehicle->deleteExecutiveVehicle($executive_id,$vehicle_no);
            $updateVehicleIdleStatus= $objectModelVehicle->updateVehicleIdleStatus($vehicle_no,$vehicle_idle_status);


            $response->code = 200;
            $response->message = "You have successfully logged out";
            echo json_encode($response);
        }
    }
    public function updateExecutiveToken(Request $request){


        $response = new stdClass();

        if ($request->isMethod('POST')) {
            $executive_id = $request->input('executive_id');
            $executive_token_no['executive_token_no'] = $request->input('executive_token_no');

            $objectModelExecutive = Executive::getInstance();

            $updateExcecutiveToken= $objectModelExecutive->updateExecutiveById($executive_id,$executive_token_no);


            $response->code = 200;
            $response->message = "Token updated successfully";
            echo json_encode($response);
        }
    }



}
