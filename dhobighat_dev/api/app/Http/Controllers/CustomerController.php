<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;
use stdClass;
use App\Http\Requests;

class CustomerController extends Controller
{
    //Customer sign up

public function customerSignUp(Request $request)
{

    $response = new stdClass();

    if ($request->isMethod('POST')) {

        $objectModelCustomer = Customer::getInstance();
        $data = array();
        $data['customer_name'] = $request->input('customer_name');
        $customer_phone_no = $request->input('customer_phone_no');
        $data['customer_phone_no'] = $request->input('customer_phone_no');

        $customer_email = $request->input('customer_email');
        $data['customer_email'] = $request->input('customer_email');

        $data['customer_password'] = $request->input('customer_password');
        $data['customer_agreement'] = $request->input('customer_agreement');
        $data['customer_device_id'] = $request->input('customer_device_id');
        $data['customer_token_no'] = $request->input('customer_token_no');

//
//          print_r($data);
//           die();




        $checkCustomerByEmail = $objectModelCustomer->checkCustomerByEmail($customer_email);
        $checkCustomerByPhone = $objectModelCustomer->checkCustomerByPhone($customer_phone_no);

        if ($checkCustomerByEmail || $checkCustomerByPhone) {



            $response->code = 400;
            $response->message = "Email Id or Phone number already exist";
            $response->data = null;
            echo json_encode($response);


        }else{

            $addCustomerById = $objectModelCustomer->addCustomer($data);
            // print_r($addCustomerById);

            if ($addCustomerById) {

                $customerDataById = $objectModelCustomer->getCustomerById($addCustomerById);

                $response->code = 200;
                $response->message = "Customer Registered Successfully";
                $response->customer_data = $customerDataById;
                echo json_encode($response);
            } else {
                $response->code = 400;
                $response->message = "Customer registration unsuccessful";
                $response->data = null;
                echo json_encode($response);
            }

        }
    }
}


    //Customer log In
    public function customerLogIn(Request $request)
    {

        $response = new stdClass();

        if ($request->isMethod('POST'))
        {
            $auth = $request->input('auth');
            $password = $request->input('password');
            $data['customer_device_id'] = $request->input('customer_device_id');
            $data['customer_token_no'] = $request->input('customer_token_no');

            $objectModelCustomer = Customer::getInstance();





            if(strpos($auth, '@') == false) {


                if ($auth || $password) {

                    $customerById = $objectModelCustomer->getUsersLoginWithPhone($auth, $password);

                    if ($customerById) {
                        $updateDeviceId = $objectModelCustomer->updateCustomerById($data, $customerById->customer_id);

                        $customerUpdatedData = $objectModelCustomer->getCustomerById($customerById->customer_id);
                        $response->code = 200;
                        $response->message = "Login successful";
                        $response->data = $customerUpdatedData;
                        echo json_encode($response);

                    } else {

                        $response->code = 400;
                        $response->message = "Please enter the right credentials";
                        @$response->data = null;
                        echo json_encode($response);
                    }

                } else {
                    $response->code = 400;
                    $response->message = "Please enter phone number/email and password";
                    @$response->data = null;
                    echo json_encode($response);
                }
            }



            else{

                    if($auth || $password) {


                        $customerById = $objectModelCustomer->getUsersLoginWithEmail($auth, $password);

                        if ($customerById) {

                            $updateDeviceId = $objectModelCustomer->updateCustomerById($data, $customerById->customer_id);

                            $customerUpdatedData = $objectModelCustomer->getCustomerById($customerById->customer_id);
                            $response->code = 200;
                            $response->message = "Login successful";
                            $response->data = $customerUpdatedData;
                            echo json_encode($response);
                        } else {

                            $response->code = 400;
                            $response->message = "Please enter the right credentials";
                            $response->data = null;
                            echo json_encode($response);
                        }

                    }else {
                        $response->code = 400;
                        $response->message = "Please enter phone number/email and password";
                        $response->data = null;
                        echo json_encode($response);
                    }
                }




        }

    }



    public function getCustomerData(Request $request)
    {

        $response = new stdClass();

        if ($request->isMethod('POST')) {
            $customer_id = $request->input('customer_id');
            $objectModelCustomer = Customer::getInstance();
            $getCustomerById=$objectModelCustomer->getCustomerById($customer_id);
            if($getCustomerById) {
                $response->code = 200;
                $response->message = "Customer Data";
                $response->data = $getCustomerById;
                echo json_encode($response);
            }
            else{

                $response->code = 400;
                $response->message = "Customer does not exist";
                $response->data = null;
                echo json_encode($response);

            }
        }
    }


    public function updateCustomerToken(Request $request){


        $response = new stdClass();

        if ($request->isMethod('POST')) {
            $customer_id = $request->input('customer_id');
            $customer_token_no['customer_token_no'] = $request->input('customer_token_no');

            $objectModelCustomer = Customer::getInstance();

            $updateCustomerToken= $objectModelCustomer->updateCustomerById($customer_token_no,$customer_id);


            $response->code = 200;
            $response->message = "Token updated successfully";
            echo json_encode($response);
        }
    }


    public function updateCustomerProfile(Request $request){


        $response = new stdClass();

        if ($request->isMethod('POST')) {

            $objectModelCustomer = Customer::getInstance();

            $customer_id = $request->input('customer_id');
            $password= $request->input('customer_password');
            $name= $request->input('customer_name');

                $data['customer_password']=$password;
                $data['customer_name'] =$name;

                $updateCustomerProfile= $objectModelCustomer->updateCustomerById($data,$customer_id);

                if($updateCustomerProfile)
            {
                $customer_data = $objectModelCustomer->getCustomerById($customer_id);

                $response->code = 200;
                $response->message = "profile updated successfully";
                $response->data=$customer_data;
                echo json_encode($response);

            }else{

                    $response->code = 400;
                    $response->message = "No new data to update";
                    $response->data=null;
                    echo json_encode($response);

                }

        }
    }






}


