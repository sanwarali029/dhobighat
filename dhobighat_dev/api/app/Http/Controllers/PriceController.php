<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Price;
use stdClass;

class PriceController extends Controller
{
    //

    public function getPriceData(){


        $response =new stdClass();
        $objectModelPrice = Price::getInstance();
        $getPriceDetails= $objectModelPrice->getPriceDetails();
        if($getPriceDetails){



            $response->code = 200;
            $response->message ='Minimum Price-weight Data';
            $response->data = $getPriceDetails;
            echo json_encode($response);


        }
        else{


            $response->code = 400;
            $response->message ='No minimum price-weight details available';
            $response->data = null;
            echo json_encode($response);

        }

    }
}
