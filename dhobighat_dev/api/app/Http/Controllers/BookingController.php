<?php

namespace App\Http\Controllers;

use App\Executive;
use App\Vehicle;
use App\ExecutiveVehicle;
use App\Booking;
use App\BookingCategory;
use App\Customer;
use App\Category;
use App\Price;
use App\Cancellation;
use App\BookingCancellation;
use App\Feedback;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\ExecutiveNotification;
use stdClass;

class BookingController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */


    public function addBooking(Request $request)
    {
        if ($request->isMethod('POST')) {
            // return 'addBooking';

            $response = new stdClass();

            $objectModelExecutiveNotification = ExecutiveNotification::getInstance();
            $objectModelBookingDetails = Booking::getInstance();
            $objectModelBookingCategoryDetails = BookingCategory::getInstance();


            //check for free customer logic customer logic

//            $objectModelCustomerDetails = Customer::getInstance();
//            $customer_id = $request->input('customer_id');
//            $customerById=$objectModelCustomerDetails->getCustomerById($customer_id);
//            //print_r($customerById->customer_status);
//
//            if($customerById->customer_status==0){
//
//                //booking process
//
//            }else{
//
//                $response->code = 400;
//                $response->message = "Your previous booking not yet completed";
//                echo json_encode($response);
//
//            }

                if ($this->getExecutiveData()) {

                   //create new booking

                    $data = array();

                    // $data['executive_id'] = $request->input('executive_id');
                    $data['customer_id'] = $request->input('customer_id');
                    $data['booking_time'] = $request->input('booking_time');
                    $data['booking_type'] = 0;
                    $data['customer_address'] = $request->input('customer_address');
                    $data['customer_longitude'] = $request->input('customer_longitude');
                    $data['customer_latitude'] = $request->input('customer_latitude');
                    $data['estimated_price'] = $request->input('estimated_price');
                    $data['service_time'] = $request->input('service_time');
                    $data['estimated_weight'] = $request->input('estimated_weight');
                    $data['estimated_count'] = $request->input('estimated_count');

                    $addBookingById = $objectModelBookingDetails->addNewBooking($data);

                    //add data to booking category table
                    $items = $request->input('items');
                    $data = (json_decode($items));


                    foreach ($data as $key => $value) {


                        $id = $value->category_id;
                        $quantity = $value->count;

                        //  echo $id . ", " . $quantity . "<br>";


                        $catdata['category_id'] = $id;
                        $catdata['count'] = $quantity;
                        $catdata['booking_id'] = $addBookingById;

                        $addBookingCategoryDetails = $objectModelBookingCategoryDetails->addBookingCategoryDetails($catdata);
                        //echo $addBookingCategoryDetails;
                    }


                    //create a new booking ends



                     if ($addBookingCategoryDetails && $addBookingById) {


                    //gather executive data for sending notification


                    $exe_data = $this->getExecutiveData();

                    $token_ids = $exe_data[0];
                    $executive_ids = $exe_data[1];
                  //  $message = 'New Booking Request';


                    $notification_data = array_combine($executive_ids, $token_ids);

                    //insert notification data in table

                    foreach ($notification_data as $key => $value) {

                        $notdata['executive_id'] = $key;
                        $notdata['exe_notification_message'] = 'New Booking Request';
                        $notdata['booking_id'] = $addBookingById;

//                           echo $key;
//                           echo $value;


                        $addNewNotification = $objectModelExecutiveNotification->addNewNotification($notdata);
                        //  echo $addNewNotification;

                    }

                    if ($addNewNotification) {

                        //get notification data from table

                        $notificationData = $objectModelExecutiveNotification->getNotificationDataById($addNewNotification);

                        $notification_id = $notificationData->exe_notification_id;
                        $booking_id = $notificationData->booking_id;
                        $executive_id = $notificationData->executive_id;
                        $message = $notificationData->exe_notification_message;
                        $alert ="booking";


                        //send notification

                        $this->getNotification($token_ids, $message, $booking_id, $executive_id, $notification_id,$alert);

                    }
                    $response->code = 200;
                $response->message = "Booking created successfully";
                $response->booking_id =$addBookingById ;
                echo json_encode($response);
                }


            }

        }
    }

    public function getExecutiveData()
    {


        $response = new stdClass();

        $result = DB::table('executive_vehicle_login')
            ->join('executive_details', 'executive_vehicle_login.executive_id', '=', 'executive_details.executive_id')
            ->select('executive_details.executive_token_no', 'executive_details.executive_id')
            ->where('executive_vehicle_login.service_status', '=' , 1)
            ->get();

//        print_r($result) ;
//        die();

        if (count($result) > 0) {


            $tokens = array();
            $ids = array();

            foreach ($result as $key => $value) {
                $tokens[] = $value->executive_token_no;
                $ids[] = $value->executive_id;
            }

            $data = array($tokens, $ids);
            return $data;
            // print_r($data);

        } else {

            $response->code = 400;
            $response->message = 'No Executives Are Available To Serve At This Moment';
            echo json_encode($response);
            return 0;
        }


    }

    public function getNotification($token_nos, $notification_message, $booking_no, $executive_no, $notification_no,$alert_type)
    {


        $url = 'https://fcm.googleapis.com/fcm/send';
        $priority = "high";


        $tokens = $token_nos;
        // $tokens=array('dsW1NRFtGCc:APA91bGbi9CaIhfI60fFeR-ys8k8nMgFALR5ClgKmsNjp_jaMxd3yQejkIylK0xQUOIh2KzPyxQM8YR7SDIuUb8J5Ccrrd3BjEhOtDnhmovrGbJdQ8GgOxhggUm3_yOYT5l-14U1rKVl');
        $message = $notification_message;
        $booking_id = $booking_no;
        $executive_id = $executive_no;
        $notification_id = $notification_no;
        $alert=$alert_type;


//          $fields = array(
//              'registration_ids' =>$tokens,
//             // 'data' =>$message
//              'data' => array("message" => $message),
//          );


        $fields = array(
            'registration_ids' => $tokens,
            'data' => array("message" => $message, 'booking_id' => $booking_id, 'executive_id' => $executive_id, 'notification_id' => $notification_id, 'alert_type' => $alert)

        );


        $headers = array(
            'Authorization:key=AAAAm2HK5ZQ:APA91bGuJQV1-FjL_ONPreKZETrPfg3AaBHR_DJPXH_Bfzw9v9wI9h_Jcs3F-rhQzewtTovJZMqRnv9XrKJdQ5h1Y71oaEfTZMwfelp9a6yw8ChGB6Eexuzhlmo9OpmJpIczzeNfpxef',
            'Content-Type: application/json'
        );

        $ch = curl_init();
//          print_r($ch);
//          die();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
       // print_r( json_encode($fields));
        $result = curl_exec($ch);
        curl_error($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;

    }



    public function getExecutiveBookingNotificationList(Request $request)
    {


        if ($request->isMethod('POST')) {


            $response = new stdClass();
            $executive_id = $request->input('executive_id');

            $objectModelBookingDetails = Booking::getInstance();
            $objectModelExecutiveNotificationDetails = ExecutiveNotification::getInstance();
           $objectModelCustomer = Customer::getInstance();

            $BookingIdsByExecutiveId = $objectModelExecutiveNotificationDetails->getBookingIdsByExecutiveId($executive_id);

           //print_r($BookingIdsByExecutiveId);
            //$bookingDetailsByExecutiveId = $objectModelBookingDetails->getBookingDetailsByExecutiveId($executive_id);

            if ($BookingIdsByExecutiveId) {


                foreach ($BookingIdsByExecutiveId as $booking_id){

                    $booking_data= $objectModelBookingDetails->getBookingDetailsById($booking_id);
                    $customer_data = $objectModelCustomer->getCustomerById($booking_data->customer_id);

                    if($booking_data->booking_status == 0 &&  $booking_data->not_accepted_status ==0 && $booking_data->bcan_id == null){

                        $data[]=$booking_data;
                       $data[]=$customer_data;

                    }

                }

                $response->code = 200;
                $response->message = "Executive Notifications Data";
                $response->data = $data;
                echo json_encode($response);

            } else {

                $response->code = 400;
                $response->message = "No Executive Notifications Found";
                $response->data = null;
                echo json_encode($response);


            }

        }

    }

    public function cancelNewBooking(Request $request)
    {


        if ($request->isMethod('POST')) {


            $response = new stdClass();
            $booking_id = $request->input('booking_id');

            $objectModelBookingDetails = Booking::getInstance();
            $objectModelBookingCategoryDetails = BookingCategory::getInstance();


            //logic for changing customer status

//            $cdata['customer_status']=0;
//            $bookingData = $objectModelBookingDetails->getBookingDetailsById($booking_id);
//            $objectModelCustomerDetails = Customer::getInstance();
//            $updateCustomerStatus= $objectModelCustomerDetails->updateCustomerById($cdata,$bookingData->customer_id);



           $deleteBookingById = $objectModelBookingDetails->deleteBookingById($booking_id);

           // print_r($deleteBookingById);
           $deleteBookingCategoriesById=$objectModelBookingCategoryDetails->deleteBookingCategoriesById($booking_id);

             // print_r($deleteBookingCategoriesById);
//
           $objectModelExecutiveNotifications = ExecutiveNotification::getInstance();
            $deleteExecutiveNotificationsByBookingId =$objectModelExecutiveNotifications->deleteExecutiveNotificationsByBookingId($booking_id);



            if ($deleteBookingById && $deleteBookingCategoriesById && $deleteExecutiveNotificationsByBookingId) {

                $response->code = 200;
                $response->message = "Booking cancelled successfully";
                echo json_encode($response);

            }

        }

    }




    public function getBookingDetailsToServe(Request $request)
    {


        if ($request->isMethod('POST')) {


            $response = new stdClass();
            $booking_id= $request->input('booking_id');

            $objectModelBookingDetails = Booking::getInstance();
            $bookingDetailsById = $objectModelBookingDetails->getBookingDetailsById($booking_id);
            $status=$bookingDetailsById->booking_status;
           // print_r($bookingDetailsById);

            if ($status == 0) {

                $response->code = 200;
                $response->message = "Show Booking Details";
                echo json_encode($response);

            } else {

                $response->code = 400;
                $response->message = "Booking already accepted by other executive";
                echo json_encode($response);


            }

        }

    }



    public function serveBookingRequest(Request $request)
    {


        if ($request->isMethod('POST')) {


            $response = new stdClass();
            $booking_id= $request->input('booking_id');
            $executive_id= $request->input('executive_id');

            $objectModelBookingDetails = Booking::getInstance();
            $objectModelExecutiveDetails = Executive::getInstance();
            $objectModelCustomerDetails = Customer::getInstance();
            $objectModelExecutiveNotification = ExecutiveNotification::getInstance();
            $objectModelExecutiveVehicle = ExecutiveVehicle::getInstance();
            $objectModelVehicle = Vehicle::getInstance();


            $bookingDetailsById = $objectModelBookingDetails->getBookingDetailsById($booking_id);
            $executiveVehicleDetailsById = $objectModelExecutiveVehicle->getExecutiveVehicleDetailsById($executive_id);

            //  print_r($bookingDetailsById);

            if($executiveVehicleDetailsById->service_status == 1){

                if ($bookingDetailsById->not_accepted_status == 0) {



                    if ($bookingDetailsById->booking_status == 0) {



                        $executiveDataById = $objectModelExecutiveDetails->getExecutiveData($executive_id);
                        $data=array();

                        $data['executive_id']= $executiveDataById->executive_id;
                        $vehicle= $executiveDataById->executive_last_logged_in_vehicle;

                        $result= DB::table('vehicle_details')
                            ->where('vehicle_registration_no',$vehicle)
                            ->pluck('vehicle_id');
                        $data['vehicle_id']= $result[0];
                        $data['booking_status']= 1;

                        //  print_r($data);

                        $addExecutiveToBooking =$objectModelBookingDetails->updateBookingById($booking_id,$data);

                        $notData['notification_status']=1;
                        $updateExecutiveNotificationData = $objectModelExecutiveNotification->updateExecutiveNotificationData($booking_id,$executive_id,$notData);

                        $serData['service_status']=0;
                        $updateExecutiveVehicleServiceStatus =$objectModelExecutiveVehicle->updateExecutiveWithVehicleById($executive_id,$serData);


                        $vdata['vehicle_services_done']=$objectModelVehicle->getVehicleData($result[0])->vehicle_services_done + 1;
                        $vdata['vehicle_last_service']=$booking_id;
                        $updateVehicle=$objectModelVehicle->updateVehicleById($result[0],$vdata);

                        $bookingDetails = $objectModelBookingDetails->getBookingDetailsById($booking_id);

                        $customer_id=$bookingDetailsById->customer_id;
                        $getCustomerById=$objectModelCustomerDetails->getCustomerById($customer_id);


                        $response->code = 200;
                        $response->message = "Booking Accepted successfully";
                        $response->booking_details = $bookingDetails;
                        $response->customer_details = $getCustomerById;
                        echo json_encode($response);

                    } else {

                        $response->code = 400;
                        $response->message = "Booking already accepted by other executive";
                        $response->data = null;
                        echo json_encode($response);


                    }



                }else{


                    $response->code = 400;
                    $response->message = "This booking's acceptance time is over";
                    $response->data = null;
                    echo json_encode($response);



                }






            }else{


                $response->code = 400;
                $response->message = "Your previous booking not yet completed";
                $response->data = null;
                echo json_encode($response);

            }



        }

    }




    public function checkRequestAcceptance(Request $request)
    {


        if ($request->isMethod('POST')) {


            $response = new stdClass();
            $booking_id= $request->input('booking_id');

            $objectModelBookingDetails = Booking::getInstance();
            $objectModelExecutiveDetails = Executive::getInstance();
            $objectModelVehicleDetails = Vehicle::getInstance();

            $bookingDetailsById = $objectModelBookingDetails->getBookingDetailsById($booking_id);
            $status=$bookingDetailsById->booking_status;

            $executive_id=$bookingDetailsById->executive_id;
            $getExecutiveData=$objectModelExecutiveDetails->getExecutiveData($executive_id);
            // print_r($bookingDetailsById);

            $vehicle_id=$bookingDetailsById->vehicle_id;
            $getVehicle_data=$objectModelVehicleDetails->getVehicleData($vehicle_id);

            if ($status == 0) {

                $response->code = 400;
                $response->message = "Booking Not Yet Accepted";
                echo json_encode($response);

            } else {

                $response->code = 200;
                $response->message = "Booking Accepted Successfully";
                $response->booking_data = $bookingDetailsById;
                $response->executive_data = $getExecutiveData;
                $response->vehicle_data = $getVehicle_data;
                echo json_encode($response);


            }

        }

    }





    public function getBookingData(Request $request)
    {


        if ($request->isMethod('POST')) {


            $response = new stdClass();
            $booking_id= $request->input('booking_id');

            $objectModelBookingDetails = Booking::getInstance();
            $objectModelExecutiveDetails = Executive::getInstance();
            $objectModelVehicleDetails = Vehicle::getInstance();
            $objectModelCustomerDetails = Customer::getInstance();

            $bookingDetailsById = $objectModelBookingDetails->getBookingDetailsById($booking_id);
            if($bookingDetailsById){

                 if($bookingDetailsById->service_status < 3){


                     $executive_id=$bookingDetailsById->executive_id;
                     $getExecutiveData=$objectModelExecutiveDetails->getExecutiveData($executive_id);

                     $vehicle_id=$bookingDetailsById->vehicle_id;
                     $getVehicle_data=$objectModelVehicleDetails->getVehicleData($vehicle_id);

                     $customer_id=$bookingDetailsById->customer_id;
                     $getCustomerData=$objectModelCustomerDetails->getCustomerById($customer_id);


                     $response->code = 200;
                     $response->message = "Booking Data";
                     $response->booking_data = $bookingDetailsById;
                     $response->executive_data = $getExecutiveData;
                     $response->vehicle_data = $getVehicle_data;
                     $response->customer_data = $getCustomerData;
                     echo json_encode($response);

                 }

               else{

                   $response->code = 400;
                   $response->code = 400;
                   echo json_encode($response);
               }

            }else{


                $response->code = 400;
                echo json_encode($response);

            }


        }


    }




    public function updateNoOneAccepted(Request $request)
    {


        if ($request->isMethod('POST')) {


                 $response = new stdClass();
                 $booking_id= $request->input('booking_id');
                 $data['not_accepted_status']=1;

                $objectModelBookingDetails = Booking::getInstance();
                $updateBookingDetailsById = $objectModelBookingDetails->updateBookingById($booking_id,$data);



                $response->code = 200;
                $response->message = "Not Accepted Status Updated Successfully";
                echo json_encode($response);



        }

    }


    public function startService(Request $request)
    {


        if ($request->isMethod('POST')) {


            $response = new stdClass();
            $booking_id= $request->input('booking_id');
            $data['service_started']=1;

            $objectModelBookingDetails = Booking::getInstance();
            $bookingDetailsById=$objectModelBookingDetails->getBookingDetailsById($booking_id);

            if($bookingDetailsById->service_status == 4){

                $response->code = 400;
                $response->message = "This order has been cancelled by customer";
                echo json_encode($response);

            }else{
                $updateBookingDetailsById = $objectModelBookingDetails->updateBookingById($booking_id,$data);
                $response->code = 200;
                $response->message = "Service Started Successfully";
                echo json_encode($response);
            }





        }

    }




    public function getBookingCategoryData(Request $request)
    {


        if ($request->isMethod('POST')) {


            $response = new stdClass();
            $booking_id= $request->input('booking_id');


            $objectModelBookingCategoryDetails = BookingCategory::getInstance();
            $objectModelBookingDetails = Booking::getInstance();
            $bookingDetailsById=$objectModelBookingDetails->getBookingDetailsById($booking_id);

            $objectModelCategoryDetails = Category::getInstance();
            $objectModelPriceDetails = Price::getInstance();

            $bookingCategoryDetailsByBookingId = $objectModelBookingCategoryDetails->getBookingCategoryDetailsByBookingId($booking_id);
            $price_data=$objectModelPriceDetails->getPriceDetails();

            if ($bookingCategoryDetailsByBookingId) {

                $data=array();

               foreach ($bookingCategoryDetailsByBookingId as $key => $value){

                   $sata=array();
                   $id = $value->category_id;
                   $quantity = $value->count;
                   $categoryDetailsById=$objectModelCategoryDetails->getCategoryDetailsById($id);

                   $sata['category_id']=$id;
                   $sata['count']=$quantity;
                   $sata['category_name']=$categoryDetailsById->category_name;
                   $sata['category_icon_black']=$categoryDetailsById->category_icon_black;
                   $sata['category_icon_white']=$categoryDetailsById->category_icon_white;
                   $sata['category_average_weight_per_piece']=$categoryDetailsById->category_average_weight_per_piece;
                   $sata['category_average_price_per_kg']=$categoryDetailsById->category_average_price_per_kg;
                   $data[]=$sata;
                }



               //print_r($data);

//                   echo $price_data->minimu_price;
//                   echo $price_data->minimun_weight;
//                 die();

                $response->code = 200;
                $response->message = "Booking Category Data";
                $response->estimated_price = $bookingDetailsById->estimated_price;
                $response->service_status = $bookingDetailsById->service_status;
                $response->minimun_price = $price_data[0]->minimum_price;
                $response->minimun_weight = $price_data[0]->minimum_weight;
                $response->data =$data ;
                echo json_encode($response);

            } else {

                $response->code = 400;
                $response->estimated_price = 0;
                $response->minimun_price = 0;
                $response->minimun_weight = 0;
                $response->message = "No Booking Category Data Found";
                $response->data = null;
                echo json_encode($response);


            }

        }

    }




    public function updateBookingCategoryDataCustomer(Request $request)
    {


        if ($request->isMethod('POST')) {


            $response = new stdClass();
            $booking_id= $request->input('booking_id');
            $data['estimated_price']= $request->input('estimated_price');
            $data['estimated_weight']= $request->input('estimated_weight');
            $data['estimated_count']= $request->input('estimated_count');


            $objectModelBookingCategoryDetails = BookingCategory::getInstance();
            $objectModelBookingDetails = Booking::getInstance();
            $getBookingData=$objectModelBookingDetails->getBookingDetailsById($booking_id);

            if($getBookingData->service_status == 0){



                $updateBookingDetails=$objectModelBookingDetails->updateBookingById($booking_id,$data);


                $items = $request->input('items');
                $data = (json_decode($items));




                foreach ($data as $key => $value) {


                    $id = $value->category_id;
                    $quantity = $value->count;

                    // echo $id . ", " . $quantity . "<br>";


                    // $catdata['category_id'] = $id;
                    $catdata['count'] = $quantity;


                    $updateBookingCategoryDetails = $objectModelBookingCategoryDetails->updateBookingCategoryDetails($booking_id,$id,$catdata);

                }

                $response->code = 200;
                $response->message = "Booking data updated Successfully";
                echo json_encode($response);


            }else{

                $response->code = 400;
                $response->message = "Booking data can not be updated at this stage";
                echo json_encode($response);

            }



        }

    }





    public function updateBookingCategoryDataExecutive(Request $request)
    {


        if ($request->isMethod('POST')) {


            $response = new stdClass();
            $booking_id= $request->input('booking_id');

            $objectModelBookingCategoryDetails = BookingCategory::getInstance();
            $objectModelBookingDetails = Booking::getInstance();
            $objectModelCategoryDetails = Category::getInstance();
            $objectModelPriceDetails = Price::getInstance();


            $getBookingData=$objectModelBookingDetails->getBookingDetailsById($booking_id);
            $final_count=0;
            $final_weight=0;
            $final_price=0;


            if($getBookingData){




                if($getBookingData->service_status == 0){

                    $items = $request->input('items');
                    $data = (json_decode($items));



                    foreach ($data as $key => $value) {


                        $id = $value->category_id;
                        $quantity = $value->count;
                        $catdata['count'] = $value->count;
                        $weight=$value->weight;

                        $final_count=$final_count+$quantity;
                        $final_weight=$final_weight+$weight;

                        $CategoryPriceDetails=$objectModelCategoryDetails->getCategoryDetailsById($id);
                        $cost=$CategoryPriceDetails->category_average_price_per_kg;
                        $price=($weight/1000)*$cost;

                        $final_price=$final_price+$price;


                        //   echo $id . ", " . $quantity .  ", " . $weight . "<br>";

                        $updateBookingCategoryDetails = $objectModelBookingCategoryDetails->updateBookingCategoryDetails($booking_id,$id,$catdata);

                    }

                    $getPriceDetails=$objectModelPriceDetails->getPriceDetails();
                    $minPrice =   $getPriceDetails[0]->minimum_price;
                    $minWeight =   $getPriceDetails[0]->minimum_weight;


                    if($minWeight*1000 > $final_weight){

                        $final_price=$minPrice;


                    }

                    //   echo $final_count . ", " . $final_weight .  ", " . $final_price ;

                    //  echo $minPrice . ", " . $minWeight . "<br>";

                    // print_r($getPriceDetails);

                    $fdata=array();

                    $fdata['final_price']= $final_price;
                    $fdata['estimated_price']= $final_price;
                    $fdata['final_weight']=$final_weight;
                    $fdata['estimated_weight']=$final_weight;
                    $fdata['final_count']= $final_count;
                    $fdata['estimated_count']= $final_count;
                    $fdata['service_status']= 1;

                    // print_r($fdata);

                    $updateBookingDetails=$objectModelBookingDetails->updateBookingById($booking_id,$fdata);

                    $BookingData=$objectModelBookingDetails->getBookingDetailsById($booking_id);
                    $response->code = 200;
                    $response->message = "Booking data updated Successfully";
                    $response->booking_data = $BookingData;
                    echo json_encode($response);


                }else{

                    $response->code = 400;
                    $response->message = "Booking data can not be updated at this stage";
                    $response->booking_data = null;
                    echo json_encode($response);

                }




            }else{

                $response->code = 400;
                $response->message = "No Booking Found";
                $response->booking_data = null;
                echo json_encode($response);

            }




         // //  echo $final_count . ", " . $final_weight .  ", " . $final_price ;
        }

    }





    public function cancellationData()
    {


     //   echo 'sanwar';

            $response = new stdClass();
            $objectModelBookingCancellationDetails = Cancellation::getInstance();
            $getCancellationData = $objectModelBookingCancellationDetails->getCancellationDetails();

            if ($getCancellationData) {

                $response->code = 200;
                $response->message = "Cancellation Data";
                $response->data =$getCancellationData ;
                echo json_encode($response);

            } else {

                $response->code = 400;
                $response->message = "No Cancellation Data Found";
                $response->data = null;
                echo json_encode($response);


            }

    }




    public function cancelOrder(Request $request)
    {


        if ($request->isMethod('POST')) {


            $response = new stdClass();
            $booking_id= $request->input('booking_id');
            $data['booking_id']= $request->input('booking_id');
            $data['cancellation_reason']= $request->input('cancellation_reason');
            $data['comments'] = $request->input('comments');


            $objectModelBookingCancellationDetails = BookingCancellation::getInstance();
            $objectModelBookingDetails = Booking::getInstance();
            $bookingData = $objectModelBookingDetails->getBookingDetailsById($booking_id);

            $bookingCancellationId = $objectModelBookingCancellationDetails->addBookingCancellation($data);

            if ($bookingCancellationId) {
                $bcan['bcan_id']=$bookingCancellationId;
                $bcan['service_status']=4;
                $updateBookingCancellationId = $objectModelBookingDetails->updateBookingById($booking_id,$bcan);


//
                //logic for changing customer status

//            $cdata['customer_status']=0;
//            $bookingData = $objectModelBookingDetails->getBookingDetailsById($booking_id);
//            $objectModelCustomerDetails = Customer::getInstance();
//            $updateCustomerStatus= $objectModelCustomerDetails->updateCustomerById($cdata,$bookingData->customer_id);

           //FREE THE EXECUTIVE

                $EVdata['service_status']=1;
                $objectModelExecutiveVehicleDetails = ExecutiveVehicle::getInstance();
                $updateEVDetailsById=$objectModelExecutiveVehicleDetails->updateExecutiveWithVehicleById($bookingData->executive_id,$EVdata);




                $response->code = 200;
                $response->message = "Oder Cancelled successfully";
                $response->data =$updateBookingCancellationId ;
                echo json_encode($response);

            } else {

                $response->code = 400;
                $response->message = "Could not cancel the order";
                $response->data = null;
                echo json_encode($response);


            }

        }

    }



    public function bookingFeedback(Request $request)
    {


        if ($request->isMethod('POST')) {


            $response = new stdClass();
            $booking_id= $request->input('booking_id');
            $data['booking_id']= $request->input('booking_id');
            $data['stars']= $request->input('stars');
            $data['comments'] = $request->input('comments');


            $objectModelFeedbackDetails = Feedback::getInstance();
            $objectModelBookingDetails = Booking::getInstance();

            $bookingFeedbackId = $objectModelFeedbackDetails->addBookingFeedback($data);

            if ($bookingFeedbackId) {
                $feed['feedback_id']=$bookingFeedbackId;
                $updateBookingFeedbackId = $objectModelBookingDetails->updateBookingById($booking_id,$feed);


                $response->code = 200;
                $response->message = "Feedback saved";
                $response->data =$updateBookingFeedbackId ;
                echo json_encode($response);

            } else {

                $response->code = 400;
                $response->message = "Could not save feedback";
                $response->data = null;
                echo json_encode($response);


            }

        }

    }




    public function serviceCompleted(Request $request)
    {


        if ($request->isMethod('POST')) {


            $response = new stdClass();
            $booking_id= $request->input('booking_id');
            $data['service_status']=2;
            $EVdata['service_status']=1;

            $objectModelBookingDetails = Booking::getInstance();
            $bookingData = $objectModelBookingDetails->getBookingDetailsById($booking_id);
            if($bookingData){


                $updateBookingDetailsById = $objectModelBookingDetails->updateBookingById($booking_id,$data);

                $objectModelExecutiveVehicleDetails = ExecutiveVehicle::getInstance();
                $updateEVDetailsById=$objectModelExecutiveVehicleDetails->updateExecutiveWithVehicleById($bookingData->executive_id,$EVdata);


                $response->code = 200;
                $response->message = "Service Completed Successfully";
                echo json_encode($response);


            }else{

                $response->code = 400;
                $response->message = "No booking data found";
                echo json_encode($response);

            }






        }

    }




    public function delivered(Request $request)
    {


        if ($request->isMethod('POST')) {


            $response = new stdClass();
            $booking_id= $request->input('booking_id');
            $objectModelBookingDetails = Booking::getInstance();
            $bookingData = $objectModelBookingDetails->getBookingDetailsById($booking_id);

            if($bookingData){


                if($bookingData->final_price == $bookingData->payment_received){

                    $data['service_status']=3;
                    $updateBookingDetailsById = $objectModelBookingDetails->updateBookingById($booking_id,$data);

                    $response->code = 200;
                    $response->message = "Service Delivered Successfully";
                    echo json_encode($response);

                }else{


                    $response->code = 400;
                    $response->message = "Payment yet to be received";
                    echo json_encode($response);


                }

            }else{

                $response->code = 400;
                $response->message = "No Booking data found";
                echo json_encode($response);

            }

        }

    }







    public function executiveBookingHistory(Request $request)
    {


        if ($request->isMethod('POST')) {


            $response = new stdClass();
            $executive_id= $request->input('executive_id');

            $objectModelBookingDetails = Booking::getInstance();
            $objectModelExecutiveDetails = Executive::getInstance();
           // $objectModelVehicleDetails = Vehicle::getInstance();
            $objectModelCustomerDetails = Customer::getInstance();

            $bookingDetailsByExecutiveId = $objectModelBookingDetails->getBookingDetailsByExecutiveId($executive_id);
            if($bookingDetailsByExecutiveId){

              //  print_r($bookingDetailsByExecutiveId) ;

                $data=array();

                foreach ($bookingDetailsByExecutiveId as $key=>$value){

                    $sata=array();
                    $customer_id= $value->customer_id;
                    $customerDataById=$objectModelCustomerDetails->getCustomerById($customer_id);
              //       print_r($customer_id);

                    $sata['booking_id']=$value->booking_id;
                    $sata['booking_time']=$value->booking_time;
                    $sata['booking_type']=$value->booking_type;
                    $sata['service_status']=$value->service_status;
                    $sata['customer_address']=$value->customer_address;
                    $sata['customer_longitude']=$value->customer_longitude;
                    $sata['customer_latitude']=$value->customer_latitude;
                    $sata['estimated_count']=$value->estimated_count;
                    $sata['payment_received']=$value->payment_received;
                    $sata['service_started']=$value->service_started;

                    $sata['customer_name']=$customerDataById->customer_name;
                    $sata['customer_phone_no']=$customerDataById->customer_phone_no;


                    $data[]=$sata;

                }

                    //print_r($data);


                    $response->code = 200;
                    $response->message = "Executive Booking History";
                    $response->data = $data;
                    echo json_encode($response);

            }else{


                $response->code = 400;
                $response->mesage = "No Booking Data Found";
                $response->data = null;
                echo json_encode($response);

            }


        }


    }




    public function ifServiceCompleted(Request $request)
    {


        if ($request->isMethod('POST')) {


            $response = new stdClass();
            $booking_id= $request->input('booking_id');

            $objectModelBookingDetails = Booking::getInstance();

            $BookingDetailsById = $objectModelBookingDetails->getBookingDetailsById($booking_id);
            if($BookingDetailsById) {

                   $service_status= $BookingDetailsById->service_status;

                   if($service_status == 1){

                        $response->code = 200;
                        $response->message = "Service Can Be Completed";
                        echo json_encode($response);

                   }elseif ($service_status == 2){

                        $response->code = 400;
                        $response->message = "Service Already Completed";
                        echo json_encode($response);
                   }else{

                         $response->code = 400;
                         $response->message = "Can Not Be Completed At this stage";
                         echo json_encode($response);
                   }


            }else{

                $response->code = 400;
                $response->message = "No booking data found";
                echo json_encode($response);

            }


        }

    }






    public function ifServiceDelivered(Request $request)
    {


        if ($request->isMethod('POST')) {


            $response = new stdClass();
            $booking_id= $request->input('booking_id');

            $objectModelBookingDetails = Booking::getInstance();

            $BookingDetailsById = $objectModelBookingDetails->getBookingDetailsById($booking_id);
            if($BookingDetailsById) {

                $service_status= $BookingDetailsById->service_status;

                if($service_status == 2){

                    $response->code = 200;
                    $response->message = "Service Can Be Completed";
                    echo json_encode($response);

                }elseif ($service_status == 3){

                    $response->code = 400;
                    $response->message = "Service Already Completed";
                    echo json_encode($response);
                }else{

                    $response->code = 400;
                    $response->message = "Can Not Be Completed At this stage";
                    echo json_encode($response);
                }


            }else{

                $response->code = 400;
                $response->message = "No booking data found";
                echo json_encode($response);

            }

//
        }

    }



    public function checkServiceStatus(Request $request)
    {


        if ($request->isMethod('POST')) {


            $response = new stdClass();
            $booking_id = $request->input('booking_id');

            $objectModelBookingDetails = Booking::getInstance();

            $BookingDetailsById = $objectModelBookingDetails->getBookingDetailsById($booking_id);
            if ($BookingDetailsById) {

                $service_status = $BookingDetailsById->service_status;

                if ($service_status == 4) {

                    $response->code = 400;
                    $response->message = "This Booking has been canceled by the customer";
                    $response->service_status = $service_status;
                    echo json_encode($response);


                } else {

                    $response->code = 400;
                    $response->message = "The service stage is " . $service_status;
                    $response->service_status = $service_status;
                    echo json_encode($response);
                }


            } else {

                $response->code = 400;
                $response->message = "No booking data found";
                echo json_encode($response);

            }


        }

    }



    public function acceptPayment(Request $request)
    {


        if ($request->isMethod('POST')) {


            $response = new stdClass();
            $booking_id= $request->input('booking_id');
            $payment_status= $request->input('payment_status');
            $payment_mode= $request->input('payment_mode');


            $objectModelBookingDetails = Booking::getInstance();
            $BookingDetailsById = $objectModelBookingDetails->getBookingDetailsById($booking_id);


            if($BookingDetailsById){



                if($payment_status){


                    $finalPrice=$BookingDetailsById->final_price;
                    $data['payment_received']=$finalPrice;
                    $data['payment_mode']=$payment_mode;
                    $updateBookingDetailsById = $objectModelBookingDetails->updateBookingById($booking_id,$data);

                    $BookingData = $objectModelBookingDetails->getBookingDetailsById($booking_id);

                    if($BookingData->payment_received != null){

                        $response->code = 200;
                        $response->message = "Payment Received Updated Successfully";
                        echo json_encode($response);

                    }
                    else{

                        $response->code = 400;
                        $response->message = "Payment received not updated";
                        echo json_encode($response);

                    }



                }else{


                    $response->code = 400;
                    $response->message = "Payment could not be received";
                    echo json_encode($response);


                }


            }else{


                $response->code = 400;
                $response->message = "No booking data found";
                echo json_encode($response);


            }

        }

    }




}
