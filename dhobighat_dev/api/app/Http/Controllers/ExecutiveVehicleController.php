<?php

namespace App\Http\Controllers;

use App\Executive;
use App\Vehicle;
use App\ExecutiveVehicle;
use Illuminate\Http\Request;

use App\Http\Requests;
use stdClass;

class ExecutiveVehicleController extends Controller
{


    public function updateCurrentLocation(Request $request){

        $response = new stdClass();

        if ($request->isMethod('POST')) {
            $executive_id = $request->input('executive_id');
            $vehicle_no = $request->input('vehicle_no');

            $data=array();

            $data['current_address'] = $request->input('current_address');
            $data['current_longitude'] = $request->input('current_longitude');
            $data['current_latitude'] = $request->input('current_latitude');


            $objectModelExecutiveVehicle = ExecutiveVehicle::getInstance();

            if($executive_id || $vehicle_no){

                $checkVehicleByExecutive= $objectModelExecutiveVehicle->checkVehicleByExecutiveVehicleId($executive_id,$vehicle_no);

               //print_r($checkVehicleByExecutive);
               //die();

                if($checkVehicleByExecutive){



                        $updateLocationOfExecutiveWithVehicle=$objectModelExecutiveVehicle->updateExecutiveWithVehicleById($executive_id,$data);




                        $response->code = 200;
                        $response->message = "Location Updated successfully";
                        echo json_encode($response);



                }
            }

        }
    }

        public function getAllExecutiveVehicleData(){

            $response =new stdClass();
            $objectModelExecutiveVehicle = ExecutiveVehicle::getInstance();
            $getAllExecutiveVehicleData= $objectModelExecutiveVehicle->getAllExecutiveVehicleDetails();
             if($getAllExecutiveVehicleData){



                 $response->code = 200;
                 $response->message ='Executive vehicle data';
                 $response->data = $getAllExecutiveVehicleData;
                 echo json_encode($response);


             }
             else{


                 $response->code = 400;
                 $response->message ='No Executive available';
                 $response->data = null;
                 echo json_encode($response);

             }

         }



}