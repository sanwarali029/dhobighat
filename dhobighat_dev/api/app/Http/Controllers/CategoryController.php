<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use stdClass;
use App\Category;
use App\Price;

class CategoryController extends Controller
{
    //

    public function getCategoryPriceData(){


        $response =new stdClass();
        $objectModelCategory = Category::getInstance();
        $getCategoryPriceData= $objectModelCategory->getCategoryPriceData();

        $objectModelPrice = Price::getInstance();
        $getPriceDetails= $objectModelPrice->getPriceDetails();


        if($getCategoryPriceData){



            $response->code = 200;
            $response->message ='Executive Price Data';
            $response->category_data = $getCategoryPriceData;
            $response->price_data = $getPriceDetails;
            echo json_encode($response);


        }
        else{


            $response->code = 400;
            $response->message ='No Categories available';
            $response->data = null;
            echo json_encode($response);

        }

    }

}
