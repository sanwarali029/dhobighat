<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});


// Customer

Route::resource('/customer-sign-up','CustomerController@customerSignUp');
Route::resource('/customer-log-in','CustomerController@customerLogIn');
Route::resource('/forgot-password','EmailController@forgotPassword');
Route::resource('/get-customer-data','CustomerController@getCustomerData');
Route::resource('/update-customer-token','CustomerController@updateCustomerToken');
Route::resource('/update-customer-profile','CustomerController@updateCustomerProfile');

//Executive


Route::resource('/executive-check-logged-in','ExecutiveController@checkLoggedIn');
Route::resource('/executive-log-in','ExecutiveController@executiveLogIn');
Route::resource('/executive-log-out','ExecutiveController@executiveLogOut');
Route::resource('/update-executive-token','ExecutiveController@updateExecutiveToken');
Route::resource('/executive-booking-history','BookingController@executiveBookingHistory');


//Executive Vehicle

Route::resource('/update-current-location','ExecutiveVehicleController@updateCurrentLocation');
Route::resource('/executive-vehicle-data','ExecutiveVehicleController@getAllExecutiveVehicleData');
Route::resource('/data','BookingController@data');

//booking cancellation

Route::resource('/cancellation-data','BookingController@cancellationData');
Route::resource('/cancel-order','BookingController@cancelOrder');

//feedback

Route::resource('/booking-feedback','BookingController@bookingFeedback');

//category


Route::resource('/category-price-data','CategoryController@getCategoryPriceData');

//booking stages
Route::resource('/if-service-completed','BookingController@ifServiceCompleted');
Route::resource('/if-service-Delivered','BookingController@ifServiceDelivered');
Route::resource('/check-service-status','BookingController@checkServiceStatus');
Route::resource('/service-completed','BookingController@serviceCompleted');
Route::resource('/service-delivered','BookingController@delivered');
Route::resource('/accept-payment','BookingController@acceptPayment');
Route::resource('/start-service','BookingController@startService');


//booking

Route::resource('/add-booking','BookingController@addBooking');
Route::resource('/get-executive-booking-notification-list','BookingController@getExecutiveBookingNotificationList');
Route::resource('/cancel-new-booking','BookingController@cancelNewBooking');
Route::resource('/get-booking-details-to-serve','BookingController@getBookingDetailsToServe');
Route::resource('/serve-booking-request','BookingController@serveBookingRequest');
Route::resource('/check-request-acceptance','BookingController@checkRequestAcceptance');
Route::resource('/update-no-one-accepted','BookingController@updateNoOneAccepted');
Route::resource('/get-booking-category-data','BookingController@getBookingCategoryData');
Route::resource('/update-booking-category-data-customer','BookingController@updateBookingCategoryDataCustomer');
Route::resource('/update-booking-category-data-executive','BookingController@updateBookingCategoryDataExecutive');
Route::resource('/get-booking-data','BookingController@getBookingData');
