<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class BookingCategory extends Model {

    //

    protected $table = 'booking_category_details';
    private static $_instance = null;


    public static function getInstance()
    {
        if (!is_object(self::$_instance))  //or if( is_null(self::$_instance) ) or if( self::$_instance == null )
            self::$_instance = new BookingCategory();
        return self::$_instance;
    }


    /**
     * @return string
     */
    public function addBookingCategoryDetails()
    {

        if (func_num_args() > 0) {
            $BookingCategoryData = func_get_arg(0);
            try {
                $result = DB::table($this->table)
                    ->insertGetId($BookingCategoryData);
                return $result;
            } catch (\Exception $e) {
                return $e->getMessage();
            }
        } else {
            throw new Exception('Argument Not Passed');
        }
    }


    public function deleteBookingCategoriesById(){


        if (func_num_args() > 0) {
            $booking_id = func_get_arg(0);
            try {
                $result = DB::table($this->table)
                    ->where('booking_id', $booking_id)
                    ->delete();
            } catch (\Exception $e) {
                return $e->getMessage();
            }
            if ($result) {
                return $result;
            } else {
                return 0;
            }
        } else {
            throw new Exception('Argument Not Passed');
        }


    }



    public function getBookingCategoryDetailsByBookingId()
    {


        if (func_num_args() > 0) {
            $booking_id = func_get_arg(0);
            try {
                $result = DB::table($this->table)
                    ->where('booking_id', $booking_id)
                    ->get();
            } catch (\Exception $e) {
                return $e->getMessage();
            }
            if ($result) {
                return $result;
            } else {
                return 0;
            }
        } else {
            throw new Exception('Argument Not Passed');
        }

    }


    public function updateBookingCategoryDetails(){


        if (func_num_args() > 0) {
            $booking_id = func_get_arg(0);
            $id = func_get_arg(1);
            $data =func_get_arg(2);



            try {
                $result = DB::table($this->table)
                    ->where('booking_id', $booking_id)
                    ->where('category_id', $id)
                    ->update($data);
            } catch (\Exception $e) {
                return $e->getMessage();
            }
            if ($result) {
                return $result;
            } else {
                return 0;
            }
        } else {
            throw new Exception('Argument Not Passed');
        }


    }


}
