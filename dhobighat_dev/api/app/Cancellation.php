<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Cancellation extends Model
{
    //

    protected $table='cancellation_details';
    private static $_instance = null;

    public static function getInstance()
    {
        if (!is_object(self::$_instance))  //or if( is_null(self::$_instance) ) or if( self::$_instance == null )
            self::$_instance = new Cancellation();
        return self::$_instance;
    }


    public function getCancellationDetails(){

        $result = DB::table($this->table)
            ->where('cancellation_status','0')
            ->get();

        if ($result) {
            return $result;
        } else {
            return 0;
        }

    }

}
