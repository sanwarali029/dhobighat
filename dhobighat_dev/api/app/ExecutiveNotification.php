<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class ExecutiveNotification extends Model
{
    //

    protected $table='executive_notification';
    private static $_instance = null;

    public static function getInstance()
    {
        if (!is_object(self::$_instance))  //or if( is_null(self::$_instance) ) or if( self::$_instance == null )
            self::$_instance = new ExecutiveNotification();
        return self::$_instance;
    }


    public function addNewNotification(){

        if (func_num_args() > 0) {
            $data = func_get_arg(0);
            try {
                $result = DB::table($this->table)
                    ->insertGetId($data);
                return $result;
            } catch (\Exception $e) {
                return $e->getMessage();
            }
        } else {
            throw new Exception('Argument Not Passed');
        }

    }


    public function getNotificationDataById(){



        if (func_num_args() > 0) {
            $exe_notification_id = func_get_arg(0);
            try {
                $result = DB::table($this->table)
                    ->where('exe_notification_id',$exe_notification_id)
                     ->first();
                return $result;
            } catch (\Exception $e) {
                return $e->getMessage();
            }
        } else {
            throw new Exception('Argument Not Passed');
        }
    }



    public function deleteExecutiveNotificationsByBookingId(){


        if (func_num_args() > 0) {
            $booking_id = func_get_arg(0);
            try {
                $result = DB::table($this->table)
                    ->where('booking_id', $booking_id)
                    ->delete();
            } catch (\Exception $e) {
                return $e->getMessage();
            }
            if ($result) {
                return $result;
            } else {
                return 0;
            }
        } else {
            throw new Exception('Argument Not Passed');
        }


    }


    public function getBookingIdsByExecutiveId(){



        if (func_num_args() > 0) {
            $executive_id = func_get_arg(0);
            try {
                $result = $titles = DB::table($this->table)
                    ->where('executive_id',$executive_id)
                    ->pluck('booking_id');
                if($result) {
                    return $result;
                }
                else{
                    return 0;
                }

            } catch (\Exception $e) {
                return $e->getMessage();
            }
        } else {
            throw new Exception('Argument Not Passed');
        }
    }




    public function updateExecutiveNotificationData(){


        if (func_num_args() > 0) {
            $booking_id = func_get_arg(0);
            $executive_id = func_get_arg(1);
            $data = func_get_arg(2);



            try {
                $result = DB::table($this->table)
                    ->where('booking_id', $booking_id)
                    ->where('executive_id', $executive_id)
                    ->update($data);
            } catch (\Exception $e) {
                return $e->getMessage();
            }
            if ($result) {
                return $result;
            } else {
                return 0;
            }
        } else {
            throw new Exception('Argument Not Passed');
        }


    }

}
