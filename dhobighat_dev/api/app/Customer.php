<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Customer extends Model
{
    //
    protected $table='customer_details';
    private static $_instance = null;

    public static function getInstance()
    {
        if (!is_object(self::$_instance))  //or if( is_null(self::$_instance) ) or if( self::$_instance == null )
            self::$_instance = new Customer();
        return self::$_instance;
    }


    public function addCustomer()
    {
        if (func_num_args() > 0) {
            $customerData = func_get_arg(0);
            try {
                $result = DB::table($this->table)
                    ->insertGetId($customerData);
                return $result;
            } catch (\Exception $e) {
                return $e->getMessage();
            }
        } else {
            throw new Exception('Argument Not Passed');
        }
    }

    //
    public function getCustomerById($customerDataById){

            $result = DB::table($this->table)
                ->where('customer_id',$customerDataById)
                ->first();

            if ($result) {
                return $result;
            } else {
                return 0;
            }

    }

    public function checkCustomerByEmail($customerDataById){

        $result = DB::table($this->table)
            ->where('customer_email',$customerDataById)
            ->first();

        if ($result) {
            return $result;
        } else {
            return 0;
        }

    }


    public function checkCustomerByPhone($customerDataById){

        $result = DB::table($this->table)
            ->where('customer_phone_no',$customerDataById)
            ->first();

        if ($result) {
            return $result;
        } else {
            return 0;
        }

    }




    public function getUsersLoginWithPhone($phone, $password)
    {

        $result = DB::table($this->table)
            ->where('customer_phone_no', $phone)
            ->where('customer_password', $password)
            ->first();

        if ($result) {
            return $result;
        } else {
            return 0;
        }

    }


    public function getUsersLoginWithEmail($email, $password)
    {

        $result = DB::table($this->table)
            ->where('customer_email', $email)
            ->where('customer_password', $password)
            ->first();

        if ($result) {
            return $result;
        } else {
            return 0;
        }

    }


    /**
     * @return string
     */
    public function updateCustomerById()
    {



        if (func_num_args() > 0) {
            $data = func_get_arg(0);
            $customerById = func_get_arg(1);

            try {
                $result = DB::table($this->table)
                    ->where('customer_id', $customerById)
                    ->update($data);
            } catch (\Exception $e) {
                return $e->getMessage();
            }
            if ($result) {
                return $result;
            } else {
                return 0;
            }
        } else {
            throw new Exception('Argument Not Passed');
        }

    }


}
