<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class BookingCancellation extends Model
{
    //

    protected $table='booking_cancellation_details';
    private static $_instance = null;

    public static function getInstance()
    {
        if (!is_object(self::$_instance))  //or if( is_null(self::$_instance) ) or if( self::$_instance == null )
            self::$_instance = new BookingCancellation();
        return self::$_instance;
    }


//    public function getCancellationDetails(){
//
//        $result = DB::table($this->table)
//            ->get();
//
//        if ($result) {
//            return $result;
//        } else {
//            return 0;
//        }
//
//    }



    public function addBookingCancellation()
    {

        if (func_num_args() > 0) {
            $data = func_get_arg(0);
            try {
                $result = DB::table($this->table)
                    ->insertGetId($data);
                return $result;
            } catch (\Exception $e) {
                return $e->getMessage();
            }
        } else {
            throw new Exception('Argument Not Passed');
        }
    }



}
