<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    protected $table='category_details';
    private static $_instance = null;

    public static function getInstance()
    {
        if (!is_object(self::$_instance))  //or if( is_null(self::$_instance) ) or if( self::$_instance == null )
            self::$_instance = new Category();
        return self::$_instance;
    }


    public function getCategoryPriceData(){

        $result = DB::table($this->table)
            ->where('category_status', 1)
            ->get();

        if ($result) {
            return $result;
        } else {
            return 0;
        }

    }


    public function getCategoryDetailsById()
    {

        if (func_num_args() > 0) {
            $category_id = func_get_arg(0);
            try {
                $result = DB::table($this->table)
                    ->where('category_id',$category_id)
                    ->first();

                if($result) {
                    return $result;
                }else{
                    return 0;
                }
            } catch (\Exception $e) {
                return $e->getMessage();
            }
        } else {
            throw new Exception('Argument Not Passed');
        }
    }






}

