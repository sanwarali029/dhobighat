<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Executive extends Model
{
    //

    protected $table='executive_details';
    private static $_instance = null;

    public static function getInstance()
    {
        if (!is_object(self::$_instance))  //or if( is_null(self::$_instance) ) or if( self::$_instance == null )
            self::$_instance = new Executive();
        return self::$_instance;
    }

    public function getExecutiveLogin($executive_id, $executive_password)
    {

        $result = DB::table($this->table)
            ->where('executive_custom_id', $executive_id)
            ->where('executive_password', $executive_password)
            ->where('executive_status', 1)
            ->first();

        if ($result) {
            return $result;
        } else {
            return 0;
        }

    }

    public function getExecutiveData($executive_id)
    {

        $result = DB::table($this->table)
            ->where('executive_id', $executive_id)
            ->first();

        if ($result) {
            return $result;
        } else {
            return 0;
        }

    }


    public function updateExecutiveById(){


        if (func_num_args() > 0) {
            $executive_id = func_get_arg(0);
            $data = func_get_arg(1);



            try {
                $result = DB::table($this->table)
                    ->where('executive_id', $executive_id)
                    ->update($data);
            } catch (\Exception $e) {
                return $e->getMessage();
            }
            if ($result) {
                return $result;
            } else {
                return 0;
            }
        } else {
            throw new Exception('Argument Not Passed');
        }


    }

//    public function updateLastLoggedInVehicle(){
//
//
//        if (func_num_args() > 0) {
//            $executive_custom_id = func_get_arg(0);
//            $data = func_get_arg(1);
//
//
//
//            try {
//                $result = DB::table($this->table)
//                    ->where('executive_custom_id', $executive_custom_id)
//                    ->update($data);
//            } catch (\Exception $e) {
//                return $e->getMessage();
//            }
//            if ($result) {
//                return $result;
//            } else {
//                return 0;
//            }
//        } else {
//            throw new Exception('Argument Not Passed');
//        }
//
//
//    }
//


}
