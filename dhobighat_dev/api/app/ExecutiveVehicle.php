<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class ExecutiveVehicle extends Model
{
    //
    protected $table = 'executive_vehicle_login';
    private static $_instance = null;




    public static function getInstance()
    {
        if (!is_object(self::$_instance))  //or if( is_null(self::$_instance) ) or if( self::$_instance == null )
            self::$_instance = new ExecutiveVehicle();
        return self::$_instance;
    }

public function addVehicleByExecutiveId(){



        if (func_num_args() > 0) {

            $data = func_get_arg(0);

            try {
                $result = DB::table($this->table)
                    ->insertGetId($data);
                return $result;
            } catch (\Exception $e) {
                return $e->getMessage();
            }
        } else {
            throw new Exception('Argument Not Passed');
        }


     }


     public function getExecutiveVehicleDetailsById($executive_id)
     {


         $result = DB::table($this->table)
             ->where('executive_id', $executive_id)
             ->first();

         if ($result) {
             return $result;
         } else {
             return 0;
         }


     }

    public function checkVehicleByExecutiveVehicleId($executive_id,$vehicle_no)
    {

        $result = DB::table($this->table)
            ->where('executive_id', $executive_id)
            ->where('vehicle_registration_no', $vehicle_no)
            ->first();

        if ($result) {
            return $result;
        } else {
            return 0;
        }



    }


public function updateExecutiveWithVehicleById(){


    if (func_num_args() > 0) {
        $executive_id = func_get_arg(0);
        $data = func_get_arg(1);



        try {
            $result = DB::table($this->table)
                ->where('executive_id', $executive_id)
                ->update($data);

        } catch (\Exception $e) {
            return $e->getMessage();
        }
        if ($result) {
            return $result;
        } else {
            return 0;
        }
    } else {
        throw new Exception('Argument Not Passed');
    }


  }


  public function getAllExecutiveVehicleDetails(){


          $result = DB::table($this->table)
              ->where('service_status', 1)
              ->get();

          if ($result) {
              return $result;
          } else {
              return 0;
          }

  }


  public function deleteExecutiveVehicle(){


      if (func_num_args() > 0) {
          $executive_id = func_get_arg(0);
          $vehicle_registration_no = func_get_arg(1);



          try {
              $result = DB::table($this->table)
                  ->where('executive_id', $executive_id)
                  ->where('vehicle_registration_no', $vehicle_registration_no)
                  ->delete();
          } catch (\Exception $e) {
              return $e->getMessage();
          }
          if ($result) {
              return $result;
          } else {
              return 0;
          }
      } else {
          throw new Exception('Argument Not Passed');
      }


  }




}

