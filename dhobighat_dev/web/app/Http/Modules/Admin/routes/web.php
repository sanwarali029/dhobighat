<?php

Route::group(array('module' => 'AdminDetails', 'middleware' => ['web'], 'namespace' => 'App\Http\Modules\Admin\Controllers'), function() {


    //dashboard
    Route::resource('/admin/overview', 'DashboardController@overview');
    Route::resource('/admin/search-newbooking', 'DashboardController@searchNewBooking');
    Route::resource('/admin/view-booking', 'DashboardController@viewBooking');
    Route::resource('/admin/dashboard-ajax-handler', 'DashboardController@dashboardAjaxHandler');
    Route::resource('/admin/dashboard-details-ajax-handler', 'DashboardController@dashboardDetailsAjaxHandler');

    //executive
    Route::resource('/admin/executive', 'ExecutiveController@executive');
    Route::resource('/admin/add-executive', 'ExecutiveController@addExecutive');
    Route::resource('/admin/edit-executive', 'ExecutiveController@editExecutive');
    Route::resource('/admin/delete-executive', 'ExecutiveController@deleteExecutive');
    Route::resource('/admin/executive-ajax-handler', 'ExecutiveController@executiveAjaxHandler');
    Route::resource('/admin/executive-details-ajax-handler', 'ExecutiveController@executiveDetailsAjaxHandler');


    //promotion
    Route::resource('/admin/promotions', 'PromotionsController@promotions');
    Route::resource('/admin/add-promotions', 'PromotionsController@addPromotions');
    Route::resource('/admin/edit-promotions', 'PromotionsController@editPromotions');
    Route::resource('/admin/delete-promotions', 'PromotionsController@deletePromotions');
    Route::resource('/admin/promotions-ajax-handler', 'PromotionsController@promotionsAjaxHandler');

//booking
    Route::resource('/admin/booking', 'BookingController@booking');
    Route::resource('/admin/add-booking', 'BookingController@addBooking');
    Route::resource('/admin/search-booking', 'BookingController@searchBooking');
    Route::resource('/admin/update-booking', 'BookingController@updateBooking');
    Route::resource('/admin/booking-ajax-handler', 'BookingController@bookingAjaxHandler');
    Route::resource('/admin/add-booking-column', 'BookingController@addBookingColumn');



    //vehicle
    Route::resource('/admin/vehicle', 'VehicleController@vehicle');
    Route::resource('/admin/add-vehicle', 'VehicleController@addVehicle');
    Route::resource('/admin/edit-vehicle', 'VehicleController@editVehicle');
    Route::resource('/admin/delete-vehicle', 'VehicleController@deleteVehicle');
    Route::resource('/admin/vehicle-ajax-handler', 'VehicleController@vehicleAjaxHandler');
    Route::resource('/admin/vehicle-details-ajax-handler', 'VehicleController@vehicleDetailsAjaxHandler');

    //business
    Route::resource('/admin/business', 'BusinessController@business');
    Route::resource('/admin/set-price', 'BusinessController@setPrice');
    Route::resource('/admin/add-category', 'BusinessController@addCategory');
    Route::resource('/admin/edit-category', 'BusinessController@editCategory');
    Route::resource('/admin/delete-category', 'BusinessController@deleteCategory');
    Route::resource('/admin/business-ajax-handler', 'BusinessController@businessAjaxHandler');
    Route::resource('/admin/business-details-ajax-handler', 'BusinessController@businessDetailsAjaxHandler');
    //price

    Route::resource('/admin/update-price', 'BusinessController@updatePrice');

    Route::resource('/admin/delete-cancellation', 'CancellationController@deleteCancellation');

    //feedback
    Route::resource('/admin/feedbacks', 'FeedbackController@feedbacks');
    Route::resource('/admin/feedback-ajax-handler', 'FeedbackController@feedbackAjaxHandler');


    //cancellation
    Route::resource('/admin/cancellation', 'CancellationController@cancellation');
    Route::resource('/admin/cancellation-ajax-handler', 'CancellationController@cancellationAjaxHandler');
    Route::resource('/admin/cancellation-details-ajax-handler', 'CancellationController@cancellationDetailsAjaxHandler');
    Route::resource('/admin/add-cancellation', 'CancellationController@addCancellation');
    Route::resource('/admin/edit-cancellation', 'CancellationController@editCancellation');


//AdminDetails
    Route::resource('/admin/forgot', 'AdminController@forgot');
    Route::resource('/admin', 'AdminController@login');
    Route::resource('/admin/login-auth', 'AdminController@loginAuth');
    Route::resource('/admin/profile', 'AdminController@profile');
    Route::resource('/admin/index', 'AdminController@index');



});	
