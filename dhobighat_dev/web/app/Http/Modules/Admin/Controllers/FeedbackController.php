<?php namespace App\Http\Modules\Admin\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Modules\Admin\Models\ExecutiveDetails;
use App\Http\Modules\Admin\Models\BookingDetails;
use App\Http\Modules\Admin\Models\CustomerDetails;
use App\Http\Modules\Admin\Models\VehicleDetails;
use App\Http\Modules\Admin\Models\FeedbackDetails;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
class FeedbackController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

    public function feedbacks()
    {


     $objModelFeedback = FeedbackDetails::getInstance();

       $result = $objModelFeedback->getFeedbackDetails();
       // $total_rejection=count($result1);

        $total_served=0;
        $avg_rating=0;
        if(is_array($result))
        {
            $total_served=count(array_filter($result));
            $total_stars=0;
            foreach ($result as $key=>$value){
                $stars=$value ->stars;
                $total_stars=$total_stars+$stars;
            }

            $avg_rating=round($total_stars/$total_served,1);;

        }


         return view("Admin::feedbacks",['total_served' => $total_served, 'avg_rating' => $avg_rating]);
         // return view("Admin::feedbacks");
    }


//    public function searchNewBooking()
//    {
//        return 'searchNewBooking';
//    }
//
//    public function viewBooking()
//    {
//        return 'viewBooking';
//    }

    public function feedbackAjaxHandler()
    {
       // return 'dashboardAjaxHandler';

//        $objModelBooking = BookingDetails::getInstance();
//        $objModelCustomer = CustomerDetails::getInstance();
//        $objModelVehicle = VehicleDetails::getInstance();
         $objModelFeedback = FeedbackDetails::getInstance();
        $result = $objModelFeedback->getFeedbackDetails();
        $result = json_decode(json_encode($result), true);
        $feedbackDetails = new Collection();

        foreach ($result as $res) {

            $booking_id = $res['booking_id'];
            $feedback_id = $res['feedback_id'];
            $stars = $res['stars'];
            $comments = $res['comments'];



            $feedbackDetails->push([
                'booking_id' => $res['booking_id'],
                'feedback_id' => $res['feedback_id'],
                'stars' => $res['stars'],
                'comments' => $res['comments'],


                'action' => '<button data-toggle="modal" id="viewFeedback" data-id=' . $feedback_id . '
                                class="btn btn-primary viewFeedback">
                                <i class="fa fa-info"></i></button>',

            ]);

        }
        return Datatables::of($feedbackDetails)->make(true);


    }

}
