<?php namespace App\Http\Modules\Admin\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Modules\Admin\Models\CategoryDetails;
use App\Http\Modules\Admin\Models\PriceDetails;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class BusinessController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

    public function business()
    {
        return view("Admin::business");
    }



    public function addCategory(Request $request)
    {


        if ($request->isMethod('POST')) {

            $objectModelCategory= CategoryDetails::getInstance();



//            $this->validate($request, [
//                'executive_email' => 'required|unique:executive_details,executive_email',
//                'executive_mobile_no' => 'required|numeric|regex:/^[0-9]{10}$/|unique:executive_details,executive_mobile_no',
//                'executive_custom_id' => 'required|unique:executive_details,executive_custom_id',
//                'executive_driving_licence_no' => 'required|unique:executive_details,executive_driving_licence_no',
//                'executive_date_of_birth' => 'date|before:"1999-08-08 00:00:00"',
//                'executive_name' => 'required|regex:/^[\pL\s]+$/u',
//
//            ]);



            //new executive
            $data = array();

            $data['category_name'] = $request->input('category_name');
//            $data['category_icon_black'] = $request->input('category_icon_black');
//            $data['category_icon_white'] = $request->input('category_icon_white');
            $data['category_average_weight_per_piece'] = $request->input('category_average_weight_per_piece');
            $data['category_average_price_per_kg'] = $request->input('category_average_price_per_kg');
            $data['category_status'] = 1;






            if ($request->category_icon_black || $request->category_icon_white) {

                $a="http://web.dhobighat.gq";

                if ($request->category_icon_black) {
                    $file = $request->category_icon_black;
                    $image_name = time() . "-" . $file->getClientOriginalName();
                    $res = $file->move(public_path() . '/assets/project_images/category/', $image_name);
                    $data['category_icon_black'] = $a . '/assets/project_images/category/' . $image_name;
                }

                if ($request->category_icon_white) {
                    $file = $request->category_icon_white;
                    $image_name = time() . "-" . $file->getClientOriginalName();
                    $res = $file->move(public_path() . '/assets/project_images/category/', $image_name);
                    $data['category_icon_white'] = $a . '/assets/project_images/category/' . $image_name;
                }

            }

         //   print_r($data);


            $addCategoryById = $objectModelCategory->addCategory($data);
            return redirect('/admin/business');

        }




    }

    public function editCategory(Request $request)
    {



        if ($request->isMethod('POST')) {

            $objectModelCategory= CategoryDetails::getInstance();



//            $this->validate($request, [
//                'executive_email' => 'required|unique:executive_details,executive_email',
//                'executive_mobile_no' => 'required|numeric|regex:/^[0-9]{10}$/|unique:executive_details,executive_mobile_no',
//                'executive_custom_id' => 'required|unique:executive_details,executive_custom_id',
//                'executive_driving_licence_no' => 'required|unique:executive_details,executive_driving_licence_no',
//                'executive_date_of_birth' => 'date|before:"1999-08-08 00:00:00"',
//                'executive_name' => 'required|regex:/^[\pL\s]+$/u',
//
//            ]);



            //new executive
            $data = array();

            $category_id= $request->input('hidden_category_id');
            $data['category_name'] = $request->input('edit_category_name');
//            $data['category_icon_black'] = $request->input('category_icon_black');
//            $data['category_icon_white'] = $request->input('category_icon_white');
            $data['category_average_weight_per_piece'] = $request->input('edit_category_average_weight_per_piece');
            $data['category_average_price_per_kg'] = $request->input('edit_category_average_price_per_kg');
            $data['category_status'] = 1;






            if ($request->edit_category_icon_black) {

                $a="http://web.dhobighat.gq";

                if ($request->edit_category_icon_black) {
                    $file = $request->edit_category_icon_black;
                    $image_name = time() . "-" . $file->getClientOriginalName();
                    $res = $file->move(public_path() . '/assets/project_images/category/', $image_name);
                    $data['category_icon_black'] = $a . '/assets/project_images/category/' . $image_name;
                }

            }



            if ($request->edit_category_icon_white) {

                $a="http://web.dhobighat.gq";


                if ($request->edit_category_icon_white) {
                    $file = $request->edit_category_icon_white;
                    $image_name = time() . "-" . $file->getClientOriginalName();
                    $res = $file->move(public_path() . '/assets/project_images/category/', $image_name);
                    $data['category_icon_white'] = $a . '/assets/project_images/category/' . $image_name;
                }

            }


             //  print_r($data);


            $updateCategoryById = $objectModelCategory->updateCategoryById($category_id,$data);
            return redirect('/admin/business');

        }


    }
    public function deleteCategory(Request $request)
    {




        $category_id = $request->input('delete_id');

        $data['category_status'] = 2;
        $objModelCategory = CategoryDetails::getInstance();
        $delete = $objModelCategory->updateCategoryById($category_id, $data);

        return redirect('/admin/business');




    }
    public function businessAjaxHandler()
    {
        //return 'businessAjaxHandler';

        $objModelCategory = CategoryDetails::getInstance();


        $result = $objModelCategory->getCategoryDetails();
        $result = json_decode(json_encode($result), true);
        $CategoryDetails = new Collection();

        foreach ($result as $res) {

//
            $category_id = $res['category_id'];
            $category_name = $res['category_name'];
            $category_average_weight_per_piece = $res['category_average_weight_per_piece'];
            $category_average_price_per_kg = $res['category_average_price_per_kg'];
            $category_icon_black = $res['category_icon_black'];
            $category_icon_white = $res['category_icon_white'];


            $CategoryDetails->push([
                'category_id' => $res['category_id'],
                'category_name' => $res['category_name'],
                'category_average_weight_per_piece' => $res['category_average_weight_per_piece'],
                'category_average_price_per_kg' => $res['category_average_price_per_kg'],
//                'category_icon_black' => $res['category_icon_black'],
//                'category_icon_white' => $res['category_icon_white'],
                'category_icon_black' => '<img src="' . $res['category_icon_black'] . '" height="50" width="50">',
                'category_icon_white' => '<img src="' . $res['category_icon_white'] . '" height="50" width="50">',




                'action' => '<button data-toggle="modal" id="editCategory" data-id=' . $category_id . '
                                class="btn btn-primary editCategory">
                                <i class="fa fa-pencil"></i></button>
                                <button data-id=' . $category_id . ' id="deleteCategory" data-toggle="modal"
                                class="btn btn-danger deleteCategory">
                                <i class="fa fa-trash"></i></button>',

            ]);

        }
        return Datatables::of($CategoryDetails)->make(true);


    }


    public function BusinessDetailsAjaxHandler(Request $request){

        $inputData = $request->input();
        $method = $inputData['method'];
        // print_r($inputData);

        switch ($method) {


            case 'getCategoryById':
                $category_id['category_id'] = $request->input('category_id');
                $objModelCategory = CategoryDetails::getInstance();
                $category = $objModelCategory->getCategoryById($category_id);

                echo json_encode($category);
                break;
            default:
                break;
        }

    }








    public function updatePrice(Request $request)
    {

        if ($request->isMethod('POST')) {

            // return 'add vehicle';

            $objectModelPrice= PriceDetails::getInstance();


            $data = array();

            $price_id = 1;
            $data['minimum_price'] = $request->input('minimum_price');
            $data['minimum_weight'] = $request->input('minimum_weight');

         //   print_r($data);

            $updatePriceById = $objectModelPrice->updatePriceById($price_id,$data);

            return redirect('/admin/business');

        }




    }









}
