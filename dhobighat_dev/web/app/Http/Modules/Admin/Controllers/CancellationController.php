<?php namespace App\Http\Modules\Admin\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Modules\Admin\Models\ExecutiveDetails;
use App\Http\Modules\Admin\Models\BookingDetails;
use App\Http\Modules\Admin\Models\CustomerDetails;
use App\Http\Modules\Admin\Models\VehicleDetails;
use App\Http\Modules\Admin\Models\FeedbackDetails;
use App\Http\Modules\Admin\Models\CancellationDetails;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
class CancellationController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

    public function cancellation()
    {


        $objModelBooking = BookingDetails::getInstance();
        $result = $objModelBooking->getBookingDetails();
        //  $total_orders=count($result);

        if(is_array($result))
        {
            $total_orders= count(array_filter($result));
        }
        else
        {
            $total_orders= 0;
        }



        $result1 = $objModelBooking->getRejectedBookingDetails();
        // $total_rejection=count($result1);

        if(is_array($result1))
        {
            $total_rejection=count(array_filter($result1));
        }
        else
        {
            $total_rejection= 0;
        }




        return view("Admin::cancellation",['total_orders' => $total_orders, 'total_rejected' => $total_rejection]);
        //  return view("Admin::cancellation");
    }


    public function addCancellation(Request $request)
    {

        if ($request->isMethod('POST')) {

            // return 'add vehicle';

            $objectModelCancellation= CancellationDetails::getInstance();
            $data = array();

            $data['reason'] = $request->input('reason');

            // print_r($data);

            $addCancellationById = $objectModelCancellation->addCancellationById($data);
//
////           print_r($addVehicleById);
////          die();
//
            return redirect('/admin/cancellation');

        }
    }



    public function editCancellation(Request $request)
    {

        if ($request->isMethod('POST')) {

            // return 'add vehicle';

            $objectModelCancellation= CancellationDetails::getInstance();
            $data = array();

            $data['reason'] = $request->input('edit_reason');
            $cancellation_id = $request->input('hidden_cancellation_id');

            // print_r($data);

            $updateCancellationById = $objectModelCancellation->updateCancellationById($cancellation_id,$data);
//
////           print_r($addVehicleById);
////          die();
//
            return redirect('/admin/cancellation');

        }
    }




    public function deleteCancellation(Request $request)
    {

        if ($request->isMethod('POST')) {

            // return 'add vehicle';

            $objectModelCancellation= CancellationDetails::getInstance();
            $data = array();

            $data['cancellation_status'] = 1;
            $cancellation_id = $request->input('delete_id');

            // print_r($cancellation_id);

            $updateCancellationById = $objectModelCancellation->updateCancellationById($cancellation_id,$data);
           return redirect('/admin/cancellation');

        }
    }




    public function cancellationAjaxHandler()
    {

         $objModelFeedback = CancellationDetails::getInstance();
        $result = $objModelFeedback->getCancellationDetails();
        $result = json_decode(json_encode($result), true);
        $cancellationDetails = new Collection();

        foreach ($result as $res) {

            $cancellation_id = $res['cancellation_id'];
            $reason = $res['reason'];



            $cancellationDetails->push([
                'cancellation_id' => $res['cancellation_id'],
                'reason' => $res['reason'],
                'action' => '<button data-toggle="modal" id="editCancellation" data-id=' . $cancellation_id . '
                                class="btn btn-primary editCancellation">
                                <i class="fa fa-pencil"></i></button>,
                <button data-toggle="modal" id="deleteCancellation" data-id=' . $cancellation_id . '
                                class="btn btn-danger deleteCancellation">
                                <i class="fa fa-trash"></i></button>'

            ]);

        }
        return Datatables::of($cancellationDetails)->make(true);


    }



    public function cancellationDetailsAjaxHandler(Request $request){


        $inputData = $request->input();
        $method = $inputData['method'];
       //  print_r($method);

        switch ($method) {


            case 'getCancellationById':

              //  print_r('sanwar');

                $cancellation_id['cancellation_id'] = $request->input('cancellation_id');

                $objModelCancellation = CancellationDetails::getInstance();
                $cancellation = $objModelCancellation->getCancellationById($cancellation_id);

                echo json_encode($cancellation);
                break;
            default:
                break;
        }


    }




}
