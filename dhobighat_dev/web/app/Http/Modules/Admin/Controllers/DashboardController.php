<?php namespace App\Http\Modules\Admin\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Modules\Admin\Models\ExecutiveDetails;
use App\Http\Modules\Admin\Models\BookingDetails;
use App\Http\Modules\Admin\Models\CustomerDetails;
use App\Http\Modules\Admin\Models\VehicleDetails;
use App\Http\Modules\Admin\Models\BookingCancellationDetails;
use App\Http\Modules\Admin\Models\FeedbackDetails;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    public function overview()
    {


        $objModelBooking = BookingDetails::getInstance();
        $result = $objModelBooking->getBookingDetails();
        //  $total_orders=count($result);

        if (is_array($result)) {
            $total_orders = count(array_filter($result));
        } else {
            $total_orders = 0;
        }


        $result1 = $objModelBooking->getRejectedBookingDetails();
        // $total_rejection=count($result1);

        if (is_array($result1)) {
            $total_rejection = count(array_filter($result1));
        } else {
            $total_rejection = 0;
        }


        $revenue = 0;
        foreach ($result as $key => $value) {

            $final_price = $value->final_price;
            $revenue = $revenue + $final_price;
        }

        return view("Admin::overview", ['total_orders' => $total_orders, 'total_rejection' => $total_rejection, 'total_revenue' => $revenue]);

        //    return view("Admin::overview");
    }


    public function searchNewBooking()
    {
        return 'searchNewBooking';
    }

    public function viewBooking()
    {
        return 'viewBooking';
    }

    public function dashboardAjaxHandler()
    {
        // return 'dashboardAjaxHandler';

        $objModelBooking = BookingDetails::getInstance();
        $objModelCustomer = CustomerDetails::getInstance();
        $objModelVehicle = VehicleDetails::getInstance();


        $result = $objModelBooking->getBookingDetails();
        $result = json_decode(json_encode($result), true);
        $bookingDetails = new Collection();

        foreach ($result as $res) {

            $booking_id = $res['booking_id'];

            $booking_type = "";
            switch ($res['booking_type']) {
                case 0:
                    $booking_type = "Now";
                    break;

                case 1:

                    $booking_type = "Later";
                    break;


            }


            $service_status = "";

            switch ($res['service_status']) {
                case 0:
                    $service_status = "Booked";
                    break;

                case 1:

                    $service_status = "cloths Collected";
                    break;

                case 2:

                    $service_status = "Service Completed";
                    break;

                case 3:

                    $service_status = "Delivered";
                    break;


                case 4:

                    $service_status = "Cancelled";
                    break;


            }


            $customer_id = $res['customer_id'];
            $customer_phone_no = $objModelCustomer->getCustomerById($customer_id)->customer_phone_no;

            $customer_address = $res['customer_address'];
            $booking_time = $res['booking_time'];

            $executive_id = $res['executive_id'];

            $vehicle_id = $res['vehicle_id'];
            $vehicle_registration_no = $objModelVehicle->getVehicleById($vehicle_id)->vehicle_registration_no;

            $revenue = $res['final_price'];


            $bookingDetails->push([
                'booking_id' => $res['booking_id'],
                'booking_type' => $booking_type,
                'customer_id' => $res['customer_id'],
                'vehicle_registration_no' => $vehicle_registration_no,
                'customer_phone_no' => $customer_phone_no,
                'customer_address' => $res['customer_address'],
                'booking_time' => $res['booking_time'],
                'service_status' => $service_status,
                'executive_id' => $res['executive_id'],
                'vehicle_id' => $res['vehicle_id'],
                'revenue' => $res['final_price'],

                'action' => '<button data-toggle="modal" id="viewBooking" data-id=' . $booking_id . '
                                class="btn btn-primary viewBooking">
                                <i class="fa fa-info"></i></button>',

            ]);

        }
        return Datatables::of($bookingDetails)->make(true);


    }


    public function dashboardDetailsAjaxHandler(Request $request)
    {

        $inputData = $request->input();
        $method = $inputData['method'];

        switch ($method) {


            case 'getBookingById':
                $booking_id['booking_id'] = $request->input('booking_id');

                $objModelBooking = BookingDetails::getInstance();
                $objModelCustomer = CustomerDetails::getInstance();
                $objModelVehicle = VehicleDetails::getInstance();
                $objModelExecutive = ExecutiveDetails::getInstance();
                $objModelBookingCancellation = BookingCancellationDetails::getInstance();
                $objModelFeedback = FeedbackDetails::getInstance();

                $booking_data = $objModelBooking->getBookingById($booking_id);
                $customer_data = $objModelCustomer->getCustomerById($booking_data->customer_id);
                $executive_data = $objModelExecutive->getExecutiveByExeId($booking_data->executive_id);
                $vehicle_data = $objModelVehicle->getVehicleById($booking_data->vehicle_id);
                $cancellation_data = $objModelBookingCancellation->getBookingCancellationById($booking_id);
                $feedback_data = $objModelFeedback->getFeedbackById($booking_id);

                $data = array();

                $data['booking_id'] = $booking_data->booking_id;
                $data['customer_name'] = $customer_data->customer_name;
                $data['customer_id'] = $customer_data->customer_id;
                $data['customer_phone_no'] = $customer_data->customer_phone_no;
                $data['customer_address'] = $booking_data->customer_address;
                $data['executive_id'] = $booking_data->executive_id;
               $data['executive_custom_id'] = $executive_data->executive_custom_id;
               $data['executive_name'] = $executive_data->executive_name;
                $data['booking_time'] = $booking_data->booking_time;
                $data['vehicle_registration_no'] = $vehicle_data->vehicle_registration_no;
                $data['service_time'] = $booking_data->service_time;

                $service_status = $booking_data->service_status;

                if ($service_status == 0) {

                    $data['service_status'] = 'Booked';
                    $data['clothes_count'] = $booking_data->estimated_count;
                    $data['clothes_weight'] = $booking_data->estimated_weight;
                    $data['cancellation_reason'] = '';
                    $data['stars'] = "";
                    $data['comments'] = "";
                    $data['price'] = $booking_data->estimated_price;
                    $data['final_price'] = "";

                } elseif ($service_status == 1) {
                    $data['service_status'] = 'Clothes Collected';
                    $data['clothes_count'] = $booking_data->final_count;
                    $data['clothes_weight'] = $booking_data->final_weight;
                    $data['cancellation_reason'] = '';
                    $data['stars'] = "";
                    $data['comments'] = "";
                    $data['price'] = $booking_data->price;
                    $data['final_price'] = $booking_data->final_price;

                } elseif ($service_status == 2) {

                    $data['service_status'] = 'Service Completed';
                    $data['clothes_count'] = $booking_data->final_count;
                    $data['clothes_weight'] = $booking_data->final_weight;
                    $data['cancellation_reason'] = '';
                    $data['stars'] = "";
                    $data['comments'] = "";
                    $data['price'] = $booking_data->price;
                    $data['final_price'] = $booking_data->final_price;

                } elseif ($service_status == 3) {

                    $data['service_status'] = 'Delivered';
                    $data['clothes_count'] = $booking_data->final_count;
                    $data['clothes_weight'] = $booking_data->final_weight;
                    $data['cancellation_reason'] = '';
                    $data['stars'] = $feedback_data->stars;
                    $data['comments'] = $feedback_data->comments;
                    $data['price'] = $booking_data->price;
                    $data['final_price'] = $booking_data->final_price;

                } else {

                    $data['service_status'] = 'Cancelled';
                    $data['clothes_count'] = $booking_data->estimated_count;
                    $data['clothes_weight'] = $booking_data->final_weight;
                    $data['cancellation_reason'] = $cancellation_data->cancellation_reason;
                    $data['stars'] = "";
                    $data['comments'] = "";
                    $data['price'] = $booking_data->estimated_price;
                    $data['final_price'] = "";


                }



                $data['promotion_id'] = $booking_data->promotion_id;
                $data['promotion_value'] = $booking_data->promotion_value;

                $payment_mode = $booking_data->payment_mode;
                if ($payment_mode == 1) {

                    $data['payment_mode'] = 'Swipe';
                } elseif ($payment_mode == 2) {

                    $data['payment_mode'] = 'Swipe';
                } else {

                    $data['payment_mode'] = 'Cash';
                }

                echo json_encode($data);
                break;
            default:
                break;
        }

    }


}
