<?php namespace App\Http\Modules\Admin\Controllers;

use App\Http\Modules\Admin\Models\BookingDetails;
use App\Http\Modules\Admin\Models\BookingCategoryDetails;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use stdClass;

class BookingController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

    public function booking()
    {
        return view("Admin::booking");
    }

    public function addBooking(Request $request)
    {
        if ($request->isMethod('POST')) {
            // return 'addBooking';

            $response = new stdClass();
            $data = array();

           // $data['executive_id'] = $request->input('executive_id');
            $data['customer_id'] = $request->input('customer_id');
            $data['booking_time'] = $request->input('booking_time');
           // $data['vehicle_id'] = $request->input('vehicle_id');
            $data['customer_address'] = $request->input('customer_address');
            $data['customer_longitude'] = $request->input('customer_longitude');
            $data['customer_latitude'] = $request->input('customer_latitude');
            $data['estimated_price'] = $request->input('estimated_price');
            $data['service_time'] = $request->input('service_time');
            $data['executive_device_id'] = $request->input('executive_device_id');

            //insert in booking table and get booking id

            $objectModelBookingDetails = BookingDetails::getInstance();
            $addBookingById = $objectModelBookingDetails->addNewBooking($data);
            if($addBookingById){



            }

//            $items = $request->input(items);
//            $objectModelBookingCategoryDetails= BookingCategoryDetails::getInstance();
//
//            foreach ($items as $item) {
//                $catdata[category_id] = $item->category_id;
//                $catdata[count] = $item->count;
//                $catdata[booking_id] = $addBookingById;
//
//                //insert in booking category table whwre id is booking id
//                $addBookingCategoryDetails= $objectModelBookingCategoryDetails->addBookingCategoryDetails($catdata);
//
//            }


        }
    }

    public function searchBooking()
    {
        return 'searchBooking';
    }

    public function updateBooking()
    {
        return 'updateBooking';
    }

    public function bookingAjaxHandler()
    {
        return 'bookingAjaxHandler';
    }



}
