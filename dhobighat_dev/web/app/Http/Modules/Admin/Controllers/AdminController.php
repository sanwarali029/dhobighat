<?php namespace App\Http\Modules\Admin\Controllers;


use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Http\Modules\Admin\Models\AdminDetails;




class AdminController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view("Admin::index");
	}

    public function forgot()
    {
        return view("Admin::forgot");
    }
    public function login()
    {
        return view("Admin::login");


    }


 public function loginAuth(Request $request)
 {

    // return $request->all();


           if ($request->isMethod('POST'))
            {

                   $email = $request->input('email');
                   $password = $request->input('password');

//                   $this->validate($request,
//                       ['email'=>'max:3']);

                   $objectModelAdminDetails = AdminDetails::getInstance();

                   if ($email || $password) {
                       $adminDetailsById = $objectModelAdminDetails->getUsersLogin($email, $password);

                       if ($adminDetailsById) {




                          return redirect("/admin/overview");


                       }
                       else {

                                  alert()->error('Invalid Credentials');
                                  return redirect("/admin");

                       }
                   }

                   else {

                                  alert()->error('Invalid Credentials');
                                  return redirect("/admin");

                   }


            }

     return redirect("/admin");
 }



    public function profile()
    {
        return view("Admin::profile");
    }


}
