<?php namespace App\Http\Modules\Admin\Controllers;


use App\Http\Modules\Admin\Models\ExecutiveDetails;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class ExecutiveController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    public function executive()
    {
        return view("Admin::executive");
    }

    public function addExecutive(Request $request)
    {
        if ($request->isMethod('POST')) {

             $objectModelExecutive= ExecutiveDetails::getInstance();



            $this->validate($request, [
                'executive_email' => 'required|unique:executive_details,executive_email',
                'executive_mobile_no' => 'required|numeric|regex:/^[0-9]{10}$/|unique:executive_details,executive_mobile_no',
                'executive_custom_id' => 'required|unique:executive_details,executive_custom_id',
                'executive_driving_licence_no' => 'required|unique:executive_details,executive_driving_licence_no',
                'executive_date_of_birth' => 'date|before:"1999-08-08 00:00:00"',
                'executive_name' => 'required|regex:/^[\pL\s]+$/u',

            ]);



           //new executive
            $data = array();

                $data['executive_name'] = $request->input('executive_name');
                $data['executive_driving_licence_no'] = $request->input('executive_driving_licence_no');
                $data['executive_date_of_birth'] = $request->input('executive_date_of_birth');
                $data['executive_mobile_no'] = $request->input('executive_mobile_no');
                $data['executive_email'] = $request->input('executive_email');
                $data['executive_address'] = $request->input('executive_address');
                $data['executive_custom_id'] = $request->input('executive_custom_id');
            //    $data['executive_status'] = 1;


                $executiveDetails = $objectModelExecutive->addExecutive($data);

//                print_r($executiveDetails);
//                die();

            return redirect('/admin/executive');

        }


    }

    public function editExecutive(Request $request)
    {
        //return 'editExecutive';

        $objectModelExecutive= ExecutiveDetails::getInstance();
        $data = array();

        //validation will be here

        $this->validate($request, [
            'edit_executive_email' => 'required|email',
           'edit_executive_mobile_no' => 'required|numeric|regex:/^[0-9]{10}$/',
            'edit_executive_custom_id' => 'required',
            'edit_executive_name' => 'required|regex:/^[\pL\s]+$/u',
            'edit_executive_driving_licence_no' => 'required',
            'edit_executive_address' => 'required',
         //   'edit_executive_date_of_birth' => 'date|before:"1999-08-08 00:00:00"',
            'executive_new_password' => 'min:8',
            'executive_confirm_new_password' => 'same:executive_new_password',

        ]);



      //  if ($check) {
//
        $hidden_executive_id = $request->input('hidden_executive_id');
        $data['executive_custom_id'] = $request->input('edit_executive_custom_id');
        $data['executive_name'] = $request->input('edit_executive_name');
        $data['executive_driving_licence_no'] = $request->input('edit_executive_driving_licence_no');
       // $data['executive_date_of_birth'] = $request->input('edit_executive_date_of_birth');
        $data['executive_mobile_no'] = $request->input('edit_executive_mobile_no');
        $data['executive_email'] = $request->input('edit_executive_email');
        $data['executive_address'] = $request->input('edit_executive_address');
        $executive_password = $request->input('executive_new_password');
        $executive_confirm_password = $request->input('executive_confirm_new_password');
        $data['executive_status'] = 1;

        if($executive_password && $executive_confirm_password ){

            if($executive_password==$executive_confirm_password){

                $data['executive_password'] = $executive_confirm_password;
            }
        }


        $result = $objectModelExecutive->updateExecutiveById($hidden_executive_id, $data);
            return redirect('/admin/executive');



        //}
    }






    public function deleteExecutive(Request $request)
    {
        //return 'deleteExecutive';

        $executive_id['executive_id'] = $request->input('delete_id');
        $data['executive_status'] = 2;
        $objModelExecutive = ExecutiveDetails::getInstance();
        $delete = $objModelExecutive->deleteExecutiveById($executive_id, $data);

            return redirect('/admin/executive');


    }

    public function executiveAjaxHandler()
    {
        //return 'executiveAjaxHandler';

        $objModelExecutive = ExecutiveDetails::getInstance();
        $result = $objModelExecutive->getExecutiveDetails();
        $result = json_decode(json_encode($result), true);
        $executiveDetails = new Collection();

        foreach ($result as $res) {

            $executive_custom_id = $res['executive_custom_id'];
            $executive_name = $res['executive_name'];
            $executive_driving_licence_no = $res['executive_driving_licence_no'];
            $executive_mobile_no = $res['executive_mobile_no'];
            $executive_address = $res['executive_address'];
            $executive_last_logged_in_vehicle = $res['executive_last_logged_in_vehicle'];



            $executiveDetails->push([
                'executive_custom_id' => $res['executive_custom_id'],
                'executive_name' => $res['executive_name'],
                'executive_driving_licence_no' => $res['executive_driving_licence_no'],
                'executive_mobile_no' => $res['executive_mobile_no'],
                'executive_address' => $res['executive_address'],
                'executive_last_logged_in_vehicle' => $res['executive_last_logged_in_vehicle'],

                'action' => '<button data-toggle="modal" id="editExecutive" data-id=' . $executive_custom_id . '
                                class="btn btn-info editExecutive">
                                <i class="fa fa-pencil"></i></button>
                                <button data-id=' . $executive_custom_id . ' id="deleteExecutive" data-toggle="modal"
                                class="btn btn-danger deleteExecutive">
                                <i class="fa fa-trash"></i></button>',

            ]);

        }
        return Datatables::of($executiveDetails)->make(true);


    }


    public function executiveDetailsAjaxHandler(Request $request){

        $inputData = $request->input();
        $method = $inputData['method'];

        switch ($method) {


            case 'getExecutiveById':
                $executive_custom_id['executive_custom_id'] = $request->input('executive_custom_id');
                $objModelExecutive = ExecutiveDetails::getInstance();
                $executive = $objModelExecutive->getExecutiveById($executive_custom_id);

                echo json_encode($executive);
                break;
            default:
                break;
        }

    }

}
