<?php namespace App\Http\Modules\Admin\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\Http\Modules\Admin\Models\PromotionDetails;

class PromotionsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    public function promotions()
    {
        return view("Admin::promotions");
    }

    public function addPromotions()
    {
        return 'addPromotions';
    }

    public function editPromotions()
    {
        return 'editPromotions';
    }

    public function deletePromotions()
    {
        return 'deletePromotions';
    }

    public function promotionsAjaxHandler()
    {

        $objModelPromotion = PromotionDetails::getInstance();
        $result = $objModelPromotion->getPromotionDetails();
        $result = json_decode(json_encode($result), true);
        $promotionDetails = new Collection();

        foreach ($result as $res) {

            $promotion_id = $res['promotion_id'];
            $promotion_code = $res['promotion_code'];
            $promotion_name = $res['promotion_name'];
            $promotion_from_date = $res['promotion_from_date'];
            $promotion_to_date = $res['promotion_to_date'];
            $promotion_discount_type = $res['promotion_discount_type'];
            $promotion_discount_value = $res['promotion_discount_value'];
            $promotion_on_booking_amount = $res['promotion_on_booking_amount'];
            $promotion_on_booking_weight = $res['promotion_on_booking_weight'];
            $promotion_for_new_users = $res['promotion_for_new_users'];
            $promotion_visibility = $res['promotion_visibility'];




            $promotionDetails->push([
                'promotion_id' => $res['promotion_id'],
                'promotion_code' => $res['promotion_code'],
                'promotion_name' => $res['promotion_name'],
                'promotion_to_date' => $res['promotion_to_date'],
                'promotion_from_date' => $res['promotion_from_date'],
                'promotion_discount_type' => $res['promotion_discount_type'],
                'promotion_discount_value' => $res['promotion_discount_value'],
                'promotion_on_booking_amount' => $res['promotion_on_booking_amount'],
                'promotion_on_booking_weight' => $res['promotion_on_booking_weight'],
                'promotion_for_new_users' => $res['promotion_for_new_users'],
                'promotion_visibility' => $res['promotion_visibility'],

                'action' => '<button data-toggle="modal" id="editPromotion" data-id=' . $promotion_id . '
                                class="btn btn-primary editPromotion">
                                <i class="fa fa-pencil"></i></button>
                                <button data-id=' . $promotion_id . ' id="deletePromotion" data-toggle="modal"
                                class="btn btn-danger deletePromotion">
                                <i class="fa fa-trash"></i></button>',

            ]);

        }
        return Datatables::of($promotionDetails)->make(true);


    }

}
