<?php namespace App\Http\Modules\Admin\Controllers;

use App\Http\Modules\Admin\Models\ExecutiveDetails;
use App\Http\Modules\Admin\Models\VehicleDetails;
use App\Http\Modules\Admin\Models\BookingDetails;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class VehicleController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    public function vehicle()
    {
        return view("Admin::vehicle");
    }



        public function addVehicle(Request $request){

        if ($request->isMethod('POST')) {

           // return 'add vehicle';

            $objectModelVehicle= VehicleDetails::getInstance();



//            $this->validate($request, [
//                'executive_email' => 'required|unique:executive_details,executive_email',
//                'executive_mobile_no' => 'required|numeric|regex:/^[0-9]{10}$/|unique:executive_details,executive_mobile_no',
//                'executive_custom_id' => 'required|unique:executive_details,executive_custom_id',
//                'executive_driving_licence_no' => 'required|unique:executive_details,executive_driving_licence_no',
//                'executive_date_of_birth' => 'date|before:"1999-08-08 00:00:00"',
//                'executive_name' => 'required|regex:/^[\pL\s]+$/u',
//
//            ]);



            //new executive
            $data = array();

            $data['vehicle_make'] = $request->input('vehicle_make');
            $data['vehicle_model'] = $request->input('vehicle_model');
            $data['vehicle_date_of_purchase'] = $request->input('vehicle_date_of_purchase');
            $data['vehicle_make_year'] = $request->input('vehicle_make_year');
            $data['vehicle_kms_driven'] = $request->input('vehicle_kms_driven');
            $data['vehicle_registration_no'] = $request->input('vehicle_registration_no');
            $data['vehicle_color'] = $request->input('vehicle_color');
               $data['vehicle_status'] = 1;

           // print_r($data);


            $addVehicleById = $objectModelVehicle->addVehicle($data);

//           print_r($addVehicleById);
//          die();

            return redirect('/admin/vehicle');

        }


    }





    public function editVehicle(Request $request)
    {
      //  return 'editVehicle';



        if ($request->isMethod('POST')) {

            // return 'add vehicle';

            $objectModelVehicle= VehicleDetails::getInstance();



//            $this->validate($request, [
//                'executive_email' => 'required|unique:executive_details,executive_email',
//                'executive_mobile_no' => 'required|numeric|regex:/^[0-9]{10}$/|unique:executive_details,executive_mobile_no',
//                'executive_custom_id' => 'required|unique:executive_details,executive_custom_id',
//                'executive_driving_licence_no' => 'required|unique:executive_details,executive_driving_licence_no',
//                'executive_date_of_birth' => 'date|before:"1999-08-08 00:00:00"',
//                'executive_name' => 'required|regex:/^[\pL\s]+$/u',
//
//            ]);



            //new executive
            $data = array();

            $vehicle_id = $request->input('hidden_vehicle_id');
            $data['vehicle_make'] = $request->input('edit_vehicle_make');
            $data['vehicle_model'] = $request->input('edit_vehicle_model');
            $data['vehicle_date_of_purchase'] = $request->input('edit_vehicle_date_of_purchase');
            $data['vehicle_make_year'] = $request->input('edit_vehicle_make_year');
            $data['vehicle_kms_driven'] = $request->input('edit_vehicle_kms_driven');
            $data['vehicle_registration_no'] = $request->input('edit_vehicle_registration_no');
            $data['vehicle_color'] = $request->input('edit_vehicle_color');
          //  $data['vehicle_status'] = 1;

           //  print_r($vehicle_id) ;


          $editVehicleById = $objectModelVehicle->updateVehicleById($vehicle_id,$data);

    // return $editVehicleById;
//          die();
//
            return redirect('/admin/vehicle');

        }




    }

//
    public function deleteVehicle(Request $request)
    {
        //return 'deleteExecutive';

        $vehicle_id = $request->input('delete_id');
        $data['vehicle_status'] = 2;
        $objModelVehicle = VehicleDetails::getInstance();
        $delete = $objModelVehicle->updateVehicleById($vehicle_id, $data);

        return redirect('/admin/vehicle');


    }


    public function vehicleAjaxHandler()
    {
       // return 'vehicleAjaxHandler';


        $objModelVehicle = VehicleDetails::getInstance();


        $result = $objModelVehicle->getVehicleDetails();
        $result = json_decode(json_encode($result), true);
        $vehicleDetails = new Collection();

        foreach ($result as $res) {

            $vehicle_make_year = $res['vehicle_make_year'];
            $vehicle_registration_no = $res['vehicle_registration_no'];
            $vehicle_make = $res['vehicle_make'];
            $vehicle_model = $res['vehicle_model'];
            $vehicle_color = $res['vehicle_color'];
            $vehicle_last_logged_in_executive = $res['vehicle_last_logged_in_executive'];
            $vehicle_id = $res['vehicle_id'];
            $vehicle_last_service = $res['vehicle_last_service'];
            $vehicle_services_done = $res['vehicle_services_done'];



            $vehicleDetails->push([
                'vehicle_make_year' => $res['vehicle_make_year'],
                'vehicle_registration_no' => $res['vehicle_registration_no'],
                'vehicle_last_service' => $res['vehicle_last_service'],
                'vehicle_make' => $res['vehicle_make'],
                'vehicle_model' => $res['vehicle_model'],
                'vehicle_color' => $res['vehicle_color'],
                'vehicle_id' => $res['vehicle_id'],
                'vehicle_last_logged_in_executive' => $res['vehicle_last_logged_in_executive'],
                'vehicle_services_done' => $res['vehicle_services_done'],

                'action' => '<button data-toggle="modal" id="editVehicle" data-id=' . $vehicle_id . '
                                class="btn btn-primary editVehicle">
                                <i class="fa fa-pencil"></i></button>
                                <button data-id=' . $vehicle_id . ' id="deleteVehicle" data-toggle="modal"
                                class="btn btn-danger deleteVehicle">
                                <i class="fa fa-trash"></i></button>',

            ]);

        }
        return Datatables::of($vehicleDetails)->make(true);


    }





    public function VehicleDetailsAjaxHandler(Request $request){

        $inputData = $request->input();
        $method = $inputData['method'];
       // print_r($inputData);

        switch ($method) {


            case 'getVehicleById':
                $vehicle_id['vehicle_id'] = $request->input('vehicle_id');
                $objModelVehicle = VehicleDetails::getInstance();
                $vehicle = $objModelVehicle->getVehicleById($vehicle_id);

                echo json_encode($vehicle);
                break;
            default:
                break;
        }

    }






}
