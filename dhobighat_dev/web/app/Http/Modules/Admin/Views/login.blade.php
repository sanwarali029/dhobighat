
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>DhobiGhat Admin</title>

    <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="/assets/font-awesome/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="/assets/css/local.css" />
    <link rel="stylesheet" type="text/css" href="/assets/sweetalert/sweetalert.css" />


    <script type="text/javascript" src="/assets/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="/assets/bootstrap/js/bootstrap.min.js"></script>

    <script type="text/javascript" src="/assets/sweetalert/sweetalert.min.js"></script>
    <script type="text/javascript" src="/assets/sweetalert/sweetalert-dev.js"></script>

    <!-- you need to include the shieldui css and js assets in order for the charts to work -->
    <link rel="stylesheet" type="text/css" href="http://www.shieldui.com/shared/components/latest/css/light-bootstrap/all.min.css" />
    <script type="text/javascript" src="http://www.shieldui.com/shared/components/latest/js/shieldui-all.min.js"></script>
    <script type="text/javascript" src="http://www.prepbootstrap.com/Content/js/gridData.js"></script>
</head>
<body>
   

        <div id="page-wrapper">
            <div class="row" style="margin-top:3vh;paging:1vh">
                <div class="col-lg-12">
                   <h1 style="text-align:center;color:white"> Dhobi Ghat </h1>
				    <div class="col-lg-2">
                
                    </div>
				   
				    <div class="col-lg-4"style="background-color:rgba(0,0,0,0.5);padding:30px">
					
                        <h2 style="background-color:#ebebeb;text-align:center;padding:10px;font-size:20px"> Admin Access </h2>

                       <form action="/admin/login-auth" method="post">
                        <div class="form-group">
					
				        <input type="email" name="email" id="email" placeholder="Enter Your Email" value="{{ old('email') }}" style="width:100%; padding:10px; margin-bottom:10px;margin-top:10px" required><br>
					
					    <input type="password" name="password" id="password" placeholder="Enter Your Password" style="width:100%; padding:10px; margin-bottom:10px" required><br>
					
					    <button type="submit" class="btn btn-primary" style="width:40%;margin-bottom:10px">Log-In</button> <a href="forgot" style="margin-left:10px">Forgot Password ?</a>
                        </div>
				      </form>
                    </div>
				    <div class="col-lg-4"style="background-color:rgba(0,0,0,0.5)">
                    
                   					
					<img src="/assets/images/dhobighat.png" width="100%" height="100%"; style="margin:0px;padding-bottom:23px">
                    
                
                    </div>
					 <div class="col-lg-2">
                    
                   	
                    </div>
				   
				   
				   
                </div>
            </div>
           
        </div>

    <!-- /#wrapper -->

        @include('sweet::alert')
</body>
</html>
