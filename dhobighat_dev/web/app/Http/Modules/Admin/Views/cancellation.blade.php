<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dhobi Ghat Admin</title>

    <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="/assets/font-awesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="/assets/css/local.css"/>

    <script type="text/javascript" src="/assets/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="/assets/bootstrap/js/bootstrap.min.js"></script>

    <!-- you need to include the shieldui css and js assets in order for the charts to work -->
    <link rel="stylesheet" type="text/css"
          href="http://www.shieldui.com/shared/components/latest/css/light-bootstrap/all.min.css"/>
    <script type="text/javascript"
            src="http://www.shieldui.com/shared/components/latest/js/shieldui-all.min.js"></script>
    <script type="text/javascript" src="http://www.prepbootstrap.com/Content/js/gridData.js"></script>

    <script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css"/>

</head>
<body style="background-image:none">
<div id="wrapper">
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="navbar-header" style="background-color:#535358">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index">Admin Panel</a>
        </div>
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <li><a href="overview"><i class="fa fa-bullseye"></i> Overview</a></li>
                <li><a href="executive"><i class="fa fa-tasks"></i> Executive Details</a></li>
                <li><a href="vehicle"><i class="fa fa-globe"></i> Vehicle Details</a></li>
                <li><a href="business"><i class="fa fa-list-ol"></i> Business Model</a></li>
                <li><a href="promotions"><i class="fa fa-font"></i> Promotions</a></li>
                <li><a href="booking"><i class="fa fa-font"></i> Scheduled Bookings</a></li>
                <li class="active"><a href="cancellation"><i class="fa fa-font"></i> Cancellation</a></li>
                <li><a href="feedbacks"><i class="fa fa-font"></i> Feedbacks</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right navbar-user">

                <li class="dropdown user-dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Steve Miller<b
                                class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="#"><i class="fa fa-user"></i> Profile</a></li>
                        <li><a href="#"><i class="fa fa-gear"></i> Settings</a></li>
                        <li class="divider"></li>
                        <li><a href="#"><i class="fa fa-power-off"></i> Log Out</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>

    <div id="page-wrapper">

        <form action="/admin/add-cancellation" method="POST">
            <div class="form-group">

                <div class="row" style="margin-bottom:20px">


                    <div class="col-lg-3">


                    </div>


                    <div class="col-lg-6">

                        <input type="text" required id="reason" name="reason" style="padding: 10px;width:100%" placeholder="Add New Cancellation Reason">


                    </div>

                    <div class="col-lg-3">

                    </div>
                </div>


                <div class="row" style="margin-bottom:20px">

                    <div class="col-lg-4">


                    </div>


                    <div class="col-lg-4">


                        <button type="submit" class="btn btn-primary" style="width:100%;align:bottom;margin-top:0px">
                            Add Reason
                        </button>


                    </div>


                    <div class="col-lg-4">

                    </div>

                </div>

            </div>
        </form>
        <div class="row">

            <div class="col-lg-2">

            </div>
            <div class="col-lg-4">
                <div class="panel panel-default ">
                    <div class="panel-body alert-info" style="background-color:#598A87">


                        <h4 class="alerts-text text-center">Total Orders</h4>
                        <p class="alerts-heading text-center">{{$total_orders}}</p>

                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="panel panel-default ">
                    <div class="panel-body alert-info" style="background-color:#45B3A5">

                        <h4 class="alerts-text text-center">Total Cancelled</h4>
                        <p class="alerts-heading text-center">{{$total_rejected}}</p>


                    </div>
                </div>
            </div>
            <div class="col-lg-2">

            </div>

        </div>

        <div class="row">

            <div class="col-lg-12 text-center">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Cancellation Data</h3>
                    </div>
                </div>
            </div>

        </div>


        <div class="row">

            <div class="col-lg-2">

            </div>
            <div class="col-lg-8">
                <div class="panel panel-default">
                    <div class="panel-body">

                        <div class="table-responsive">
                            <table class="table table-striped font-12" id="datatable">
                                <thead>
                                <tr>

                                    <th>Reason</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>Reason</th>
                                    <th>Actions</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="col-lg-2">

                </div>
            </div>
            <!-- /.col -->
        </div>









        <div class="modal modal-scale fade" id="editModal">
            <div class="modal-dialog modal-md">
                <div class="modal-content" style="background-color: rgba(0,0,0,0.7);color:#fff;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    style="color: #ffffff;border-bottom-color: transparent"
                                    aria-hidden="true">&times;</span></button>

                        {{--@if (count($errors) > 0)--}}
                        {{--@if ($errors->has('edit_executive_email')||$errors->has('edit_executive_mobile_no')||$errors->has('edit_executive_custom_id')||$errors->has('edit_executive_name')||$errors->has('edit_executive_driving_licence_no')||$errors->has('edit_executive_address')||$errors->has('edit_executive_date_of_birth')||$errors->has('executive_new_password')||$errors->has('executive_confirm_new_password'))--}}

                        {{--<div class="alert alert-danger">--}}
                        {{--<ul>--}}
                        {{--@foreach ($errors->all() as $error)--}}
                        {{--<li>{{ $error }}</li>--}}
                        {{--@endforeach--}}

                        {{--</ul>--}}
                        {{--</div>--}}


                        {{--@endif--}}
                        {{--@endif--}}



                    </div>
                    <div class="modal-body" style="margin: auto">
                        <form action="/admin/edit-cancellation" role="form" id="editcancellationform"
                              class="editcancellationform"
                              method="post" enctype="multipart/form-data" style="padding: 20px">
                            <h3 style="color: white;margin-top: -20px">Edit Cancellation Reason</h3>

                            <input type="text" name="hidden_cancellation_id" id="hidden_cancellation_id"
                                   style="display: none;color: black;width:100%; padding:10px; margin-bottom:5px;margin-top:0px"><br>


                            <input type="text" name="edit_reason" id="edit_reason" value="{{ old('edit_reason') }}"
                                   style="color: black;width:100%; padding:10px; margin-bottom:5px;margin-top:5px"><br>


                            <div class="modal-footer" style="margin-top:10px ">
                                <div class="pull-right">
                                    <input type="submit" class="btn btn-primary editcancellationinfo" data-id=""
                                           value="Update" id="edit-cancellation" style="background-color: #3e3232"/>
                                    <button type="button" class="btn btn-primary edit-toggle" data-dismiss="modal"
                                            style="background-color: red">
                                        Cancel
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>








        <!-- Start Modal -->
        <div class="modal modal-scale fade" id="deleteModal">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title font-header text-dark">Delete Executive</h4>
                    </div>


                    <form action="/admin/delete-cancellation" role="form" id="editcancellationform"
                          class="editcancellationform"
                          method="post" enctype="multipart/form-data" style="padding: 20px">

                        <div class="modal-body">
                            <div class="form-group form-input-group m-t-5 m-b-5">
                                <p> Are you sure you want to delete this reason? </p>

                                <input type="text" name="delete_id" id="delete_id"
                                       style="display: none;color: black;width:100%; padding:10px; margin-bottom:5px;margin-top:0px"><br>

                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary deletecancellationinfo" style="background-color: #3e3232" data-id="">Delete</button>
                            <button type="button" class="btn btn-primary" data-dismiss="modal"  style="background-color: red">Cancel</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- End Modal -->











    </div>
</div>
<!-- /#wrapper -->

<script type="text/javascript">


    $(document).ready(function () {
        $('#datatable').DataTable({
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
            processing: true,
            serverSide: true,
            ajax: '/admin/cancellation-ajax-handler',
            columns: [

                {data: 'reason', name: 'reason'},
                {data: 'action', name: 'action'}
            ],


            initComplete: function () {
                this.api().columns().every(function () {
                    var column = this;
                    var input = document.createElement("input");
                    $(input).appendTo($(column.footer()).empty())
                        .on('change', function () {
                            column.search($(this).val(), false, false, true).draw();
                        });
                });
            }
        });



        $(document).on('click', '#editCancellation', function (e) {
            e.preventDefault();
            $("#editModal").modal("show");
            var cancellation_id = $(this).attr('data-id');

            console.log(cancellation_id);

            $.ajax({
                url: '/admin/cancellation-details-ajax-handler',
                type: 'post',
                dataType: 'json',
                data: {
                    method: 'getCancellationById',
                    cancellation_id: cancellation_id

                },
                beforeSend: function () {
                },
                success: function (response) {
                    console.log(response);

                    $('#hidden_cancellation_id').val(response.cancellation_id);
                    $('#edit_reason').val(response.reason);



                }
            });
        });






        $(document).on('click', '.deleteCancellation', function (e) {
            e.preventDefault();
            $("#deleteModal").modal("show");
            var cancellation_id = $(this).attr('data-id');

            console.log(cancellation_id);

            $.ajax({
                url: '/admin/cancellation-details-ajax-handler',
                type: 'post',
                dataType: 'json',
                data: {
                    method: 'getCancellationById',
                    cancellation_id: cancellation_id

                },
                beforeSend: function () {
                },
                success: function (response) {

                    console.log(response);
                    $('#delete_id').val(response.cancellation_id);

                },

            });
        });



    });
</script>
</body>
</html>
