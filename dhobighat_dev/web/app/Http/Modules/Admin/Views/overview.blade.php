<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dhobi Ghat Admin</title>

    <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="/assets/font-awesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="/assets/css/local.css"/>

    <script type="text/javascript" src="/assets/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="/assets/bootstrap/js/bootstrap.min.js"></script>

    <!-- you need to include the shieldui css and js assets in order for the charts to work -->
    <link rel="stylesheet" type="text/css"
          href="http://www.shieldui.com/shared/components/latest/css/light-bootstrap/all.min.css"/>
    <script type="text/javascript"
            src="http://www.shieldui.com/shared/components/latest/js/shieldui-all.min.js"></script>
    <script type="text/javascript" src="http://www.prepbootstrap.com/Content/js/gridData.js"></script>

    <script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css"/>

</head>
<body style="background-image:none">
<div id="wrapper">
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="navbar-header" style="background-color:#535358">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index">Admin Panel</a>
        </div>
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <li class="active"><a href="overview"><i class="fa fa-bullseye"></i> Overview</a></li>
                <li><a href="executive"><i class="fa fa-tasks"></i> Executive Details</a></li>
                <li><a href="vehicle"><i class="fa fa-globe"></i> Vehicle Details</a></li>
                <li><a href="business"><i class="fa fa-list-ol"></i> Business Model</a></li>
                <li><a href="promotions"><i class="fa fa-font"></i> Promotions</a></li>
                <li><a href="booking"><i class="fa fa-font"></i> Scheduled Bookings</a></li>
                <li><a href="cancellation"><i class="fa fa-font"></i> Cancellation</a></li>
                <li><a href="feedbacks"><i class="fa fa-font"></i> Feedbacks</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right navbar-user">

                <li class="dropdown user-dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Steve Miller<b
                                class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="#"><i class="fa fa-user"></i> Profile</a></li>
                        <li><a href="#"><i class="fa fa-gear"></i> Settings</a></li>
                        <li class="divider"></li>
                        <li><a href="#"><i class="fa fa-power-off"></i> Log Out</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>

    <div id="page-wrapper">
        <div class="row" style="margin-bottom:20px">
            <div class="col-lg-3">

                <div class="mdl-selectfield">


                    <label style="float:left;color:#b8b8b8;">From Date</label>
                    <input type="date" class="regdate" name="date" min="2017-01-01">

                </div>

            </div>
            <div class="col-lg-3">


                <label style="float:left;color:#b8b8b8;">To Date</label>
                <input type="date" class="regdate" name="date" min="2017-01-01">

            </div>
            <div class="col-lg-3">

                <div class="mdl-selectfield">
                    <label style="float:left;color:#b8b8b8;padding-bottom:8px">Select Vehicle No.</label>
                    <select class="browser-default">
                        <option value="" disabled selected>Choose your option</option>
                        <option value="1">Model 1</option>
                        <option value="2">Model 2</option>
                        <option value="3">Model 3</option>
                    </select>
                </div>

            </div>
            <div class="col-lg-3">

                <div class="mdl-selectfield">
                    <label style="float:left;color:#b8b8b8;padding-bottom:8px">Select Executive Id</label>
                    <select class="browser-default">
                        <option value="" disabled selected>Choose your option</option>
                        <option value="1">Model 1</option>
                        <option value="2">Model 2</option>
                        <option value="3">Model 3</option>
                    </select>
                </div>

            </div>
        </div>
        <div class="row" style="margin-bottom:20px">

            <div class="col-lg-3">

                <div class="mdl-selectfield">
                    <label style="float:left;color:#b8b8b8;padding-bottom:8px">Select Service Status</label>
                    <select class="browser-default">
                        <option value="" disabled selected>Choose your option</option>
                        <option value="1">Model 1</option>
                        <option value="2">Model 2</option>
                        <option value="3">Model 3</option>
                    </select>
                </div>

            </div>
            <div class="col-lg-3">

                <div class="mdl-selectfield">
                    <label style="float:left;color:#b8b8b8;padding-bottom:8px">Payment Mode</label>
                    <select class="browser-default">
                        <option value="" disabled selected>Choose your option</option>
                        <option value="1">Model 1</option>
                        <option value="2">Model 2</option>
                        <option value="3">Model 3</option>
                    </select>
                </div>

            </div>


            <div class="col-lg-3">

                <div class="mdl-selectfield">

                    <button type="button" class="btn btn-primary" style="width:60%;align:bottom;margin-top:25px">
                        Search
                    </button>

                </div>

            </div>
            <div class="col-lg-3">


            </div>

        </div>
        <div class="row" style="margin-bottom:20px">
            <div class="col-lg-8">
                <h4> Discounts and the applied promo codes can be seen while viewing the details </h4>
            </div>

            <div class="col-lg-4">
                <img src="images/export_excel.png" style="float:right">
            </div>
        </div>


        <div class="row">
            <div class="col-lg-4">
                <div class="panel panel-default ">
                    <div class="panel-body alert-info" style="background-color:#598A87">


                        <h4 class="alerts-text text-center">Total Orders</h4>
                        <p class="alerts-heading text-center">{{$total_orders}}</p>

                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="panel panel-default ">
                    <div class="panel-body alert-info" style="background-color:#45B3A5">

                        <h4 class="alerts-text text-center">Total Revenue</h4>
                        <p class="alerts-heading text-center">{{$total_revenue}}</p>


                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="panel panel-default ">
                    <div class="panel-body alert-info" style="background-color:#FD999A">

                        <h4 class="alerts-text text-center">Total Rejection</h4>
                        <p class="alerts-heading text-center">{{$total_rejection}}</p>


                    </div>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Customer's Orders Data</h3>
                    </div>
                </div>
            </div>

        </div>


        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">

                        <div class="table-responsive">
                            <table class="table table-striped font-12" id="datatable">
                                <thead>
                                <tr>
                                    <th>Booking Type</th>
                                    <th>Customer ID</th>
                                    <th>Contact No.</th>
                                    <th>Address</th>
                                    <th>Booking Time</th>
                                    <th>Service Status</th>
                                    <th>Executive ID</th>
                                    <th>Vehicle No.</th>
                                    <th>Revenue</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>Booking Type</th>
                                    <th>Customer ID</th>
                                    <th>Contact No.</th>
                                    <th>Address</th>
                                    <th>Booking Time</th>
                                    <th>Service Status</th>
                                    <th>Executive ID</th>
                                    <th>Vehicle No.</th>
                                    <th>Revenue</th>
                                    <th>Actions</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.col -->
        </div>


        <div class="modal modal-scale fade" id="viewModal">
            <div class="modal-dialog modal-md">
                <div class="modal-content" style="background-color: rgba(0,0,0,0.8);color:#fff; width: 120%">
                    <div class="modal-header">
                        <div class="col-lg-12 text-center">


                            <h3 style="color: white;margin-bottop: 10px">Booking Details</h3>
                        </div>



                    </div>
                    <div class="modal-body" style="margin: auto">

                            <div class="row">
                                <div class="col-lg-12 text-center">


                                    <div class="col-lg-6 text-center">


                                        <label style="color: white;text-align: left">Customer Name : </label><input
                                                type="text"
                                                name="view_customer_name"
                                                id="view_customer_name"

                                                readonly
                                                style="color: white;background-color:transparent;outline: none;border: none; padding:0px; margin-bottom:5px;margin-top:5px"><br>

                                        <label style="color: white;text-align: left">Customer ID :</label><input
                                                type="text"
                                                name="view_customer_id"
                                                id="view_customer_id"

                                                readonly
                                                style="color: white;background-color:transparent;outline: none;border: none; padding:0px; margin-bottom:5px;margin-top:5px"><br>
                                        <label style="color: white;text-align: left">Contact No. : </label><input
                                                type="text"
                                                name="view_customer_phone_no"
                                                id="view_customer_phone_no"

                                                readonly
                                                style="color: white;background-color:transparent;outline: none;border: none; padding:0px; margin-bottom:5px;margin-top:5px"><br>

                                        <label style="color: white;text-align: left">Address : </label><input
                                                type="text"
                                                name="view_customer_address"
                                                id="view_customer_address"

                                                readonly
                                                style="color: white;background-color:transparent;outline: none;border: none; padding:0px; margin-bottom:5px;margin-top:5px"><br>


                                        <label style="color: white;text-align: left">Service Time : </label><input
                                                type="text"
                                                name="view_service_time"
                                                id="view_service_time"

                                                readonly
                                                style="color: white;background-color:transparent;outline: none;border: none; padding:0px; margin-bottom:5px;margin-top:5px"><br>


                                        <label style="color: white;text-align: left">Service Status :</label><input
                                                type="text"
                                                name="view_service_status"
                                                id="view_service_status"

                                                readonly
                                                style="color: white;background-color:transparent;outline: none;border: none; padding:0px; margin-bottom:5px;margin-top:5px"><br>


                                        <label style="color: white;text-align: left">Price(Rs) : </label><input
                                                type="text"
                                                name="view_price"
                                                id="view_price"

                                                readonly
                                                style="color: white;background-color:transparent;outline: none;border: none; padding:0px; margin-bottom:5px;margin-top:5px"><br>


                                        <label style="color: white;text-align: left">Discount Given : </label><input
                                                type="text"
                                                name="view_promotion_value"
                                                id="view_promotion_value"

                                                readonly
                                                style="color: white;background-color:transparent;outline: none;border: none; padding:0px; margin-bottom:5px;margin-top:5px"><br>


                                        <label style="color: white;text-align: left">Payment Mode :</label><input
                                                type="text"
                                                name="view_payment_mode"
                                                id="view_payment_mode"

                                                readonly
                                                style="color: white;background-color:transparent;outline: none;border: none; padding:0px; margin-bottom:5px;margin-top:5px"><br>


                                        <label style="color: white;text-align: left">Rating :</label><input type="text"
                                                                                                            name="view_stars"
                                                                                                            id="view_stars"

                                                                                                            readonly
                                                                                                            style="color: white;background-color:transparent;outline: none;border: none; padding:0px; margin-bottom:5px;margin-top:5px"><br>


                                    </div>

                                    <div class="col-lg-6 text-center">

                                        <label style="color: white;text-align: left">Booked By : </label><input
                                                type="text"
                                                name="view_executive_name"
                                                id="view_executive_name"

                                                readonly
                                                style="color: white;background-color:transparent;outline: none;border: none; padding:0px; margin-bottom:5px;margin-top:5px"><br>

                                        <label style="color: white;text-align: left">Executive ID : </label><input
                                                type="text"
                                                name="view_executive_custom_id"
                                                id="view_executive_custom_id"

                                                readonly
                                                style="color: white;background-color:transparent;outline: none;border: none; padding:0px; margin-bottom:5px;margin-top:5px"><br>

                                        <label style="color: white;text-align: left">Vehicle No. : </label><input
                                                type="text"
                                                name="view_vehicle_registration_no"
                                                id="view_vehicle_registration_no"

                                                readonly
                                                style="color: white;background-color:transparent;outline: none;border: none; padding:0px; margin-bottom:5px;margin-top:5px"><br>

                                        <label style="color: white;text-align: left">Booking Time : </label><input
                                                type="text"
                                                name="view_booking_time"
                                                id="view_booking_time"

                                                readonly
                                                style="color: white;background-color:transparent;outline: none;border: none; padding:0px; margin-bottom:5px;margin-top:5px"><br>


                                        <label style="color: white;text-align: left">Clothes Count : </label><input
                                                type="text"
                                                name="view_clothes_count"
                                                id="view_clothes_count"

                                                readonly
                                                style="color: white;background-color:transparent;outline: none;border: none; padding:0px; margin-bottom:5px;margin-top:5px"><br>


                                        <label style="color: white;text-align: left">Clothes Weight : </label><input
                                                type="text"
                                                name="view_clothes_weight"
                                                id="view_clothes_weight"

                                                readonly
                                                style="color: white;background-color:transparent;outline: none;border: none; padding:0px; margin-bottom:5px;margin-top:5px"><br>


                                        <label style="color: white;text-align: left">Promo Code : </label><input
                                                type="text"
                                                name="view_promotion_value"
                                                id="view_promotion_value"

                                                readonly
                                                style="color: white;background-color:transparent;outline: none;border: none; padding:0px; margin-bottom:5px;margin-top:5px"><br>


                                        <label style="color: white;text-align: left">Total Price : </label><input
                                                type="text"
                                                name="view_final_price"
                                                id="view_final_price"

                                                readonly
                                                style="color: white;background-color:transparent;outline: none;border: none; padding:0px; margin-bottom:5px;margin-top:5px"><br>


                                        <label style="color: white;">Cancelled for : </label><input type="text"
                                                                                                    name="view_cancellation_reason"
                                                                                                    id="view_cancellation_reason"

                                                                                                    readonly
                                                                                                    style="color: white;background-color:transparent;outline: none;border: none; padding:0px; margin-bottom:5px;margin-top:5px"><br>


                                        <label style="color: white;text-align: left">Feedback : </label><input
                                                type="text"
                                                name="view_feedback"
                                                id="view_feedback"

                                                readonly
                                                style="color: white;background-color:transparent;outline: none;border: none; padding:0px; margin-bottom:5px;margin-top:5px"><br>


                                    </div>
                                </div>

                            </div>


                            {{----}}
                            <div class="modal-footer" style="margin-top:10px ">
                                <div class="pull-right">

                                    <button type="button" class="btn btn-primary edit-toggle" data-dismiss="modal"
                                            style="background-color: red">
                                        Close
                                    </button>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>
<!-- /#wrapper -->

<script type="text/javascript">


    $(document).ready(function () {
        $('#datatable').DataTable({
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
            processing: true,
            serverSide: true,
            ajax: '/admin/dashboard-ajax-handler',
            columns: [
                {data: 'booking_type', name: 'booking_type'},
                {data: 'customer_id', name: 'customer_id'},
                {data: 'customer_phone_no', name: 'customer_phone_no'},
                {data: 'customer_address', name: 'customer_address'},
                {data: 'booking_time', name: 'booking_time'},

                {data: 'service_status', name: 'service_status'},
                {data: 'executive_id', name: 'executive_id'},
                {data: 'vehicle_registration_no', name: 'vehicle_registration_no'},
                {data: 'revenue', name: 'revenue'},
                {data: 'action', name: 'action'}
            ],


            initComplete: function () {
                this.api().columns().every(function () {
                    var column = this;
                    var input = document.createElement("input");
                    $(input).appendTo($(column.footer()).empty())
                        .on('change', function () {
                            column.search($(this).val(), false, false, true).draw();
                        });
                });
            }
        });


        $(document).on('click', '#viewBooking', function (e) {
            e.preventDefault();
            $("#viewModal").modal("show");
            var booking_id = $(this).attr('data-id');


            $.ajax({
                url: '/admin/dashboard-details-ajax-handler',
                type: 'post',
                dataType: 'json',
                data: {
                    method: 'getBookingById',
                    booking_id: booking_id

                },
                beforeSend: function () {
                },
                success: function (response) {
                    console.log(response);


                    $('#view_customer_name').val(response.customer_name);
                    $('#view_customer_id').val(response.customer_id);
                    $('#view_customer_phone_no').val(response.customer_phone_no);
                    $('#view_customer_address').val(response.customer_address);
                    $('#view_executive_id').val(response.executive_id);
                    $('#view_executive_custom_id').val(response.executive_custom_id);
                    $('#view_executive_name').val(response.executive_name);
                    $('#view_booking_time').val(response.booking_time);
                    $('#view_vehicle_registration_no').val(response.vehicle_registration_no);
                    $('#view_service_time').val(response.service_time);
                    $('#view_service_status').val(response.service_status);
                    $('#view_clothes_count').val(response.clothes_count);
                    $('#view_clothes_weight').val(response.clothes_weight);
                    $('#view_cancellation_reason').val(response.cancellation_reason);
                    $('#view_stars').val(response.stars);
                    $('#view_price').val(response.price);
                    $('#view_final_price').val(response.final_price);
                    $('#view_promotion_id').val(response.promotion_id);
                    $('#view_promotion_value').val(response.promotion_value);
                    $('#view_payment_mode').val(response.payment_mode);


                }
            });
        });


    });
</script>
</body>
</html>
