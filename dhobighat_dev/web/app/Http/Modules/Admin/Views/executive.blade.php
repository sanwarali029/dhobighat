<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dhobi Ghat Admin</title>

    <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="/assets/font-awesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="/assets/css/local.css"/>
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css"/>

    <script type="text/javascript" src="/assets/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="/assets/bootstrap/js/bootstrap.min.js"></script>

    <script type="text/javascript" src="/assets/sweetalert/sweetalert.min.js"></script>
    <script type="text/javascript" src="/assets/sweetalert/sweetalert-dev.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>

    <!-- you need to include the shieldui css and js assets in order for the charts to work -->
    <link rel="stylesheet" type="text/css"
          href="http://www.shieldui.com/shared/components/latest/css/light-bootstrap/all.min.css"/>
    <script type="text/javascript"
            src="http://www.shieldui.com/shared/components/latest/js/shieldui-all.min.js"></script>
    <script type="text/javascript" src="http://www.prepbootstrap.com/Content/js/gridData.js"></script>
</head>
<body style="background-image:none">
<div id="wrapper">
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="navbar-header" style="background-color:#535358">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index">Admin Panel</a>
        </div>
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <li><a href="overview"><i class="fa fa-bullseye"></i> Overview</a></li>
                <li class="active"><a href="executive"><i class="fa fa-tasks"></i> Executive Details</a></li>
                <li><a href="vehicle"><i class="fa fa-globe"></i> Vehicle Details</a></li>
                <li><a href="business"><i class="fa fa-list-ol"></i> Business Model</a></li>
                <li><a href="promotions"><i class="fa fa-font"></i> Promotions</a></li>
                <li><a href="booking"><i class="fa fa-font"></i> Scheduled Bookings</a></li>
                <li><a href="cancellation"><i class="fa fa-font"></i> Cancellation</a></li>
                <li><a href="feedbacks"><i class="fa fa-font"></i> Feedbacks</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right navbar-user">

                <li class="dropdown user-dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Steve Miller<b
                                class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="#"><i class="fa fa-user"></i> Profile</a></li>
                        <li><a href="#"><i class="fa fa-gear"></i> Settings</a></li>
                        <li class="divider"></li>
                        <li><a href="login"><i class="fa fa-power-off"></i> Log Out</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>

    <div id="page-wrapper">


        <form action="/admin/add-executive" method="POST">
            <div class="form-group">
                <div class="row" style="margin-bottom:20px">
                    <h2 style="margin-bottom:20px;margin-left:20px">Add Executive</h2>


                    <div class="col-lg-3">

                        <label style="float:left;color:#b8b8b8;">Executive Name</label>
                        <input type="text" name="executive_name" id="executive_name" value="{{ old('executive_name') }}" class="regdate"
                               placeholder="please type In" style="padding:8px" required>
                    </div>

                    <div class="col-lg-3">

                        <label style="float:left;color:#b8b8b8;">Driving Licence No</label>
                        <input type="text" name="executive_driving_licence_no" value="{{ old('executive_driving_licence_no') }}" id="executive_driving_licence_no"
                               class="regdate" placeholder="please type in" style="padding:8px" required>
                    </div>

                    <div class="col-lg-3">

                        <label style="float:left;color:#b8b8b8;">Date Of Birth</label>
                        <input type="date" name="executive_date_of_birth" value="{{ old('executive_date_of_birth') }}" id="executive_date_of_birth" class="regdate"
                               required>
                    </div>

                    <div class="col-lg-3">


                        <label style="float:left;color:#b8b8b8;">Mobile No</label>
                        <input type="number" name="executive_mobile_no" value="{{ old('executive_mobile_no') }}" id="executive_mobile_no" class="regdate"
                               placeholder="please type in" style="padding:8px" required>
                    </div>
                </div>


                <div class="row" style="margin-bottom:20px">

                    <div class="col-lg-3">


                        <label style="float:left;color:#b8b8b8;">Email Id</label>
                        <input type="email" name="executive_email" value="{{ old('executive_email') }}" id="executive_email" class="regdate"
                               placeholder="please type in" style="padding:8px" required>
                    </div>

                    <div class="col-lg-6">

                        <label style="float:left;color:#b8b8b8;">Address</label>
                        <input type="text" name="executive_address" id="executive_address" value="{{ old('executive_address') }}" class="regdate"
                               placeholder="please type in" style="padding:8px" required>
                    </div>


                    <div class="col-lg-3">

                        <label style="float:left;color:#b8b8b8;">Executive Id</label>
                        <input type="text" name="executive_custom_id" id="executive_custom_id" value="{{ old('executive_custom_id') }}" class="regdate"
                               placeholder="please type in" style="padding:8px" required>
                    </div>
                </div>


                <div class="row" style="margin-bottom:20px">
                    <div class="col-lg-3">

                        <input type="submit" class="btn btn-primary" style="width:60%;align:bottom;margin-top:25px"
                               value="Add">
                    </div>
                </div>
            </div>
        </form>


        @if (count($errors) > 0)

            @if ($errors->has('executive_name')||$errors->has('executive_email')||$errors->has('executive_mobile_no')||$errors->has('executive_custom_id')||$errors->has('executive_driving_licence_no')||$errors->has('executive_date_of_birth'))
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach

                    </ul>
                </div>
            @endif
        @endif



        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        {{--<div class="text-right">--}}
                        {{--<button data-toggle="modal" id="addCab" data-id="" class="btn btn-info addCabinfo">--}}
                        {{--<i class="fa fa-plus"></i>Add Cab--}}
                        {{--</button>--}}
                        {{--</div>--}}
                        <div class="table-responsive">
                            <table class="table table-striped font-12" id="datatable">
                                <thead>
                                <tr>
                                    <th>Executive Id</th>
                                    <th>Executive Name</th>
                                    <th>Contact No.</th>

                                    <th>Address</th>
                                    <th>Driving Licence</th>
                                    <th>Last Logged in Vehicle No.</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>Executive Id</th>
                                    <th>Executive Name</th>
                                    <th>Contact No.</th>

                                    <th>Address</th>
                                    <th>Driving Licence</th>
                                    <th>Last Logged in Vehicle No.</th>
                                    <th>Actions</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->


        <div class="modal modal-scale fade" id="editModal">
            <div class="modal-dialog modal-md">
                <div class="modal-content" style="background-color: rgba(0,0,0,0.7);color:#fff;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    style="color: #ffffff;border-bottom-color: transparent"
                                    aria-hidden="true">&times;</span></button>

                        @if (count($errors) > 0)
                        @if ($errors->has('edit_executive_email')||$errors->has('edit_executive_mobile_no')||$errors->has('edit_executive_custom_id')||$errors->has('edit_executive_name')||$errors->has('edit_executive_driving_licence_no')||$errors->has('edit_executive_address')||$errors->has('edit_executive_date_of_birth')||$errors->has('executive_new_password')||$errors->has('executive_confirm_new_password'))

                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach

                                </ul>
                            </div>


                        @endif
                        @endif



                    </div>
                    <div class="modal-body" style="margin: auto">
                        <form action="/admin/edit-executive" role="form" id="editexecutiveform"
                              class="editexecutiveform"
                              method="post" enctype="multipart/form-data" style="padding: 20px">
                            <h3 style="color: white;margin-top: -20px">Personal Details</h3>

                            <input type="text" name="hidden_executive_id" id="hidden_executive_id"
                                   style="display: none;color: black;width:100%; padding:10px; margin-bottom:5px;margin-top:0px"><br>


                            <input type="text" name="edit_executive_custom_id" id="edit_executive_custom_id" value="{{ old('edit_executive_custom_id') }}"
                                   style="color: black;width:100%; padding:10px; margin-bottom:5px;margin-top:5px"><br>

                            <input type="text" name="edit_executive_name" id="edit_executive_name" value="{{ old('edit_executive_name') }}"
                                   style="color:black;width:100%; padding:10px; margin-bottom:5px;margin-top:5px"><br>

                            <input type="text" name="edit_executive_mobile_no" id="edit_executive_mobile_no" value="{{ old('edit_executive_mobile_no') }}"
                                   style="color: black;width:100%; padding:10px; margin-bottom:5px;margin-top:5px"><br>

                            <input type="text" name="edit_executive_email" id="edit_executive_email" value="{{ old('edit_executive_email') }}"
                                   style="color: black;width:100%; padding:10px; margin-bottom:5px;margin-top:5px"><br>

                            <input type="text" name="edit_executive_driving_licence_no" id="edit_executive_driving_licence_no" value="{{ old('edit_executive_driving_licence_no') }}"
                                   style="color: black;width:100%; padding:10px; margin-bottom:5px;margin-top:5px"><br>

                            <input type="text" name="edit_executive_address" id="edit_executive_address" value="{{ old('edit_executive_address') }}"
                                   style="color: black;width:100%; padding:10px; margin-bottom:5px;margin-top:5px"><br>


                            <h3 style="color: white">App Settings</h3>

                            <input type="password" name="executive_new_password" id="executive_new_password"
                                   placeholder="New Password"
                                   style="color:black;width:100%; padding:10px; margin-bottom:5px;margin-top:5px"><br>

                            <input type="password" name="executive_confirm_new_password" id="executive_confirm_new_password"
                                   placeholder="Confirm new password"
                                   style="color:black;width:100%; padding:10px; margin-bottom:5px;margin-top:5px"><br>


                            <div class="modal-footer" style="margin-top:10px ">
                                <div class="pull-right">
                                    <input type="submit" class="btn btn-primary editexecutiveinfo" data-id=""
                                           value="Update" id="edit-executive" style="background-color: #3e3232"/>
                                    <button type="button" class="btn btn-primary edit-toggle" data-dismiss="modal"
                                            style="background-color: red">
                                        Cancel
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>





        <!-- Start Modal -->
        <div class="modal modal-scale fade" id="deleteModal">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title font-header text-dark">Delete Executive</h4>
                    </div>
                    <form action="/admin/delete-executive" role="form" id="deleteform">
                        <div class="modal-body">
                            <div class="form-group form-input-group m-t-5 m-b-5">
                                <p> Are you sure you want to delete the Executive ?? </p>

                                <input type="text" name="delete_id" id="delete_id"
                                       style="display: none;color: black;width:100%; padding:10px; margin-bottom:5px;margin-top:0px"><br>

                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary deleteexecutiveinfo" style="background-color: #3e3232" data-id="">Delete</button>
                            <button type="button" class="btn btn-primary" data-dismiss="modal"  style="background-color: red">Cancel</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- End Modal -->






    </div>
</div>

@include('sweet::alert')
</body>


<script type="text/javascript">

    @if ($errors->has('edit_executive_email')||$errors->has('edit_executive_mobile_no')||$errors->has('edit_executive_custom_id')||$errors->has('edit_executive_name')||$errors->has('edit_executive_driving_licence_no')||$errors->has('edit_executive_address')||$errors->has('edit_executive_date_of_birth')||$errors->has('executive_new_password')||$errors->has('executive_confirm_new_password'))

        $("#editModal").modal("show");

    @endif
</script>

<script type="text/javascript">

    $(document).ready(function () {
        $('#datatable').DataTable({
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
            processing: true,
            serverSide: true,
            ajax: '/admin/executive-ajax-handler',
            columns: [
                {data: 'executive_custom_id', name: 'executive_custom_id'},
                {data: 'executive_name', name: 'executive_name'},
                {data: 'executive_mobile_no', name: 'executive_mobile_no'},
                {data: 'executive_address', name: 'executive_address'},
                {data: 'executive_driving_licence_no', name: 'executive_driving_licence_no'},
                {data: 'executive_last_logged_in_vehicle', name: 'executive_last_logged_in_vehicle'},
                {data: 'action', name: 'action'}
            ],
            initComplete: function () {
                this.api().columns().every(function () {
                    var column = this;
                    var input = document.createElement("input");
                    $(input).appendTo($(column.footer()).empty())
                        .on('change', function () {
                            column.search($(this).val(), false, false, true).draw();
                        });
                });
            }
        });


        $(document).on('click', '#editExecutive', function (e) {
            e.preventDefault();
            $("#editModal").modal("show");
            var executive_custom_id = $(this).attr('data-id');


            $.ajax({
                url: '/admin/executive-details-ajax-handler',
                type: 'post',
                dataType: 'json',
                data: {
                    method: 'getExecutiveById',
                    executive_custom_id: executive_custom_id

                },
                beforeSend: function () {
                },
                success: function (response) {
                    console.log(response);

                    $('#edit_executive_custom_id').val(response.executive_custom_id);
                    $('#hidden_executive_id').val(response.executive_id);
                    $('#edit_executive_name').val(response.executive_name);
                    $('#edit_executive_mobile_no').val(response.executive_mobile_no);
                    $('#edit_executive_email').val(response.executive_email);
                    $('#edit_executive_driving_licence_no').val(response.executive_driving_licence_no);
                    $('#edit_executive_address').val(response.executive_address);


                }
            });
        });



        $(document).on('click', '.deleteExecutive', function (e) {
            e.preventDefault();
            $("#deleteModal").modal("show");
            var executive_custom_id = $(this).attr('data-id');

            $.ajax({
                url: '/admin/executive-details-ajax-handler',
                type: 'post',
                dataType: 'json',
                data: {
                    method: 'getExecutiveById',
                    executive_custom_id: executive_custom_id
                },
                beforeSend: function () {
                },
                success: function (response) {

                    console.log(response);
                    $('#delete_id').val(response.executive_id);

                },

            });
        });





    });

</script>


</html>
