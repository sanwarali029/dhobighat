<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dhobi Ghat Admin</title>

    <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="/assets/font-awesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="/assets/css/local.css"/>

    <script type="text/javascript" src="/assets/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="/assets/bootstrap/js/bootstrap.min.js"></script>

    <!-- you need to include the shieldui css and js assets in order for the charts to work -->
    <link rel="stylesheet" type="text/css"
          href="http://www.shieldui.com/shared/components/latest/css/light-bootstrap/all.min.css"/>
    <script type="text/javascript"
            src="http://www.shieldui.com/shared/components/latest/js/shieldui-all.min.js"></script>
    <script type="text/javascript" src="http://www.prepbootstrap.com/Content/js/gridData.js"></script>

    <script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css"/>

</head>
<body style="background-image:none">
<div id="wrapper">
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="navbar-header" style="background-color:#535358">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index">Admin Panel</a>
        </div>
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <li ><a href="overview"><i class="fa fa-bullseye"></i> Overview</a></li>
                <li><a href="executive"><i class="fa fa-tasks"></i> Executive Details</a></li>
                <li><a href="vehicle"><i class="fa fa-globe"></i> Vehicle Details</a></li>
                <li><a href="business"><i class="fa fa-list-ol"></i> Business Model</a></li>
                <li><a href="promotions"><i class="fa fa-font"></i> Promotions</a></li>
                <li><a href="booking"><i class="fa fa-font"></i> Scheduled Bookings</a></li>
                <li><a href="cancellation"><i class="fa fa-font"></i> Cancellation</a></li>
                <li class="active"><a href="feedbacks"><i class="fa fa-font"></i> Feedbacks</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right navbar-user">

                <li class="dropdown user-dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Steve Miller<b
                                class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="#"><i class="fa fa-user"></i> Profile</a></li>
                        <li><a href="#"><i class="fa fa-gear"></i> Settings</a></li>
                        <li class="divider"></li>
                        <li><a href="#"><i class="fa fa-power-off"></i> Log Out</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>

    <div id="page-wrapper">


        <div class="row">

            <div class="col-lg-2">

            </div>
            <div class="col-lg-4">
                <div class="panel panel-default ">
                    <div class="panel-body alert-info" style="background-color:#598A87">


                        <h4 class="alerts-text text-center">Total Orders Served</h4>
                        <p class="alerts-heading text-center">{{$total_served}}</p>

                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="panel panel-default ">
                    <div class="panel-body alert-info" style="background-color:#45B3A5">

                        <h4 class="alerts-text text-center">Average Rating</h4>
                        <p class="alerts-heading text-center">{{$avg_rating}}</p>


                    </div>
                </div>
            </div>
            <div class="col-lg-2">

            </div>

        </div>

        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Customer's Feedback Data</h3>
                    </div>
                </div>
            </div>

        </div>





        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">

                        <div class="table-responsive">
                            <table class="table table-striped font-12" id="datatable">
                                <thead>
                                <tr>
                                    <th>Booking ID</th>
                                    <th>Stars</th>
                                    <th>Comments</th>
                                    {{--<th>Actions</th>--}}
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>Booking ID</th>
                                    <th>Stars</th>
                                    <th>Comments</th>
                                    {{--<th>Actions</th>--}}
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.col -->
        </div>






    </div>
</div>
<!-- /#wrapper -->

<script type="text/javascript">



    $(document).ready(function () {
        $('#datatable').DataTable({
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
            processing: true,
            serverSide: true,
            ajax: '/admin/feedback-ajax-handler',
            columns: [
                {data: 'booking_id', name: 'booking_type'},
                {data: 'stars', name: 'stars'},
                {data: 'comments', name: 'comments'},
//                {data: 'action', name: 'action'}
            ],


            initComplete: function () {
                this.api().columns().every(function () {
                    var column = this;
                    var input = document.createElement("input");
                    $(input).appendTo($(column.footer()).empty())
                        .on('change', function () {
                            column.search($(this).val(), false, false, true).draw();
                        });
                });
            }
        });

    });
</script>
</body>
</html>
