<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dhobi Ghat Admin</title>

    <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="/assets/font-awesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="/assets/css/local.css"/>

    <script type="text/javascript" src="/assets/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="/assets/bootstrap/js/bootstrap.min.js"></script>

    <!-- you need to include the shieldui css and js assets in order for the charts to work -->
    <link rel="stylesheet" type="text/css"
          href="http://www.shieldui.com/shared/components/latest/css/light-bootstrap/all.min.css"/>
    <script type="text/javascript"
            src="http://www.shieldui.com/shared/components/latest/js/shieldui-all.min.js"></script>
    <script type="text/javascript" src="http://www.prepbootstrap.com/Content/js/gridData.js"></script>

    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css"/>
    <script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>

</head>
<body style="background-image:none">
<div id="wrapper">
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="navbar-header" style="background-color:#535358">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index">Admin Panel</a>
        </div>
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <li><a href="overview"><i class="fa fa-bullseye"></i> Overview</a></li>
                <li><a href="executive"><i class="fa fa-tasks"></i> Executive Details</a></li>
                <li><a href="vehicle"><i class="fa fa-globe"></i> Vehicle Details</a></li>
                <li><a href="business"><i class="fa fa-list-ol"></i> Business Model</a></li>
                <li class="active"><a href="promotions"><i class="fa fa-font"></i> Promotions</a></li>
                <li><a href="booking"><i class="fa fa-font"></i> Scheduled Bookings</a></li>
                <li><a href="cancellation"><i class="fa fa-font"></i> Cancellation</a></li>
                <li><a href="feedbacks"><i class="fa fa-font"></i> Feedbacks</a></li>

            </ul>
            <ul class="nav navbar-nav navbar-right navbar-user">

                <li class="dropdown user-dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Steve Miller<b
                                class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="#"><i class="fa fa-user"></i> Profile</a></li>
                        <li><a href="#"><i class="fa fa-gear"></i> Settings</a></li>
                        <li class="divider"></li>
                        <li><a href="login"><i class="fa fa-power-off"></i> Log Out</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>

    <div id="page-wrapper">
        <div class="row" style="margin-bottom:20px">
            <h3 style="margin-bottom:20px;margin-left:20px">Add Senario</h3>

            <div class="col-lg-4">


                <input type="text" class="regdate1" placeholder="Promocode" style="padding:8px;margin-bottom:10px">

            </div>
            <div class="col-lg-4">


                <label style="margin-left:10px;font-size:15px"><input type="checkbox"
                                                                      style="padding:8px;margin-top:10px;">Select if the
                    offer is for new users</label>

            </div>

            <div class="col-lg-1">


            </div>
        </div>
        <div class="row" style="margin-bottom:20px">

            <div class="col-lg-3">


                <label style="float:left;color:#b8b8b8;padding-bottom:0px">Promotion Name</label>
                <input type="email" class="regdate" style="padding:8px;margin-top:15px;margin-bottom:8px">

            </div>
            <div class="col-lg-3">

                <label style="float:left;color:#b8b8b8;padding-bottom:0px">From Date</label>
                <input type="date" class="regdate" style="padding:8px;margin-bottom:8px">


            </div>


            <div class="col-lg-3">
                <label style="float:left;color:#b8b8b8;padding-bottom:0px">To Date</label>
                <input type="date" class="regdate" style="padding:8px;margin-bottom:8px">


            </div>
            <div class="col-lg-3">

                <div class="mdl-selectfield">
                    <label style="float:left;color:#b8b8b8;padding-bottom:22px">Select Discount Type</label>
                    <select class="browser-default">
                        <option value="" disabled selected>Choose your option</option>
                        <option value="1">Type 1</option>
                        <option value="2">Type 2</option>
                        <option value="3">Type 3</option>
                    </select>
                </div>
            </div>


        </div>
        <div class="row" style="margin-bottom:20px;margin-top:30px">
            <div class="col-lg-3">


                <input type="text" class="regdate" placeholder="Discount Value" style="padding:8px;margin-bottom:10px">


            </div>
            <div class="col-lg-3">


                <input type="text" class="regdate" placeholder="On Booking Amount"
                       style="padding:8px;margin-bottom:10px">


            </div>
            <div class="col-lg-3">


                <input type="text" class="regdate" placeholder="On Booking Weight"
                       style="padding:8px;margin-bottom:10px">


            </div>
            <div class="col-lg-3">


            </div>
        </div>


        <div class="row" style="margin-bottom:20px;margin-top:30px">
            <div class="col-lg-3">


                <label style="margin-left:10px;font-size:15px"><input type="checkbox"
                                                                      style="padding:8px;margin-top:10px;">Show the
                    offers to users</label>


            </div>
            <div class="col-lg-3">


                <button type="button" class="btn btn-primary" style="width:60%;align:bottom;margin-top:0px">Add</button>


            </div>
            <div class="col-lg-6">


            </div>

        </div>

        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Promo Details</h3>
                    </div>
                </div>
            </div>

        </div>








        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">

                        <div class="table-responsive">
                            <table class="table table-striped font-12" id="datatable">
                                <thead>
                                <tr>
                                    <th>Promo Code</th>
                                    <th>From Date</th>
                                    <th>To Date</th>
                                    <th>Discount Type</th>
                                    <th>Discount Value</th>
                                    <th>Promotion Name</th>
                                    <th>For New Users?</th>
                                    <th>On Booking Amount</th>
                                    <th>On Booking Weight</th>
                                    <th>Users Visibility</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>Promo Code</th>
                                    <th>From Date</th>
                                    <th>To Date</th>
                                    <th>Discount Type</th>
                                    <th>Discount Value</th>
                                    <th>Promotion Name</th>
                                    <th>For New Users?</th>
                                    <th>On Booking Amount</th>
                                    <th>On Booking Weight</th>
                                    <th>Users Visibility</th>
                                    <th>Actions</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.col -->
        </div>








    </div>
</div>
<!-- /#wrapper -->

<script type="text/javascript">


    $(document).ready(function () {
        $('#datatable').DataTable({
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
            processing: true,
            serverSide: true,
            ajax: '/admin/promotions-ajax-handler',
            columns: [
                {data: 'promotion_code', name: 'promotion_code'},
                {data: 'promotion_from_date', name: 'promotion_from_date'},
                {data: 'promotion_to_date', name: 'promotion_to_date'},
                {data: 'promotion_discount_type', name: 'promotion_discount_type'},
                {data: 'promotion_discount_value', name: 'promotion_discount_value'},
                {data: 'promotion_name', name: 'promotion_name'},
                {data: 'promotion_for_new_users', name: 'promotion_for_new_users'},
                {data: 'promotion_on_booking_amount', name: 'promotion_on_booking_amount'},
                {data: 'promotion_on_booking_weight', name: 'promotion_on_booking_weight'},
                {data: 'promotion_visibility', name: 'promotion_visibility'},
                {data: 'action', name: 'action'}
            ],


            initComplete: function () {
                this.api().columns().every(function () {
                    var column = this;
                    var input = document.createElement("input");
                    $(input).appendTo($(column.footer()).empty())
                        .on('change', function () {
                            column.search($(this).val(), false, false, true).draw();
                        });
                });
            }
        });
    });


</script>


</body>
</html>
