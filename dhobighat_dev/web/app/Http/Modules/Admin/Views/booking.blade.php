
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dhobi Ghat Admin</title>

    <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="/assets/font-awesome/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="/assets/css/local.css" />

    <script type="text/javascript" src="/assets/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="/assets/bootstrap/js/bootstrap.min.js"></script>

    <!-- you need to include the shieldui css and js assets in order for the charts to work -->
    <link rel="stylesheet" type="text/css" href="http://www.shieldui.com/shared/components/latest/css/light-bootstrap/all.min.css" />
    <script type="text/javascript" src="http://www.shieldui.com/shared/components/latest/js/shieldui-all.min.js"></script>
    <script type="text/javascript" src="http://www.prepbootstrap.com/Content/js/gridData.js"></script>
</head>
<body style="background-image:none">
    <div id="wrapper">
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="navbar-header" style="background-color:#535358">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index">Admin Panel</a>
            </div>
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li><a href="overview"><i class="fa fa-bullseye"></i> Overview</a></li>
                    <li><a href="executive"><i class="fa fa-tasks"></i> Executive Details</a></li>
                    <li><a href="vehicle"><i class="fa fa-globe"></i> Vehicle Details</a></li>
                    <li><a href="business"><i class="fa fa-list-ol"></i> Business Model</a></li>
                    <li><a href="promotions"><i class="fa fa-font"></i> Promotions</a></li>
                    <li class="active"><a href="booking"><i class="fa fa-font"></i> Scheduled Bookings</a></li>
                    <li><a href="cancellation"><i class="fa fa-font"></i> Cancellation</a></li>
                    <li><a href="feedbacks"><i class="fa fa-font"></i> Feedbacks</a></li>
                    
                </ul>
                <ul class="nav navbar-nav navbar-right navbar-user">
                  
                     <li class="dropdown user-dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Steve Miller<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="#"><i class="fa fa-user"></i> Profile</a></li>
                            <li><a href="#"><i class="fa fa-gear"></i> Settings</a></li>
                            <li class="divider"></li>
                            <li><a href="login"><i class="fa fa-power-off"></i> Log Out</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>

        <div id="page-wrapper">
		   <div class="row" style="margin-bottom:20px">
                <h2 style="margin-bottom:20px;margin-left:20px">Add A Customer</h2>
				
				<div class="col-lg-2">
                    
				   
                                
						
                                 <label style="float:left;color:#b8b8b8;">Customer Name</label>
                                <input type="text" class="regdate" placeholder="please type In" style="padding:8px">
								
                          
				   
                </div>
				 <div class="col-lg-2">
                   
				 
                                 <label style="float:left;color:#b8b8b8;">Contact Number</label>
                                <input type="text" class="regdate" placeholder="please type in" style="padding:8px">
				   
                </div>
				 <div class="col-lg-4">
                 			 
				 
				 
                                 <label style="float:left;color:#b8b8b8;">Address</label>
                                <input type="text" class="regdate" placeholder="please type in" style="padding:8px">
				   
                </div>
				 <div class="col-lg-2">
                   
				  
                                 <label style="float:left;color:#b8b8b8;">Make Year</label>
                                <input type="date" class="regdate" placeholder="please type in" style="padding:0px">
				   
                </div>
				 <div class="col-lg-2">
                   
				  
						<button type="button" class="btn btn-primary" style="width:100%;align:bottom;margin-top:25px">Add</button>
								
                </div>
				
				
				
				
				
            </div>
            <div class="row" style="margin-bottom:20px">
               
				 <div class="col-lg-2">
                   
				 
				  
                                 <label style="float:left;color:#b8b8b8;">Make Year</label>
                                <input type="date" class="regdate" placeholder="please type in" style="padding:0px">
				   
                </div>
				 <div class="col-lg-3">
                   
				  
                                 <label style="float:left;color:#b8b8b8;">Make Year</label>
                                <input type="date" class="regdate" placeholder="please type in" style="padding:0px">
				  
				   
                </div>
				
				
				 <div class="col-lg-3">
                     <div class="mdl-selectfield">
                                 <label style="float:left;color:#b8b8b8;padding-bottom:8px">Select Executive ID</label>
                                 <select class="browser-default">
                                       <option value="" disabled selected>Choose your option</option>
                                       <option value="1">Type 1</option>
                                       <option value="2">Type 2</option>
                                       <option value="3">Type 3</option>
                                 </select>
                           </div>
				  
				   
                </div>
				
				 <div class="col-lg-3">
                   
                                
						<button type="button" class="btn btn-primary" style="width:60%;align:bottom;margin-top:25px">Search</button>
								
                          
				  
				   
                </div>
				
				
            </div>
			
			

            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Shceduled Bookings</h3>
                        </div>
                        <div class="panel-body">
                            <div id="shieldui-grid1"></div>
                        </div>
                    </div>
                </div>
               
            </div>

           
        </div>
    </div>
    <!-- /#wrapper -->

    <script type="text/javascript">
        jQuery(function ($) {
            var performance = [12, 43, 34, 22, 12, 33, 4, 17, 22, 34, 54, 67],
                visits = [123, 323, 143, 132, 274, 223, 143, 156, 223, 223],
                budget = [23, 19, 11, 34, 42, 52, 35, 22, 37, 45, 55, 57],
                sales = [11, 9, 31, 34, 42, 52, 35, 22, 37, 45, 55, 57],
                targets = [17, 19, 5, 4, 62, 62, 75, 12, 47, 55, 65, 67],
                avrg = [117, 119, 95, 114, 162, 162, 175, 112, 147, 155, 265, 167];

            $("#shieldui-chart1").shieldChart({
                primaryHeader: {
                    text: "Visitors"
                },
                exportOptions: {
                    image: false,
                    print: false
                },
                dataSeries: [{
                    seriesType: "area",
                    collectionAlias: "Q Data",
                    data: performance
                }]
            });

            $("#shieldui-chart2").shieldChart({
                primaryHeader: {
                    text: "Login Data"
                },
                exportOptions: {
                    image: false,
                    print: false
                },
                dataSeries: [
                    {
                        seriesType: "polarbar",
                        collectionAlias: "Logins",
                        data: visits
                    },
                    {
                        seriesType: "polarbar",
                        collectionAlias: "Avg Visit Duration",
                        data: avrg
                    }
                ]
            });

            $("#shieldui-chart3").shieldChart({
                primaryHeader: {
                    text: "Sales Data"
                },
                dataSeries: [
                    {
                        seriesType: "bar",
                        collectionAlias: "Budget",
                        data: budget
                    },
                    {
                        seriesType: "bar",
                        collectionAlias: "Sales",
                        data: sales
                    },
                    {
                        seriesType: "spline",
                        collectionAlias: "Targets",
                        data: targets
                    }
                ]
            });

            $("#shieldui-grid1").shieldGrid({
                dataSource: {
                    data: gridData
                },
                sorting: {
                    multiple: true
                },
                paging: {
                    pageSize: 7,
                    pageLinksCount: 4
                },
                selection: {
                    type: "row",
                    multiple: true,
                    toggle: false
                },
                columns: [
                    { field: "id",  title: "Executive ID" },
					{ field: "phone", title: "Executive Name" },
                    { field: "Contact", title: "Contact No." },
                    { field: "licence", title: "Driving Licence"},
                    { field: "Address", title: "Address"},
					{ field: "Logged in Vehicle no.", title: "Logged in Vehicle"},
				
				  { field: "more", title: "More"},
					
                ]
            });
        });
    </script>
</body>
</html>
