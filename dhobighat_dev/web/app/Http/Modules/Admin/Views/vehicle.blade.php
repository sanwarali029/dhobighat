<!DOCTYPE html>
<html lang="en" xmlns:padding="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dhobi Ghat Admin</title>

    <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="/assets/font-awesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="/assets/css/local.css"/>
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css"/>
    <script type="text/javascript" src="/assets/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="/assets/bootstrap/js/bootstrap.min.js"></script>

    <!-- you need to include the shieldui css and js assets in order for the charts to work -->
    <link rel="stylesheet" type="text/css"
          href="http://www.shieldui.com/shared/components/latest/css/light-bootstrap/all.min.css"/>
    <script type="text/javascript"
            src="http://www.shieldui.com/shared/components/latest/js/shieldui-all.min.js"></script>
    <script type="text/javascript" src="http://www.prepbootstrap.com/Content/js/gridData.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
</head>
<body style="background-image:none">
<div id="wrapper">
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="navbar-header" style="background-color:#535358">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index">Admin Panel</a>
        </div>
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <li><a href="overview"><i class="fa fa-bullseye"></i> Overview</a></li>
                <li><a href="executive"><i class="fa fa-tasks"></i> Executive Details</a></li>
                <li class="active"><a href="vehicle"><i class="fa fa-globe"></i> Vehicle Details</a></li>

                <li><a href="business"><i class="fa fa-list-ol"></i> Business Model</a></li>
                <li><a href="promotions"><i class="fa fa-font"></i> Promotions</a></li>
                <li><a href="booking"><i class="fa fa-font"></i> Scheduled Bookings</a></li>
                <li><a href="cancellation"><i class="fa fa-font"></i> Cancellation</a></li>
                <li><a href="feedbacks"><i class="fa fa-font"></i> Feedbacks</a></li>

            </ul>
            <ul class="nav navbar-nav navbar-right navbar-user">

                <li class="dropdown user-dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Steve Miller<b
                                class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="#"><i class="fa fa-user"></i> Profile</a></li>
                        <li><a href="#"><i class="fa fa-gear"></i> Settings</a></li>
                        <li class="divider"></li>
                        <li><a href="login"><i class="fa fa-power-off"></i> Log Out</a></li>
                    </ul>
                </li>

            </ul>
        </div>
    </nav>

    <div id="page-wrapper">

        <form action="/admin/add-vehicle" method="POST">
        <div class="form-group">

        <div class="row" style="margin-bottom:20px">
            <h2 style="margin-bottom:20px;margin-left:20px">Add Vehicle</h2>

            <div class="col-lg-3">


                <label style="float:left;color:#b8b8b8;">Make</label>
                <input type="text" class="regdate" required placeholder="please type In" id="vehicle_make"
                       name="vehicle_make" value="{{ old('vehicle_make') }}" style="padding:8px">


            </div>
            <div class="col-lg-3">


                <label style="float:left;color:#b8b8b8;">Model</label>
                <input type="text" class="regdate" required id="vehicle_model" name="vehicle_model"
                       value="{{ old('vehicle_model') }}" placeholder="please type in" style="padding:8px">

            </div>
            <div class="col-lg-3">

                <label style="float:left;color:#b8b8b8;">Date Of Purchase</label>
                <input type="date" class="regdate" required id="vehicle_date_of_purchase"
                       name="vehicle_date_of_purchase" value="{{ old('vehicle_date_of_purchase') }}">

            </div>
            <div class="col-lg-3">


                <label style="float:left;color:#b8b8b8;">Make Year</label>
                <input type="number" class="regdate" required id="vehicle_make_year" name="vehicle_make_year"
                       value="{{ old('vehicle_make_year') }}" placeholder="please type in" style="padding:8px">

            </div>
        </div>
        <div class="row" style="margin-bottom:20px">

            <div class="col-lg-3">


                <label style="float:left;color:#b8b8b8;">KM'S Driven</label>
                <input type="number" class="regdate" required id="vehicle_kms_driven" name="vehicle_kms_driven"
                       value="{{ old('vehicle_kms_driven') }}" placeholder="please type in" style="padding:8px">

            </div>
            <div class="col-lg-3">

                <label style="float:left;color:#b8b8b8;">Registration No</label>
                <input type="text" class="regdate" required id="vehicle_registration_no" name="vehicle_registration_no"
                       value="{{ old('vehicle_registration_no') }}" placeholder="please type in" style="padding:8px">


            </div>


            <div class="col-lg-3">

                <label style="float:left;color:#b8b8b8;">Vehicle Color</label>
                <input type="text" class="regdate" id="vehicle_color" required name="vehicle_color"
                       value="{{ old('vehicle_color') }}" placeholder="please type in" style="padding:8px">


            </div>


        </div>
        <div class="row" style="margin-bottom:20px">
            <div class="col-lg-3">


                <input type="submit" class="btn btn-primary" style="width:60%;align:bottom;margin-top:25px">


            </div>
        </div>

    </div>
    </form>

    <div class="row">
        <div class="col-lg-12 text-center">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Vehicle Details</h3>
                </div>
            </div>
        </div>

    </div>


    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    {{--<div class="text-right">--}}
                    {{--<button data-toggle="modal" id="addCab" data-id="" class="btn btn-info addCabinfo">--}}
                    {{--<i class="fa fa-plus"></i>Add Cab--}}
                    {{--</button>--}}
                    {{--</div>--}}
                    <div class="table-responsive">
                        <table class="table table-striped font-12" id="datatable">
                            <thead>
                            <tr>
                                <th>Registration Number</th>
                                <th>Make Year</th>
                                <th>Make</th>
                                <th>Model</th>
                                <th>Color</th>
                                <th>Last Service</th>
                                <th>Last Logged in Executive</th>
                                <th>Services Done</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>Registration Number</th>
                                <th>Make Year</th>
                                <th>Make</th>
                                <th>Model</th>
                                <th>Color</th>
                                <th>Last Service</th>
                                <th>Last Logged in Executive</th>
                                <th>Services Done</th>
                                <th>Actions</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.col -->
    </div>







        <div class="modal modal-scale fade" id="editModal">
            <div class="modal-dialog modal-md">
                <div class="modal-content" style="background-color: rgba(0,0,0,0.7);color:#fff;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    style="color: #ffffff;border-bottom-color: transparent"
                                    aria-hidden="true">&times;</span></button>

                        {{--@if (count($errors) > 0)--}}
                            {{--@if ($errors->has('edit_executive_email')||$errors->has('edit_executive_mobile_no')||$errors->has('edit_executive_custom_id')||$errors->has('edit_executive_name')||$errors->has('edit_executive_driving_licence_no')||$errors->has('edit_executive_address')||$errors->has('edit_executive_date_of_birth')||$errors->has('executive_new_password')||$errors->has('executive_confirm_new_password'))--}}

                                {{--<div class="alert alert-danger">--}}
                                    {{--<ul>--}}
                                        {{--@foreach ($errors->all() as $error)--}}
                                            {{--<li>{{ $error }}</li>--}}
                                        {{--@endforeach--}}

                                    {{--</ul>--}}
                                {{--</div>--}}


                            {{--@endif--}}
                        {{--@endif--}}



                    </div>
                    <div class="modal-body" style="margin: auto">
                        <form action="/admin/edit-vehicle" role="form" id="editvehicleform"
                              class="editvehicleform"
                              method="post" enctype="multipart/form-data" style="padding: 20px">
                            <h3 style="color: white;margin-top: -20px">Vehicle Details</h3>

                            <input type="text" name="hidden_vehicle_id" id="hidden_vehicle_id"
                                   style="display: none;color: black;width:100%; padding:10px; margin-bottom:5px;margin-top:0px"><br>


                            <input type="text" name="edit_vehicle_make" id="edit_vehicle_make" value="{{ old('edit_vehicle_make') }}"
                                   style="color: black;width:100%; padding:10px; margin-bottom:5px;margin-top:5px"><br>

                            <input type="text" name="edit_vehicle_model" id="edit_vehicle_model" value="{{ old('edit_vehicle_model') }}"
                                   style="color:black;width:100%; padding:10px; margin-bottom:5px;margin-top:5px"><br>

                            <input type="text" name="edit_vehicle_date_of_purchase" id="edit_vehicle_date_of_purchase" value="{{ old('edit_vehicle_date_of_purchase') }}"
                                   style="color: black;width:100%; padding:10px; margin-bottom:5px;margin-top:5px"><br>

                            <input type="text" name="edit_vehicle_make_year" id="edit_vehicle_make_year" value="{{ old('edit_vehicle_make_year') }}"
                                   style="color: black;width:100%; padding:10px; margin-bottom:5px;margin-top:5px"><br>

                            <input type="text" name="edit_vehicle_kms_driven" id="edit_vehicle_kms_driven" value="{{ old('edit_vehicle_kms_driven') }}"
                                   style="color: black;width:100%; padding:10px; margin-bottom:5px;margin-top:5px"><br>

                            <input type="text" name="edit_vehicle_color" id="edit_vehicle_color" value="{{ old('edit_vehicle_color') }}"
                                   style="color: black;width:100%; padding:10px; margin-bottom:5px;margin-top:5px"><br>

                            <input type="text" name="edit_vehicle_registration_no" id="edit_vehicle_registration_no" value="{{ old('edit_vehicle_registration_no') }}"
                                   style="color: black;width:100%; padding:10px; margin-bottom:5px;margin-top:5px"><br>

                            <div class="modal-footer" style="margin-top:10px ">
                                <div class="pull-right">
                                    <input type="submit" class="btn btn-primary editvehicleinfo" data-id=""
                                           value="Update" id="edit-vehicle" style="background-color: #3e3232"/>
                                    <button type="button" class="btn btn-primary edit-toggle" data-dismiss="modal"
                                            style="background-color: red">
                                        Cancel
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>








        <!-- Start Modal -->
        <div class="modal modal-scale fade" id="deleteModal">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title font-header text-dark">Delete Executive</h4>
                    </div>
                    <form action="/admin/delete-executive" role="form" id="deleteform">
                        <div class="modal-body">
                            <div class="form-group form-input-group m-t-5 m-b-5">
                                <p> Are you sure you want to delete this vehicle ?? </p>

                                <input type="text" name="delete_id" id="delete_id"
                                       style="display: none;color: black;width:100%; padding:10px; margin-bottom:5px;margin-top:0px"><br>

                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary deleteexecutiveinfo" style="background-color: #3e3232" data-id="">Delete</button>
                            <button type="button" class="btn btn-primary" data-dismiss="modal"  style="background-color: red">Cancel</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- End Modal -->



//




    </div>
</div>
<!-- /#wrapper -->

<script type="text/javascript">


    $(document).ready(function () {
        $('#datatable').DataTable({
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
            processing: true,
            serverSide: true,
            ajax: '/admin/vehicle-ajax-handler',
            columns: [
                {data: 'vehicle_registration_no', name: 'vehicle_registration_no'},
                {data: 'vehicle_make_year', name: 'vehicle_make_year'},
                {data: 'vehicle_make', name: 'vehicle_make'},
                {data: 'vehicle_model', name: 'vehicle_model'},
                {data: 'vehicle_color', name: 'vehicle_color'},
                {data: 'vehicle_last_service', name: 'vehicle_last_service'},
                {data: 'vehicle_last_logged_in_executive', name: 'vehicle_last_logged_in_executive'},
                {data: 'vehicle_services_done', name: 'vehicle_services_done'},
                {data: 'action', name: 'action'}
            ],


            initComplete: function () {
                this.api().columns().every(function () {
                    var column = this;
                    var input = document.createElement("input");
                    $(input).appendTo($(column.footer()).empty())
                        .on('change', function () {
                            column.search($(this).val(), false, false, true).draw();
                        });
                });
            }
        });







        $(document).on('click', '#editVehicle', function (e) {
            e.preventDefault();
            $("#editModal").modal("show");
            var vehicle_id = $(this).attr('data-id');


            $.ajax({
                url: '/admin/vehicle-details-ajax-handler',
                type: 'post',
                dataType: 'json',
                data: {
                    method: 'getVehicleById',
                    vehicle_id: vehicle_id

                },
                beforeSend: function () {
                },
                success: function (response) {
                    console.log(response);

                    $('#edit_vehicle_registration_no').val(response.vehicle_registration_no);
                    $('#edit_vehicle_make_year').val(response.vehicle_make_year);
                    $('#hidden_vehicle_id').val(response.vehicle_id);
                    $('#edit_vehicle_make').val(response.vehicle_make);
                    $('#edit_vehicle_model').val(response.vehicle_model);
                    $('#edit_vehicle_color').val(response.vehicle_color);
                    $('#edit_vehicle_date_of_purchase').val(response.vehicle_date_of_purchase);
                    $('#edit_vehicle_kms_driven').val(response.vehicle_kms_driven);


                }
            });
        });






        $(document).on('click', '.deleteVehicle', function (e) {
            e.preventDefault();
            $("#deleteModal").modal("show");
            var executive_custom_id = $(this).attr('data-id');

            $.ajax({
                url: '/admin/executive-details-ajax-handler',
                type: 'post',
                dataType: 'json',
                data: {
                    method: 'getVehicleById',
                    vehicle_id: vehicle_id

                },
                beforeSend: function () {
                },
                success: function (response) {

                    console.log(response);
                    $('#delete_id').val(response.vehicle_id);

                },

            });
        });




    });

</script>

</body>
</html>
