<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dhobi Ghat Admin</title>

    <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="/assets/font-awesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="/assets/css/local.css"/>

    <script type="text/javascript" src="/assets/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="/assets/bootstrap/js/bootstrap.min.js"></script>

    <!-- you need to include the shieldui css and js assets in order for the charts to work -->
    <link rel="stylesheet" type="text/css"
          href="http://www.shieldui.com/shared/components/latest/css/light-bootstrap/all.min.css"/>
    <script type="text/javascript"
            src="http://www.shieldui.com/shared/components/latest/js/shieldui-all.min.js"></script>
    <script type="text/javascript" src="http://www.prepbootstrap.com/Content/js/gridData.js"></script>


    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css"/>
    <script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>

</head>
<body style="background-image:none">
<div id="wrapper">
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="navbar-header" style="background-color:#535358">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index">Admin Panel</a>
        </div>
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <li><a href="overview"><i class="fa fa-bullseye"></i> Overview</a></li>
                <li><a href="executive"><i class="fa fa-tasks"></i> Executive Details</a></li>
                <li><a href="vehicle"><i class="fa fa-globe"></i> Vehicle Details</a></li>
                <li class="active"><a href="business"><i class="fa fa-list-ol"></i> Business Model</a></li>
                <li><a href="promotions"><i class="fa fa-font"></i> Promotions</a></li>
                <li><a href="booking"><i class="fa fa-font"></i> Scheduled Bookings</a></li>
                <li><a href="cancellation"><i class="fa fa-font"></i> Cancellation</a></li>
                <li><a href="feedbacks"><i class="fa fa-font"></i> Feedbacks</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right navbar-user">

                <li class="dropdown user-dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Steve Miller<b
                                class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="#"><i class="fa fa-user"></i> Profile</a></li>
                        <li><a href="#"><i class="fa fa-gear"></i> Settings</a></li>
                        <li class="divider"></li>
                        <li><a href="login"><i class="fa fa-power-off"></i> Log Out</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>

    <div id="page-wrapper">
        <div class="row" style="margin-bottom:20px">


            <form action="/admin/update-price" role="form" id="updatepriceform"
                  class="updatepriceform"
                  method="post" enctype="multipart/form-data" style="padding: 20px">

                <h3 style="margin-bottom:20px;margin-left:20px">Set Price</h3>

                <div class="col-lg-4">


                    <input type="text" required class="regdate1" placeholder="Set Minimum Price (INR)"
                           name="minimum_price" id="minimum_price" value="{{ old('minimum_price') }}"
                           style="padding:8px;margin-bottom:10px">


                </div>
                <div class="col-lg-4">


                    <input type="text" required class="regdate1" name="minimum_weight" id="minimum_weight"
                           value="{{ old('minimum_weight') }}" placeholder="Set Minimum Weight (KG)"
                           style="padding:8px;margin-bottom:10px">

                </div>
                <div class="col-lg-3">


                    <button type="submit" class="btn btn-primary" style="width:60%;align:bottom;margin-top:0px">Save
                    </button>

                </div>
                <div class="col-lg-1">


                </div>
            </form>
        </div>



        <div class="row" style="margin-bottom:20px">

            <form action="/admin/add-category" role="form" id="addcategoryform"
                  class="addcategoryform"
                  method="post" enctype="multipart/form-data" style="padding: 20px">


            <h3 style="margin-bottom:20px;margin-left:20px">Add A Category</h3>
            <div class="col-lg-3">


                <input type="text" class="regdate" placeholder="Category Name" required
                       name="category_name" id="category_name" value="{{ old('category_name') }}"
                       style="padding:8px;margin-bottom:10px">

            </div>
            <div class="col-lg-3">


                <input type="text" class="regdate" placeholder="Average Wight (Kg/Piece)" required
                       name="category_average_weight_per_piece" id="category_average_weight_per_piece" value="{{ old('category_average_weight_per_piece') }}"
                       style="padding:8px;margin-bottom:10px">


            </div>


            <div class="col-lg-3">

                <input type="text" class="regdate" placeholder="Price INR/Kg" required
                       name="category_average_price_per_kg" id="category_average_price_per_kg" value="{{ old('category_average_price_per_kg') }}"
                       style="padding:8px">


            </div>
            <div class="col-lg-3">


            </div>

        </div>
        <div class="row" style="margin-bottom:20px;margin-top:30px">
            <div class="col-lg-3">


                <label style="float:left;color:#b8b8b8;">Add a black icon</label>

                <input type="file" class="regdate" required
                       name="category_icon_black" id="category_icon_black" value="{{ old('category_icon_black') }}"

                       style="padding:8px;margin-bottom:10px">


            </div>
            <div class="col-lg-3">


                <label style="float:left;color:#b8b8b8;">Add a white icon</label>
                <input type="file" class="regdate" required
                       name="category_icon_white" id="category_icon_white" value="{{ old('category_icon_white') }}"

                       style="padding:8px;margin-bottom:10px">


            </div>
            <div class="col-lg-3">


                <button type="submit" class="btn btn-primary" style="width:60%;align:bottom;margin-top:20px">Add
                </button>


            </div>
            <div class="col-lg-3">


            </div>
        </form>
        </div>


        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Category Details</h3>
                    </div>
                </div>
            </div>

        </div>


        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        {{--<div class="text-right">--}}
                        {{--<button data-toggle="modal" id="addCab" data-id="" class="btn btn-info addCabinfo">--}}
                        {{--<i class="fa fa-plus"></i>Add Cab--}}
                        {{--</button>--}}
                        {{--</div>--}}
                        <div class="table-responsive">
                            <table class="table table-striped font-12" id="datatable">
                                <thead>
                                <tr>
                                    <th>Category Icon 1</th>
                                    <th>Category Icon 2</th>
                                    <th>Category Name</th>
                                    <th>Average Weight(Kg)/Piece</th>
                                    <th>Price(INR)/Kg</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>Category Icon 1</th>
                                    <th>Category Icon 2</th>
                                    <th>Category Name</th>
                                    <th>Average Weight(Kg)/Piece</th>
                                    <th>Price(INR)/Kg</th>
                                    <th>Actions</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.col -->
        </div>


        <div class="modal modal-scale fade" id="editModal">
            <div class="modal-dialog modal-md">
                <div class="modal-content" style="background-color: rgba(0,0,0,0.7);color:#fff;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    style="color: #ffffff;border-bottom-color: transparent"
                                    aria-hidden="true">&times;</span></button>

                        {{--@if (count($errors) > 0)--}}
                        {{--@if ($errors->has('edit_executive_email')||$errors->has('edit_executive_mobile_no')||$errors->has('edit_executive_custom_id')||$errors->has('edit_executive_name')||$errors->has('edit_executive_driving_licence_no')||$errors->has('edit_executive_address')||$errors->has('edit_executive_date_of_birth')||$errors->has('executive_new_password')||$errors->has('executive_confirm_new_password'))--}}

                        {{--<div class="alert alert-danger">--}}
                        {{--<ul>--}}
                        {{--@foreach ($errors->all() as $error)--}}
                        {{--<li>{{ $error }}</li>--}}
                        {{--@endforeach--}}

                        {{--</ul>--}}
                        {{--</div>--}}


                        {{--@endif--}}
                        {{--@endif--}}


                    </div>
                    <div class="modal-body" style="margin: auto">
                        <form action="/admin/edit-category" role="form" id="editcategoryform"
                              class="editcategoryform"
                              method="post" enctype="multipart/form-data" style="padding: 20px">
                            <h3 style="color: white;margin-top: -20px">Vehicle Details</h3>


                            <input type="text" name="hidden_category_id" id="hidden_category_id"
                                   style="display: none;color: black;width:100%; padding:10px; margin-bottom:5px;margin-top:0px"><br>

                            <label style="float:left;color:#ffffff;">Category Name</label>
                            <input type="text" name="edit_category_name" id="edit_category_name"
                                   value="{{ old('edit_category_name') }}"
                                   style="color: black;width:100%; padding:10px; margin-bottom:5px;margin-top:5px"><br>

                            <label style="float:left;color:#ffffff;">Average Weight(Kg)/Piece</label>
                            <input type="text" name="edit_category_average_weight_per_piece"
                                   id="edit_category_average_weight_per_piece"
                                   value="{{ old('edit_category_average_weight_per_piece') }}"
                                   style="color:black;width:100%; padding:10px; margin-bottom:5px;margin-top:5px"><br>

                            <label style="float:left;color:#ffffff;">Price(INR)/Kg</label>
                            <input type="text" name="edit_category_average_price_per_kg"
                                   id="edit_category_average_price_per_kg"
                                   value="{{ old('edit_category_average_price_per_kg') }}"
                                   style="color: black;width:100%; padding:10px; margin-bottom:5px;margin-top:5px"><br>

                            <h3 style="color: white;margin-bottom: 20px">Upload New Icons</h3>

                            <label style="float:left;color:#ffffff;">Category Icon 1</label>
                            <input type="file" name="edit_category_icon_black" id="edit_category_icon_black"
                                   value="{{ old('edit_category_icon_black') }}"
                                   style="color: white;width:100%; padding:10px; margin-bottom:5px;margin-top:5px"><br>

                            <label style="float:left;color:#ffffff;">Category Icon 2</label>
                            <input type="file" name="edit_category_icon_white" id="edit_category_icon_white"
                                   value="{{ old('edit_category_icon_white') }}"
                                   style="color: white;width:100%; padding:10px; margin-bottom:5px;margin-top:5px"><br>

                            <div class="modal-footer" style="margin-top:10px ">
                                <div class="pull-right">
                                    <input type="submit" class="btn btn-primary editcategoryinfo" data-id=""
                                           value="Update" id="edit-category" style="background-color: #3e3232"/>
                                    <button type="button" class="btn btn-primary edit-toggle" data-dismiss="modal"
                                            style="background-color: red">
                                        Cancel
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


        <!-- Start Modal -->
        <div class="modal modal-scale fade" id="deleteModal">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title font-header text-dark">Delete Category</h4>
                    </div>
                    <form action="/admin/delete-category" role="form" id="deleteform">
                        <div class="modal-body">
                            <div class="form-group form-input-group m-t-5 m-b-5">
                                <p> Are you sure you want to delete this category ? </p>

                                <input type="text" name="delete_id" id="delete_id"
                                       style="display: none;color: black;width:100%; padding:10px; margin-bottom:5px;margin-top:0px"><br>

                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary deletecategoryinfo"
                                    style="background-color: #3e3232" data-id="">Delete
                            </button>
                            <button type="button" class="btn btn-primary" data-dismiss="modal"
                                    style="background-color: red">Cancel
                            </button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- End Modal -->


    </div>
</div>
<!-- /#wrapper -->

<script type="text/javascript">


    $(document).ready(function () {
        $('#datatable').DataTable({
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
            processing: true,
            serverSide: true,
            ajax: '/admin/business-ajax-handler',
            columns: [
                // {data: 'category_id', name: 'category_id'},
                {data: 'category_icon_black', name: 'category_icon_black'},
                {data: 'category_icon_white', name: 'category_icon_white'},
                {data: 'category_name', name: 'category_name'},
                {data: 'category_average_weight_per_piece', name: 'category_average_weight_per_piece'},
                {data: 'category_average_price_per_kg', name: 'category_average_price_per_kg'},
                {data: 'action', name: 'action'}
            ],


            initComplete: function () {
                this.api().columns().every(function () {
                    var column = this;
                    var input = document.createElement("input");
                    $(input).appendTo($(column.footer()).empty())
                        .on('change', function () {
                            column.search($(this).val(), false, false, true).draw();
                        });
                });
            }
        });


        $(document).on('click', '#editCategory', function (e) {
            e.preventDefault();
            $("#editModal").modal("show");
            var category_id = $(this).attr('data-id');


                $.ajax({
                    url: '/admin/business-details-ajax-handler',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        method: 'getCategoryById',
                        category_id: category_id

                    },
                    beforeSend: function () {
                    },
                    success: function (response) {
                        console.log(response);

                        $('#edit_category_name').val(response.category_name);
                        $('#hidden_category_id').val(response.category_id);
                        $('#edit_category_average_weight_per_piece').val(response.category_average_weight_per_piece);
                        $('#edit_category_average_price_per_kg').val(response.category_average_price_per_kg);
//                        $('#edit_category_icon_black').val(response.category_icon_black);
//                        $('#edit_category_icon_white').val(response.category_icon_white);



                    }
                });
        });


        $(document).on('click', '.deleteCategory', function (e) {
            e.preventDefault();
            $("#deleteModal").modal("show");
            var category_id = $(this).attr('data-id');

            $.ajax({
                url: '/admin/business-details-ajax-handler',
                type: 'post',
                dataType: 'json',
                data: {
                    method: 'getCategoryById',
                    category_id: category_id

                },
                beforeSend: function () {
                },
                success: function (response) {
                    console.log(response);

                    $('#delete_id').val(response.category_id);



                }
            });
        });


    });


</script>


</body>
</html>
