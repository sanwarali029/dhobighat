<?php namespace App\Http\Modules\Admin\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\DB;

class PriceDetails extends Model {

    //
//

    protected $table = 'price_details';
    private static $_instance = null;


    public static function getInstance()
    {
        if (!is_object(self::$_instance))  //or if( is_null(self::$_instance) ) or if( self::$_instance == null )
            self::$_instance = new PriceDetails();
        return self::$_instance;
    }



    public function updatePriceById()
    {
        if (func_num_args() > 0) {
            $price_id = func_get_arg(0);
            $data = func_get_arg(1);



            try {
                $result = DB::table($this->table)
                    ->where('price_id', $price_id)
                    ->update($data);
            } catch (\Exception $e) {
                return $e->getMessage();
            }
            if ($result) {
                return $result;
            } else {
                return 0;
            }
        } else {
            throw new Exception('Argument Not Passed');
        }
    }


}
