<?php namespace App\Http\Modules\Admin\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\DB;

use Yajra\Datatables\Datatables;
use Illuminate\Support\Collection;


class CancellationDetails extends Model {

    //


    protected $table = 'cancellation_details';
    private static $_instance = null;


    public static function getInstance()
    {
        if (!is_object(self::$_instance))  //or if( is_null(self::$_instance) ) or if( self::$_instance == null )
            self::$_instance = new CancellationDetails();
        return self::$_instance;
    }


    public function getCancellationDetails(){

        $result = DB::table($this->table)
            ->where('cancellation_status', 0)
            ->get();

   return $result;

    }


    public function getCancellationById($cancellation_id){

        $result = DB::table($this->table)
            ->where('cancellation_id', $cancellation_id)
            ->first();

        if ($result) {
            return $result;
        } else {
            return 0;
        }

    }




    public function addCancellationById()
    {
        if (func_num_args() > 0) {
            $data = func_get_arg(0);
            try {
                $result = DB::table($this->table)
                    ->insertGetId($data);
                return $result;
            } catch (\Exception $e) {
                return $e->getMessage();
            }
        } else {
            throw new Exception('Argument Not Passed');
        }
    }





    public function updateCancellationById()
    {
        if (func_num_args() > 0) {
            $cancellation_id = func_get_arg(0);
            $data = func_get_arg(1);



            try {
                $result = DB::table($this->table)
                    ->where('cancellation_id', $cancellation_id)
                    ->update($data);
            } catch (\Exception $e) {
                return $e->getMessage();
            }
            if ($result) {
                return $result;
            } else {
                return 0;
            }
        } else {
            throw new Exception('Argument Not Passed');
        }
    }




}
