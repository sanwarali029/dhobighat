<?php namespace App\Http\Modules\Admin\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CustomerDetails extends Model {

    //

    protected $table='customer_details';
    private static $_instance = null;

    public static function getInstance()
    {
        if (!is_object(self::$_instance))  //or if( is_null(self::$_instance) ) or if( self::$_instance == null )
            self::$_instance = new CustomerDetails();
        return self::$_instance;
    }



    public function getCustomerById($customer_id){

        $result = DB::table($this->table)
            ->where('customer_id',$customer_id)
            ->first();

        if ($result) {
            return $result;
        } else {
            return 0;
        }

    }


}
