<?php namespace App\Http\Modules\Admin\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use Yajra\Datatables\Datatables;
use Illuminate\Support\Collection;

class ExecutiveDetails extends Model
{

    //

    protected $table = 'executive_details';
    private static $_instance = null;


    public static function getInstance()
    {
        if (!is_object(self::$_instance))  //or if( is_null(self::$_instance) ) or if( self::$_instance == null )
            self::$_instance = new ExecutiveDetails();
        return self::$_instance;
    }


//    public function getUsersLogin($email, $password)
//    {
//
//        $result = DB::table($this->table)
//            ->where('admin_email', $email)
//            ->where('admin_password', $password)
//            ->first();
//
//        if ($result) {
//            return $result;
//        } else {
//            return 0;
//        }
//
//    }


    public function addExecutive()
    {
        if (func_num_args() > 0) {
            $ExecutiveData = func_get_arg(0);
            try {
                $result = DB::table($this->table)
                    ->insertGetId($ExecutiveData);
                return $result;
            } catch (\Exception $e) {
                return $e->getMessage();
            }
        } else {
            throw new Exception('Argument Not Passed');
        }
    }


    public function getExecutiveDetails(){

        $result = DB::table($this->table)
            ->where('executive_status','!=',2)
            ->get();

            return $result;


    }
    public function getExecutiveById($executive_custom_id){

        $result = DB::table($this->table)
            ->where('executive_custom_id',$executive_custom_id)
            ->first();

        if ($result) {
            return $result;
        } else {
            return 0;
        }

    }

    public function getExecutiveByExeId($executive_id){

        $result = DB::table($this->table)
            ->where('executive_id',$executive_id)
            ->first();

        if ($result) {
            return $result;
        } else {
            return 0;
        }

    }

    /**
     * @param string $table
     */
    public function updateExecutiveById()
    {
        if (func_num_args() > 0) {
            $executive_id = func_get_arg(0);
            $data = func_get_arg(1);



            try {
                $result = DB::table($this->table)
                    ->where('executive_id', $executive_id)
                    ->update($data);
            } catch (\Exception $e) {
                return $e->getMessage();
            }
            if ($result) {
                return $result;
            } else {
                return 0;
            }
        } else {
            throw new Exception('Argument Not Passed');
        }
    }

    public function deleteExecutiveById(){

        if (func_num_args() > 0) {
            $executive_id = func_get_arg(0);
            $data = func_get_arg(1);



            try {
                $result = DB::table($this->table)
                    ->where('executive_id', $executive_id)
                    ->update($data);
            } catch (\Exception $e) {
                return $e->getMessage();
            }
            if ($result) {
                return $result;
            } else {
                return 0;
            }
        } else {
            throw new Exception('Argument Not Passed');
        }

    }

}

