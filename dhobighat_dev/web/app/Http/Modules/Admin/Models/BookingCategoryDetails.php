<?php namespace App\Http\Modules\Admin\Models;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class BookingCategoryDetails extends Model {

    //

    protected $table = 'booking_category_details';
    private static $_instance = null;


    public static function getInstance()
    {
        if (!is_object(self::$_instance))  //or if( is_null(self::$_instance) ) or if( self::$_instance == null )
            self::$_instance = new BookingCategoryDetails();
        return self::$_instance;
    }


    /**
     * @return string
     */
    public function addBookingCategoryDetails()
    {

        if (func_num_args() > 0) {
            $BookingCategoryData = func_get_arg(0);
            try {
                $result = DB::table($this->table)
                    ->insertGetId($BookingCategoryData);
                return $result;
            } catch (\Exception $e) {
                return $e->getMessage();
            }
        } else {
            throw new Exception('Argument Not Passed');
        }
    }

    }
}
