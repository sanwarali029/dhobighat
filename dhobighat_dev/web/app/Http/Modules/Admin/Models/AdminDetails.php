<?php namespace App\Http\Modules\Admin\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\DB;

class AdminDetails extends Model {

    //

    protected $table ='admin_details';
    protected $fillable=['admin_email','admin_password','admin_username'];
    protected $hidden = ['admin_password', 'remember_token'];
    private static $_instance = null;


    public static function getInstance()
    {
        if (!is_object(self::$_instance))  //or if( is_null(self::$_instance) ) or if( self::$_instance == null )
            self::$_instance = new AdminDetails();
        return self::$_instance;
    }


    public function getUsersLogin($email, $password)
    {

        $result = DB::table($this->table)
            ->where('admin_email', $email)
            ->where('admin_password', $password)
            ->first();

        if ($result) {
            return $result;
        } else {
            return 0;
        }

    }


}
