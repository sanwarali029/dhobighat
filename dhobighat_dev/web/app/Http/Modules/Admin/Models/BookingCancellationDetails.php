<?php namespace App\Http\Modules\Admin\Models;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class BookingCancellationDetails extends Model {

    //

    protected $table = 'booking_cancellation_details';
    private static $_instance = null;


    public static function getInstance()
    {
        if (!is_object(self::$_instance))  //or if( is_null(self::$_instance) ) or if( self::$_instance == null )
            self::$_instance = new BookingCancellationDetails();
        return self::$_instance;
    }


    /**
     * @return string
     */
    public function getBookingCancellationById()
    {

        if (func_num_args() > 0) {
            $booking_id = func_get_arg(0);
            try {
                $result = DB::table($this->table)
                    ->where('booking_id',$booking_id)
                    ->first();
                return $result;
            } catch (\Exception $e) {
                return $e->getMessage();
            }
        } else {
            throw new Exception('Argument Not Passed');
        }
    }


}
