<?php namespace App\Http\Modules\Admin\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use Yajra\Datatables\Datatables;
use Illuminate\Support\Collection;

class CategoryDetails extends Model {

    //


    protected $table = 'category_details';
    private static $_instance = null;


    public static function getInstance()
    {
        if (!is_object(self::$_instance))  //or if( is_null(self::$_instance) ) or if( self::$_instance == null )
            self::$_instance = new CategoryDetails();
        return self::$_instance;
    }



    public function addCategory()
    {
        if (func_num_args() > 0) {
            $data = func_get_arg(0);
            try {
                $result = DB::table($this->table)
                    ->insertGetId($data);
                return $result;
            } catch (\Exception $e) {
                return $e->getMessage();
            }
        } else {
            throw new Exception('Argument Not Passed');
        }
    }



    public function getCategoryById($category_id){

        $result = DB::table($this->table)
            ->where('category_id',$category_id)
            ->first();

        if ($result) {
            return $result;
        } else {
            return 0;
        }
//
    }




    public function getCategoryDetails(){

        $result = DB::table($this->table)
            ->where('category_status','!=',2)
            ->get();


            return $result;

    }



    public function updateCategoryById()
    {
        if (func_num_args() > 0) {
            $category_id = func_get_arg(0);
            $data = func_get_arg(1);



            try {
                $result = DB::table($this->table)
                    ->where('category_id', $category_id)
                    ->update($data);
            } catch (\Exception $e) {
                return $e->getMessage();
            }
            if ($result) {
                return $result;
            } else {
                return 0;
            }
        } else {
            throw new Exception('Argument Not Passed');
        }
    }


}
