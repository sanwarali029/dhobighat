<?php namespace App\Http\Modules\Admin\Models;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class BookingDetails extends Model {



    protected $table = 'booking_details';
    private static $_instance = null;


    public static function getInstance()
    {
        if (!is_object(self::$_instance))  //or if( is_null(self::$_instance) ) or if( self::$_instance == null )
            self::$_instance = new BookingDetails();
        return self::$_instance;
    }



    public function getBookingDetails(){

        $result = DB::table($this->table)
            ->where('booking_status',1)
            ->get();

        return $result;


    }


    public function getRejectedBookingDetails(){

        $result = DB::table($this->table)
            ->where('service_status',4)
            ->get();

        if ($result) {
            return $result;
        } else {
            return 0;
        }

    }


    public function getCompletedBookingDetails(){

        $result = DB::table($this->table)
            ->where('service_status',3)
            ->get();

        if ($result) {
            return $result;
        } else {
            return 0;
        }

    }



    /**
     * @return string
     */
    public function addNewBooking()
    {

        if (func_num_args() > 0) {
            $BookingData = func_get_arg(0);
            try {
                $result = DB::table($this->table)
                    ->insertGetId($BookingData);
                return $result;
            } catch (\Exception $e) {
                return $e->getMessage();
            }
        } else {
            throw new Exception('Argument Not Passed');
        }
    }


    public function getBookingByVehicleId()
    {

        if (func_num_args() > 0) {
            $vehicle_id = func_get_arg(0);
            try {
                $result = DB::table($this->table)
                    ->where('vehicle_id',$vehicle_id)
                    ->first();
                return $result;
            } catch (\Exception $e) {
                return $e->getMessage();
            }
        } else {
            throw new Exception('Argument Not Passed');
        }
    }




    public function getBookingById()
    {

        if (func_num_args() > 0) {
            $booking_id = func_get_arg(0);
            try {
                $result = DB::table($this->table)
                    ->where('booking_id',$booking_id)
                    ->first();
                return $result;
            } catch (\Exception $e) {
                return $e->getMessage();
            }
        } else {
            throw new Exception('Argument Not Passed');
        }
    }

}
