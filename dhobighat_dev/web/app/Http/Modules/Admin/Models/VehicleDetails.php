<?php namespace App\Http\Modules\Admin\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\DB;

use Yajra\Datatables\Datatables;
use Illuminate\Support\Collection;

class VehicleDetails extends Model {

    //
//
    protected $table = 'vehicle_details';
    private static $_instance = null;


    public static function getInstance()
    {
        if (!is_object(self::$_instance))  //or if( is_null(self::$_instance) ) or if( self::$_instance == null )
            self::$_instance = new VehicleDetails();
        return self::$_instance;
    }



    public function addVehicle()
    {
        if (func_num_args() > 0) {
            $data = func_get_arg(0);
            try {
                $result = DB::table($this->table)
                    ->insertGetId($data);
                return $result;
            } catch (\Exception $e) {
                return $e->getMessage();
            }
        } else {
            throw new Exception('Argument Not Passed');
        }
    }



    public function getVehicleById($vehicle_id){

        $result = DB::table($this->table)
            ->where('vehicle_id',$vehicle_id)
            ->first();

        if ($result) {
            return $result;
        } else {
            return 0;
        }

    }




    public function getVehicleDetails(){

        $result = DB::table($this->table)
            ->where('vehicle_status','!=',2)
            ->get();


            return $result;


    }



    public function updateVehicleById()
    {
        if (func_num_args() > 0) {
            $vehicle_id = func_get_arg(0);
            $data = func_get_arg(1);



            try {
                $result = DB::table($this->table)
                    ->where('vehicle_id', $vehicle_id)
                    ->update($data);
            } catch (\Exception $e) {
                return $e->getMessage();
            }
            if ($result) {
                return $result;
            } else {
                return 0;
            }
        } else {
            throw new Exception('Argument Not Passed');
        }
    }





}
